<?php

include_once 'vendor/autoload.php';
use DAT\PostmanDAT\PostmanDAT;

Class MyClaimUploader {

    private $_postman;

    public function __construct()
    {
        $postman = new PostmanDAT();
        $postman->setCustomerNumber(getenv('mcCstNum'));
        $postman->setLogin(getenv('mcCstLogin'));
        $postman->setCustomerSignature(getenv('mcCstSgn'));
        $postman->setInterfacePartner(getenv('mcIntPtNum'));
        $postman->setInterfacePartnerSignature(getenv('mcIntPtSqn'));
        $postman->setHost(getenv('mcHost'));

        $this->_postman = $postman;
    }

    private function _showErrors(Exception $e)
    {
        echo '<pre>';
        echo htmlentities($this->_postman->getLastRequest());
        echo htmlentities($this->_postman->getLastResponse());
        echo '</pre>';
    }

    public function create($referenceNumber)
    {
        $contractData = new StdClass();
        $contractData->contractType = getenv('mcCtrType');
        $contractData->networkType  = getenv('mcNetwork');
        $contractData->templateId   = getenv('mcTemplId');

        $contractData->Dossier = new stdClass();
        $contractData->Dossier->Country  = getenv('mcCountry');
        $contractData->Dossier->Language = getenv('mcLang');
        $contractData->Dossier->Currency = getenv('mcCurr');

        $contractData->templateData = [];
        $contractData->templateData[] = $this->_postman->createEntry( "referenceNumber", $referenceNumber);

        try {
            $response = $this->_postman->mcCreateOrUpdateContract($contractData);
            $ret = $response->return;
        }
        catch (Exception $e)
        {
            $ret = FALSE;
            $this->_showErrors($e);
        }

        return $ret;
    }

    public function uploadImage($filePath, $contractId, $myClaimFolder)
    {
        $contractData = new StdClass();
        $contractData->contractId = $contractId;
        $contractData->attachmentItem = new stdClass();
        $contractData->attachmentItem->fileName = basename($filePath);
        $contractData->attachmentItem->mimeType = "image/jpeg";
        $contractData->attachmentItem->documentType = $myClaimFolder;
        $contractData->attachmentItem->binaryData = file_get_contents($filePath);

        $ret = FALSE;

        try {
            $this->_postman->mcUploadAttachmentByIdentification($contractData);
            $ret = TRUE;
        } catch (Exception $e)
        {
            $this->_showErrors($e);
        }

        return $ret;
    }

    private static function moveToBackupFolder($filePath)
    {
        $path = getenv('processed');
        @mkdir($path);
        $path .= DIRECTORY_SEPARATOR . basename(dirname(dirname($filePath)));
        @mkdir($path);
        $path .= DIRECTORY_SEPARATOR . basename(dirname($filePath));
        @mkdir($path);
        $path .= DIRECTORY_SEPARATOR . basename($filePath);

        @rename($filePath, $path);
    }

    public function processClaimList(ClaimLister $cl)
    {
        /**
         * @var Claim[]
         */
        foreach ($cl->getClaims() as $kn => $claims)
        {
            foreach ($claims as $timestamp => $claim) {
                $refNr = $kn.'_'.$timestamp;
                $contractId = $this->create($refNr);
                if ($contractId === FALSE) {
                    echo "Impossible to create contract $contractId</br>";
                    continue;
                }

                $path = getenv('localrepo') . DIRECTORY_SEPARATOR . $kn . DIRECTORY_SEPARATOR . $timestamp;

                foreach ($claim->getImageList() as $directory => $image) {
                    $fileName = $path . DIRECTORY_SEPARATOR . $image;
                    if (!$this->uploadImage($fileName, $contractId, $directory)) continue;
                    self::moveToBackupFolder($fileName);
                }

                @rmdir($path);
            }

        }
    }

}

class ClaimLister
{
    private $_claims = array();

    /**
     * @return Claim[]
     */
    public function getClaims():iterable
    {
        return $this->_claims;
    }

    public function addClaim(Claim $claim)
    {
        $this->_claims[$claim->getKn()][$claim->getTimestamp()] = $claim;
    }

    public function buildFromLocalPath($path){}

    public function buildFromRangesArray($rangesArray)
    {
        foreach ($rangesArray as $kn => $timestamps)
        {
            foreach ($timestamps as $timestamp => $imageList) {
                $claim = new Claim($kn, $timestamp);

                foreach ($imageList as $image)
                {
                    $claim->addImage(basename(dirname($image)), basename($image));
                }

                $this->addClaim($claim);
            }

        }
    }

}

class Claim {
    private $_kn;
    private $_timestamp;
    private $_imageList = array();

    public function __construct($kn, $timestamp)
    {
        $this->_kn = $kn;
        $this->_timestamp = $timestamp;
    }

    public function addImageList($imageList)
    {
        $this->_imageList = $imageList;
    }

    public function addImage($index, $path)
    {
        $this->_imageList[$index] = $path;
    }

    public function getKn()
    {
        return $this->_kn;
    }

    public function getTimestamp()
    {
        return $this->_timestamp;
    }

    public function getImageList():array
    {
        return $this->_imageList;
    }
}