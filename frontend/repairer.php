<?php header('Access-Control-Allow-Origin: *'); ?>
<script>
    $(document).ready(function(){
        $(document).trigger('inner_frame_loaded');
    });
</script>
<style>
.h3Selected{
	font-weight:bold;
	color:black;
	cursor: pointer;
}
.h3NotSelected{
	font-weight:unset;
	color:grey;
	cursor: pointer;
}
.labourRates{
	display:none;
}
body{
    background: #f8f8f8;
}
.form-control {
    background-color: #fff;
    color: #363636;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    justify-content: flex-start;
    line-height: 1.5;
    padding: calc(.375em - 1px) calc(.625em - 1px);
    border: 1px transparent;
	display:inline-block !important;
}
label {
    color: #86868b;
    text-transform: uppercase;
    font-size: 11px;
    margin: 0px;
    position: relative;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    float: left;
}
.form-group.col-md-6{
	display:inline-block !important;
}
.hiddenAbsolute{
visibility:hidden;
position:absolute;
}
.modal-content{
background-color:#f8f8f8;
}
</style>
<script>


function calcolaCF(nome, cognome, comune, data, sesso, targetFieldId) {
var ret = '';

$.ajax({
//url: 'http://webservices.dotnethell.it/codicefiscale.asmx/CalcolaCodiceFiscale',
url: 'https://wedat.it/myClaimDMS/calcolaCF.php',
type: "POST",
data: {
Nome: nome,
Cognome: cognome,
ComuneNascita: comune,
DataNascita: data,
Sesso: sesso
},
success: function (result) {
//console.log(result);
//ret = JSON.parse(JSON.stringify(result.getElementsByTagName('string')[0].innerHTML));
//$('#'+targetFieldId).val(ret);
$('#'+targetFieldId).val(result);
},
error: function (result) {
ret = 'Errore di connessione, riprovare';
$('#'+targetFieldId).val(ret);
}
});
}

$(function(){
$(document).ready(function(){
    $('#calcolaCF').on('click', function(){
		function month2digits(month)
    { 
        return (month < 10 ? '0' : '') + month;
    }
var dataDiNascita=$("#repairerCF_bornDate").val();
var formattedDate = new Date(dataDiNascita);
var d = formattedDate.getDate();
d= month2digits(d);
var m =  formattedDate.getMonth();
m += 1;  // JavaScript months are 0-11
m= month2digits(m);
var y = formattedDate.getFullYear();
var finalData=d + "/" + m + "/" + y;
        calcolaCF($("#repairerCF_name").val(), $("#repairerCF_surname").val(), $("#repairerCF_bornAt").val(), finalData, $("#repairerCF_gender").val(), 'repairerCF_Id');
    });

$(document).on('hide.bs.modal', '#frontEnd', function(e) {
	$("#calcoloCF").hide();
});
$(document).on('hide.bs.modal', '#calcoloCF', function(e) {
          console.log('pagehide');
         $("#repairer_idVatNumber").val($("#repairerCF_Id").val());

      } );
	$("#copiaCF").on("click",function(){ 
         $("#repairer_idVatNumber").val($("#repairerCF_Id").val());
	});	
$("#datiGeneraliH3").on("click",function(){
	$("#manodoperaH3").removeClass("h3Selected").addClass("h3NotSelected");
	$("#datiGeneraliH3").removeClass("h3NotSelected").addClass("h3Selected");
	$(".form-row").not('.labourRates').show();
	$(".form-row.labourRates").hide();
});
$("#manodoperaH3").on("click",function(){
	$("#manodoperaH3").removeClass("h3NotSelected").addClass("h3Selected");
	$("#datiGeneraliH3").removeClass("h3Selected").addClass("h3NotSelected");
	$(".form-row.labourRates").show();
	$(".form-row").not('.labourRates').hide();
});
});
});
</script>
<form id="repairer" class="">
 <div class="form-row2" style="display:none;">
<label class="kind_repairer" for="repairer_id">Id</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_id" id="repairer_id" placeholder=""></title>
 </div>
 <h3 id="datiGeneraliH3" class="h3Selected" style="display:inline-block;width:auto">Dati Generali</h3><h3 style="display:inline-block;width:auto;margin-left:10px" class="h3NotSelected" id="manodoperaH3">Manodopera</h3>
 <div class="form-row">
        <div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_name">Nome</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_name" id="repairer_name" placeholder=""></title>
 </div>
  <div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_code">Codice</label>
<input class="kind_repairer form-control" type="number" class="form-control" name="repairer_code" id="repairer_code" placeholder=""></title>
</div>
      </div>
	  <div class="form-row">
	  <div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_address">Indirizzo</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_address" id="repairer_address" placeholder=""></title>
 </div>
        <div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_city">Città</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_city" id="repairer_city" placeholder=""></title>
 </div></div>
 	  <div class="form-row">
  <div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_zip">Cap</label>
<input class="kind_repairer form-control" type="tel" maxlength="5" class="form-control" name="repairer_zip" id="repairer_zip" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_province"  >Provincia</label>
<input class="kind_repairer form-control" type="text" maxlength="2" style="text-transform: uppercase;" onkeyup="this.value = this.value.toUpperCase();" class="form-control" name="repairer_province" id="repairer_province" placeholder=""></title>
</div>
      </div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_country">Stato</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_country" id="repairer_country" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_phone">Telefono</label>
<input class="kind_repairer form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="repairer_phone" id="repairer_phone" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_mobilePhone">Cellulare</label>
<input class="kind_repairer form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="repairer_mobilePhone" id="repairer_mobilePhone" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_fax">Fax</label>
<input class="kind_repairer form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="repairer_fax" id="repairer_fax" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_idVatNumber">Cod.Fiscale</label>
<input class="kind_repairer form-control" type="text"  name="repairer_idVatNumber" id="repairer_idVatNumber" style="width:calc(100% - 40px);" placeholder=""><button style="width:34px;height:34px;float:right;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#calcoloCF">
 <i class="fas fa-id-card-alt" style="position:relative;top:-4px;left:-5px;"></i>
</button></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_vatNumber">P.Iva</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_vatNumber" id="repairer_vatNumber" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_mail">Email</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_mail" id="repairer_mail" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_pec">PEC</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_pec" id="repairer_pec" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_bank">Banca</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_bank" id="repairer_bank" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_swift">SWIFT</label>
<input class="kind_repairer form-control" type="text" class="form-control" pattern="[A-Z]{6,6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3,3}){0,1}" name="repairer_iban" id="repairer_swift" placeholder=""></title>
</div></div>
<div class="form-row"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_iban">IBAN</label>
<input class="kind_repairer form-control" type="text" class="form-control" pattern="/^(?:(?:IT|SM)\d{2}[A-Z]\d{22}|CY\d{2}[A-Z]\d{23}|NL\d{2}[A-Z]{4}\d{10}|LV\d{2}[A-Z]{4}\d{13}|(?:BG|BH|GB|IE)\d{2}[A-Z]{4}\d{14}|GI\d{2}[A-Z]{4}\d{15}|RO\d{2}[A-Z]{4}\d{16}|KW\d{2}[A-Z]{4}\d{22}|MT\d{2}[A-Z]{4}\d{23}|NO\d{13}|(?:DK|FI|GL|FO)\d{16}|MK\d{17}|(?:AT|EE|KZ|LU|XK)\d{18}|(?:BA|HR|LI|CH|CR)\d{19}|(?:GE|DE|LT|ME|RS)\d{20}|IL\d{21}|(?:AD|CZ|ES|MD|SA)\d{22}|PT\d{23}|(?:BE|IS)\d{24}|(?:FR|MR|MC)\d{25}|(?:AL|DO|LB|PL)\d{26}|(?:AZ|HU)\d{27}|(?:GR|MU)\d{28})$/i" name="repairer_iban" id="repairer_iban" placeholder=""></title>
</div></div>
    <div class="form-row labourRates">
        <div class="form-group col-md-6">
            <button class=" btn btn-secondary dropdown-toggle" type="button" id="subcollection"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Seleziona una Fascia
            </button>
            <div class="dropdown-menu" aria-labelledby="subcollection" id="subcollection_items">
                <a class="dropdown-item" href="#">-Nuova Fascia-</a>
            </div>
        </div>
        <div class="form-group col-md-6">
            <button class="btn btn-secondary" type="button" id="newCostRateButton" aria-expanded="false">
                &plus; Crea nuova Fascia
            </button>
        </div>
    </div>
<div class="form-row labourRates" id="divFascia">
    <div class="form-group col-md-6">
        <label class="kind_repairer kind_costRate" for="repairer_rateSelected">Fascia</label>
        <input class="kind_repairer kind_costRate form-control" type="text" name="repairer_rateSelected" id="repairer_rateSelected" placeholder=""></title>
    </div>
</div>
<div class="form-row labourRates"><div class="form-group col-md-6">
<label class="kind_repairer kind_costRate" for="repairer_labourRatesBody">Mdo Carrozzeria</label>
<input class="kind_repairer kind_costRate form-control" type="number" step=".01" class="form-control" name="repairer_labourRatesBody" id="repairer_labourRatesBody" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer kind_costRate" for="repairer_labourRatesMec">Mdo Meccanica</label>
<input class="kind_repairer kind_costRate form-control" type="number" step=".01" class="form-control" name="repairer_labourRatesMec" id="repairer_labourRatesMec" placeholder=""></title>
</div></div>
<div class="form-row labourRates"><div class="form-group col-md-6">
<label class="kind_repairer kind_costRate" for="repairer_labourRatesTotalDiscount">Sconto su totale%</label>
<input class="kind_repairer kind_costRate form-control" type="number"  class="form-control" name="repairer_labourRatesTotalDiscount" id="repairer_labourRatesTotalDiscount" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer kind_costRate" for="repairer_labourRatesSPDiscount">Sconto su ricambi%</label>
<input class="kind_repairer kind_costRate form-control" type="number"  class="form-control" name="repairer_labourRatesSPDiscount" id="repairer_labourRatesSPDiscount" placeholder=""></title>
</div></div>
<div class="form-row labourRates"><div class="form-group col-md-6">
<label class="kind_repairer kind_costRate" for="repairer_labourRatesMonoCoat">Monostrato</label>
<input class="kind_repairer kind_costRate form-control" type="number"  class="form-control" name="repairer_labourRatesMonoCoat" id="repairer_labourRatesMonoCoat" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer kind_costRate" for="repairer_labourRatesMonoCoat15">Monostrato +15h</label>
<input class="kind_repairer kind_costRate form-control" type="number"  class="form-control" name="repairer_labourRatesMonoCoat15" id="repairer_labourRatesMonoCoat15" placeholder=""></title>
</div></div>
<div class="form-row labourRates"><div class="form-group col-md-6">
<label class="kind_repairer kind_costRate" for="repairer_labourRatesDoubleCoat">Doppiostrato</label>
<input class="kind_repairer kind_costRate form-control" type="number"  class="form-control" name="repairer_labourRatesDoubleCoat" id="repairer_labourRatesDoubleCoat" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer kind_costRate" for="repairer_labourRatesDoubleCoat15">Doppiostrato +15h</label>
<input class="kind_repairer kind_costRate form-control" type="number"  class="form-control" name="repairer_labourRatesDoubleCoat15" id="repairer_labourRatesDoubleCoat15" placeholder=""></title>
</div></div>
<div class="form-row labourRates"><div class="form-group col-md-6">
<label class="kind_repairer kind_costRate" for="repairer_labourRatesTripleCoat">Triplostrato</label>
<input class="kind_repairer kind_costRate form-control" type="number"  class="form-control" name="repairer_labourRatesTripleCoat" id="repairer_labourRatesTripleCoat" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer kind_costRate" for="repairer_labourRatesTripleCoat15">Triplostrato +15h</label>
<input class="kind_repairer kind_costRate form-control" type="number"  class="form-control" name="repairer_labourRatesTripleCoat15" id="repairer_labourRatesTripleCoat15" placeholder=""></title>
</div></div>
<div class="form-row labourRates"><div class="form-group col-md-6">
<label class="kind_repairer kind_costRate" for="repairer_labourRatesQuadCoat">Quadruplostrato</label>
<input class="kind_repairer kind_costRate form-control" type="number"  class="form-control" name="repairer_labourRatesQuadCoat" id="repairer_labourRatesQuadCoat" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer kind_costRate" for="repairer_labourRatesQuadCoat15">Quadruplostrato +15h</label>
<input class="kind_repairer kind_costRate form-control" type="number"  class="form-control" name="repairer_labourRatesQuadCoat15" id="repairer_labourRatesQuadCoat15" placeholder=""></title>
</div></div>
<div class="form-row labourRates"><div class="form-group col-md-6">
<label class="kind_repairer kind_costRate" for="repairer_labourRatesWasteDisposal">Smaltimento rifiuti %</label>
<input class="kind_repairer kind_costRate form-control" type="number"   class="form-control" name="repairer_labourRatesWasteDisposal" id="repairer_labourRatesWasteDisposal" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer kind_costRate" for="repairer_labourRatesWasteDisposalMax">Smaltimento rifiuti max €</label>
<input class="kind_repairer kind_costRate form-control" type="number" step=".01" class="form-control" name="repairer_labourRatesWasteDisposalMax" id="repairer_labourRatesWasteDisposalMax" placeholder=""></title>
</div></div>

 
</form>
<div class="modal  fade" id="calcoloCF" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content event-selector">
      <div class="modal-header">
        <h4 class="modal-title" id="calcoloCFTitle">Calcola <b> Codice Fiscale</b></h4>
      </div>
      <div class="modal-body">
       <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_repairerCF" for="repairerCF_name">Nome</label>
<input class="kind_repairerCF form-control" type="text" class="form-control" name="repairerCF_name" id="repairerCF_name" placeholder=""></title>
 </div>
 <div class="form-group col-md-6">
<label class="kind_repairerCF" for="repairerCF_surname">Cognome</label>
<input class="kind_repairerCF form-control" type="text" class="form-control" name="repairerCF_surname" id="repairerCF_surname" placeholder=""></title>
 </div></div>
 <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_repairerCF" for="repairerCF_gender" >Sesso</label>
<select class="browser-default custom-select form-control" name="repairerCF_gender" id="repairerCF_gender">
  <option value="M" selected>M</option>
  <option value="F">F</option>
</select></div>
 <div class="form-group col-md-6">
<label class="kind_repairerCF" for="repairerCF_bornAt">Nato a</label>
<input class="kind_repairerCF form-control" type="text" class="form-control" name="repairerCF_bornAt" id="repairerCF_bornAt" placeholder=""></title>
 </div></div>
  <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_repairerCF" for="repairerCF_bornDate">Data di nascita</label>
<input class="kind_repairerCF form-control" type="date" class="form-control" name="repairerCF_bornDate" id="repairerCF_bornDate" placeholder=""></title>
 </div></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="calcolaCF">Calcola</button>
		<input class="kind_repairerCF form-control" type="text" class="form-control" name="repairerCF_Id" id="repairerCF_Id" placeholder=""></title>
         <button type="button" id="copiaCF" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-arrow-right"></i></span>
        </button>
      </div>
    </div>
  </div>
</div>