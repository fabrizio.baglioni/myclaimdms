<?php header('Access-Control-Allow-Origin: *'); ?>
<style>
.h3Selected{
	font-weight:bold;
	color:black;
	cursor: pointer;
}
.h3NotSelected{
	font-weight:unset;
	color:grey;
	cursor: pointer;
}
.labourRates{
	display:none;
}
body{
    background: #f8f8f8;
}
.form-control {
    background-color: #fff;
    color: #363636;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    justify-content: flex-start;
    line-height: 1.5;
    padding: calc(.375em - 1px) calc(.625em - 1px);
    border: 1px transparent;
	display:inline-block !important;
}
label {
    color: #86868b;
    text-transform: uppercase;
    font-size: 11px;
    margin: 0px;
    position: relative;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    float: left;
}
.form-group.col-md-6{
	display:inline-block !important;
}
.hiddenAbsolute{
visibility:hidden;
position:absolute;
}
.modal-content{
background-color:#f8f8f8;
}
</style>
<script>

$(function(){
$(document).ready(function(){
   
$("#datiGeneraliH3").on("click",function(){
	$("#manodoperaH3").removeClass("h3Selected").addClass("h3NotSelected");
	$("#datiGeneraliH3").removeClass("h3NotSelected").addClass("h3Selected");
	$(".form-row").not('.labourRates').show();
	$(".form-row.labourRates").hide();
});
$("#manodoperaH3").on("click",function(){
	$("#manodoperaH3").removeClass("h3NotSelected").addClass("h3Selected");
	$("#datiGeneraliH3").removeClass("h3Selected").addClass("h3NotSelected");
	$(".form-row.labourRates").show();
	$(".form-row").not('.labourRates').hide();
});
});
});
</script>
<form id="placeOfExpertise" class="">
 <div class="form-row2" style="display:none;">
<label class="kind_placeOfExpertise" for="placeOfExpertise_id">Id</label>
<input class="kind_placeOfExpertise form-control" type="text" class="form-control" name="placeOfExpertise_id" id="placeOfExpertise_id" placeholder=""></title>
 </div>
 <div class="form-row">
     <div class="form-group col-md-6">
<label class="kind_placeOfExpertise" for="placeOfExpertise_address">Indirizzo</label>
<input class="kind_placeOfExpertise form-control" class="form-control" name="placeOfExpertise_address" id="placeOfExpertise_address" placeholder=""></title>
</div>   <div class="form-group col-md-6">
<label class="kind_placeOfExpertise" for="placeOfExpertise_name">Città</label>
<input class="kind_placeOfExpertise form-control" type="text" class="form-control" name="placeOfExpertise_name" id="placeOfExpertise_name" placeholder=""></title>
 </div></div>
 	  <div class="form-row">
  <div class="form-group col-md-6">
<label class="kind_placeOfExpertise" for="placeOfExpertise_zip">Cap</label>
<input class="kind_placeOfExpertise form-control" type="tel" maxlength="5" class="form-control" name="placeOfExpertise_zip" id="placeOfExpertise_zip" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_placeOfExpertise" for="placeOfExpertise_province"  >Provincia</label>
<input class="kind_placeOfExpertise form-control" type="text" maxlength="2" style="text-transform: uppercase;" onkeyup="this.value = this.value.toUpperCase();" class="form-control" name="placeOfExpertise_province" id="placeOfExpertise_province" placeholder=""></title>
</div> </div>
<div class="form-row"><div class="form-group col-md-6">
<label class="kind_placeOfExpertise" for="placeOfExpertise_notes">Note</label>
<i class="fas fa-pencil-alt prefix"></i>
<input class="kind_placeOfExpertise form-control md-textarea" type="text" class="form-control" name="placeOfExpertise_notes" id="placeOfExpertise_notes" placeholder=""></title>
</div></div>

</form>
