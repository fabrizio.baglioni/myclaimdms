<?php header('Access-Control-Allow-Origin: *'); ?>
<style>
.h3Selected{
	font-weight:bold;
	color:black;
	cursor: pointer;
}
.h3NotSelected{
	font-weight:unset;
	color:grey;
	cursor: pointer;
}
.labourRates{
	display:none;
}
body{
    background: #f8f8f8;
}
.form-control {
    background-color: #fff;
    color: #363636;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    justify-content: flex-start;
    line-height: 1.5;
    padding: calc(.375em - 1px) calc(.625em - 1px);
    border: 1px transparent;
	display:inline-block !important;
}
label {
    color: #86868b;
    text-transform: uppercase;
    font-size: 11px;
    margin: 0px;
    position: relative;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    float: left;
}
.form-group.col-md-6{
	display:inline-block !important;
}
.hiddenAbsolute{
visibility:hidden;
position:absolute;
}
</style>
<script>
$("#datiGeneraliH3").on("click",function(){
	$("#manodoperaH3").removeClass("h3Selected").addClass("h3NotSelected");
	$("#datiGeneraliH3").removeClass("h3NotSelected").addClass("h3Selected");
	$(".form-row").not('.labourRates').show();
	$(".form-row.labourRates").hide();
});
$("#manodoperaH3").on("click",function(){
	$("#manodoperaH3").removeClass("h3NotSelected").addClass("h3Selected");
	$("#datiGeneraliH3").removeClass("h3Selected").addClass("h3NotSelected");
	$(".form-row.labourRates").show();
	$(".form-row").not('.labourRates').hide();
});
</script>
<form id="repairer" class="">
 <div class="form-row2" style="display:none;">
<label class="kind_repairer" for="repairer_id">Id</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_id" id="repairer_id" placeholder=""></title>
 </div>
 <h3 id="datiGeneraliH3" class="h3Selected" style="display:inline-block;width:auto">Dati Generali</h3><h3 style="display:inline-block;width:auto;margin-left:10px" class="h3NotSelected" id="manodoperaH3">Manodopera</h3>
 <div class="form-row">
        <div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_name">Nome</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_name" id="repairer_name" placeholder=""></title>
 </div>
  <div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_code">Codice</label>
<input class="kind_repairer form-control" type="number" class="form-control" name="repairer_code" id="repairer_code" placeholder=""></title>
</div>
      </div>
	  <div class="form-row">
        <div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_city">Città</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_city" id="repairer_city" placeholder=""></title>
 </div></div>
 	  <div class="form-row">
  <div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_zip">Cap</label>
<input class="kind_repairer form-control" type="tel" maxlength="5" class="form-control" name="repairer_zip" id="repairer_zip" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_province"  >Provincia</label>
<input class="kind_repairer form-control" type="text" maxlength="2" style="text-transform: uppercase;" onkeyup="this.value = this.value.toUpperCase();" class="form-control" name="repairer_province" id="repairer_province" placeholder=""></title>
</div>
      </div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_country">Stato</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_country" id="repairer_country" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_phone">Telefono</label>
<input class="kind_repairer form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="repairer_phone" id="repairer_phone" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_mobilePhone">Cellulare</label>
<input class="kind_repairer form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="repairer_mobilePhone" id="repairer_mobilePhone" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_idVatNumber">Cod.Fiscale</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_idVatNumber" id="repairer_idVatNumber" placeholder=""><i class="fas fa-id-card-alt"></i></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_vatNumber">P.Iva</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_vatNumber" id="repairer_vatNumber" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_mail">Email</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_mail" id="repairer_mail" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_pec">PEC</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_pec" id="repairer_pec" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_zone">Zona</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_zone" id="repairer_zone" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_bank">Banca</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_bank" id="repairer_bank" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_accountCountry">Nazione</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_accountCountry" id="repairer_accountCountry" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_cin">Codice CIN</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_cin" id="repairer_cin" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_abi">A.B.I.</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_abi" id="repairer_abi" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_cab">C.A.B.</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_cab" id="repairer_cab" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_cc">C.C.</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_cc" id="repairer_cc" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_iban">IBAN</label>
<input class="kind_repairer form-control" type="text" class="form-control" name="repairer_iban" id="repairer_iban" placeholder=""></title>
</div></div>
<div class="form-row labourRates"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_labourRatesBody">Mdo Carrozzeria</label>
<input class="kind_repairer form-control" type="number" step=".01" class="form-control" name="repairer_labourRatesBody" id="repairer_labourRatesBody" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_labourRatesMec">Mdo Meccanica</label>
<input class="kind_repairer form-control" type="number" step=".01" class="form-control" name="repairer_labourRatesMec" id="repairer_labourRatesMec" placeholder=""></title>
</div></div>
<div class="form-row labourRates"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_labourRatesTotalDiscount">Sconto su totale%</label>
<input class="kind_repairer form-control" type="number"  class="form-control" name="repairer_labourRatesTotalDiscount" id="repairer_labourRatesTotalDiscount" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_labourRatesSPDiscount">Sconto su ricambi%</label>
<input class="kind_repairer form-control" type="number"  class="form-control" name="repairer_labourRatesSPDiscount" id="repairer_labourRatesSPDiscount" placeholder=""></title>
</div></div>
<div class="form-row labourRates"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_labourRatesDoubleCoat">Doppiostrato</label>
<input class="kind_repairer form-control" type="number"  class="form-control" name="repairer_labourRatesDoubleCoat" id="repairer_labourRatesDoubleCoat" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_labourRatesDoubleCoat15">Doppiostrato +15h</label>
<input class="kind_repairer form-control" type="number"  class="form-control" name="repairer_labourRatesDoubleCoat15" id="repairer_labourRatesDoubleCoat15" placeholder=""></title>
</div></div>
<div class="form-row labourRates"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_labourRatesTripleCoat">Triplostrato</label>
<input class="kind_repairer form-control" type="number"  class="form-control" name="repairer_labourRatesTripleCoat" id="repairer_labourRatesTripleCoat" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_labourRatesTripleCoat15">Triplostrato +15h</label>
<input class="kind_repairer form-control" type="number"  class="form-control" name="repairer_labourRatesTripleCoat15" id="repairer_labourRatesTripleCoat15" placeholder=""></title>
</div></div>
<div class="form-row labourRates"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_labourRatesQuadCoat">Quadruplostrato</label>
<input class="kind_repairer form-control" type="number"  class="form-control" name="repairer_labourRatesQuadCoat" id="repairer_labourRatesQuadCoat" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_labourRatesQuadCoat15">Quadruplostrato +15h</label>
<input class="kind_repairer form-control" type="number"  class="form-control" name="repairer_labourRatesQuadCoat15" id="repairer_labourRatesQuadCoat15" placeholder=""></title>
</div></div>
<div class="form-row labourRates"><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_labourRatesWasteDisposal">Smaltimento rifiuti %</label>
<input class="kind_repairer form-control" type="number"   class="form-control" name="repairer_labourRatesWasteDisposal" id="repairer_labourRatesWasteDisposal" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_repairer" for="repairer_labourRatesWasteDisposalMax">Smaltimento rifiuti max €</label>
<input class="kind_repairer form-control" type="number" step=".01" class="form-control" name="repairer_labourRatesWasteDisposalMax" id="repairer_labourRatesWasteDisposalMax" placeholder=""></title>
</div></div>
</form>