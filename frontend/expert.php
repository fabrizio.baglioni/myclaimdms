<?php header('Access-Control-Allow-Origin: *'); ?>
<style>
.h3Selected{
	font-weight:bold;
	color:black;
	cursor: pointer;
}
.h3NotSelected{
	font-weight:unset;
	color:grey;
	cursor: pointer;
}
.labourRates{
	display:none;
}
body{
    background: #f8f8f8;
}
.form-control {
    background-color: #fff;
    color: #363636;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    justify-content: flex-start;
    line-height: 1.5;
    padding: calc(.375em - 1px) calc(.625em - 1px);
    border: 1px transparent;
	display:inline-block !important;
}
label {
    color: #86868b;
    text-transform: uppercase;
    font-size: 11px;
    margin: 0px;
    position: relative;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    float: left;
}
.form-group.col-md-6{
	display:inline-block !important;
}
.hiddenAbsolute{
visibility:hidden;
position:absolute;
}
.modal-content{
background-color:#f8f8f8;
}
</style>
<script>


function calcolaCF(nome, cognome, comune, data, sesso, targetFieldId) {
var ret = '';

$.ajax({
//url: 'http://webservices.dotnethell.it/codicefiscale.asmx/CalcolaCodiceFiscale',
url: 'https://wedat.it/myClaimDMS/calcolaCF.php',
type: "POST",
data: {
Nome: nome,
Cognome: cognome,
ComuneNascita: comune,
DataNascita: data,
Sesso: sesso
},
success: function (result) {
//console.log(result);
//ret = JSON.parse(JSON.stringify(result.getElementsByTagName('string')[0].innerHTML));
//$('#'+targetFieldId).val(ret);
$('#'+targetFieldId).val(result);
},
error: function (result) {
ret = 'Errore di connessione, riprovare';
$('#'+targetFieldId).val(ret);
}
});
}

$(function(){
$(document).ready(function(){
    $('#calcolaCF').on('click', function(){
		function month2digits(month)
    { 
        return (month < 10 ? '0' : '') + month;
    }
var dataDiNascita=$("#expertCF_bornDate").val();
var formattedDate = new Date(dataDiNascita);
var d = formattedDate.getDate();
d= month2digits(d);
var m =  formattedDate.getMonth();
m += 1;  // JavaScript months are 0-11
m= month2digits(m);
var y = formattedDate.getFullYear();
var finalData=d + "/" + m + "/" + y;
        calcolaCF($("#expertCF_name").val(), $("#expertCF_surname").val(), $("#expertCF_bornAt").val(), finalData, $("#expertCF_gender").val(), 'expertCF_Id');
    });

$(document).on('hide.bs.modal', '#frontEnd', function(e) {
	$("#calcoloCF").hide();
});
$(document).on('hide.bs.modal', '#calcoloCF', function(e) {
          console.log('pagehide');
         $("#expert_idVatNumber").val($("#expertCF_Id").val());

      } );
	$("#copiaCF").on("click",function(){ 
         $("#expert_idVatNumber").val($("#expertCF_Id").val());
	});	
$("#datiGeneraliH3").on("click",function(){
	$("#manodoperaH3").removeClass("h3Selected").addClass("h3NotSelected");
	$("#datiGeneraliH3").removeClass("h3NotSelected").addClass("h3Selected");
	$(".form-row").not('.labourRates').show();
	$(".form-row.labourRates").hide();
});
$("#manodoperaH3").on("click",function(){
	$("#manodoperaH3").removeClass("h3NotSelected").addClass("h3Selected");
	$("#datiGeneraliH3").removeClass("h3Selected").addClass("h3NotSelected");
	$(".form-row.labourRates").show();
	$(".form-row").not('.labourRates').hide();
});
});
});
</script>
<form id="expert" class="">
 <div class="form-row2" style="display:none;">
<label class="kind_expert" for="expert_id">Id</label>
<input class="kind_expert form-control" type="text" class="form-control" name="expert_id" id="expert_id" placeholder=""></title>
 </div>
 <div class="form-row">
        <div class="form-group col-md-6">
<label class="kind_expert" for="expert_name">Nome</label>
<input class="kind_expert form-control" type="text" class="form-control" name="expert_name" id="expert_name" placeholder=""></title>
 </div>
  <div class="form-group col-md-6">
<label class="kind_expert" for="expert_code">Ruolo nr.</label>
<input class="kind_expert form-control" type="text" class="form-control" name="expert_code" id="expert_code" placeholder=""></title>
</div>
      </div>
	  <div class="form-row">
        <div class="form-group col-md-6">
<label class="kind_expert" for="expert_city">Città</label>
<input class="kind_expert form-control" type="text" class="form-control" name="expert_city" id="expert_city" placeholder=""></title>
 </div></div>
 	  <div class="form-row">
  <div class="form-group col-md-6">
<label class="kind_expert" for="expert_zip">Cap</label>
<input class="kind_expert form-control" type="tel" maxlength="5" class="form-control" name="expert_zip" id="expert_zip" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_expert" for="expert_province"  >Provincia</label>
<input class="kind_expert form-control" type="text" maxlength="2" style="text-transform: uppercase;" onkeyup="this.value = this.value.toUpperCase();" class="form-control" name="expert_province" id="expert_province" placeholder=""></title>
</div>
      </div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_expert" for="expert_country">Stato</label>
<input class="kind_expert form-control" type="text" class="form-control" name="expert_country" id="expert_country" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_expert" for="expert_phone">Telefono privato</label>
<input class="kind_expert form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="expert_phone" id="expert_phone" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_expert" for="expert_mobilePhone">Telefono aziendale</label>
<input class="kind_expert form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="expert_mobilePhone" id="expert_mobilePhone" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_expert" for="expert_fax">Fax</label>
<input class="kind_expert form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="expert_fax" id="expert_fax" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_expert" for="expert_idVatNumber">Cod.Fiscale</label>
<input class="kind_expert form-control" type="text"  name="expert_idVatNumber" id="expert_idVatNumber" style="width:calc(100% - 40px);" placeholder=""><button style="width:34px;height:34px;float:right;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#calcoloCF">
 <i class="fas fa-id-card-alt" style="position:relative;top:-4px;left:-5px;"></i>
</button></title>
</div><div class="form-group col-md-6">
<label class="kind_expert" for="expert_vatNumber">P.Iva</label>
<input class="kind_expert form-control" type="text" class="form-control" name="expert_vatNumber" id="expert_vatNumber" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_expert" for="expert_mail">Email</label>
<input class="kind_expert form-control" type="text" class="form-control" name="expert_mail" id="expert_mail" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_expert" for="expert_pec">PEC</label>
<input class="kind_expert form-control" type="text" class="form-control" name="expert_pec" id="expert_pec" placeholder=""></title>
</div></div>
<div class="form-row"><div class="form-group col-md-6">
<label class="kind_expert" for="expert_notes">Note</label>
<i class="fas fa-pencil-alt prefix"></i>
<input class="kind_expert form-control md-textarea" type="text" class="form-control" name="expert_notes" id="expert_notes" placeholder=""></title>
</div></div>
 
</form>
<div class="modal  fade" id="calcoloCF" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content event-selector">
      <div class="modal-header">
        <h4 class="modal-title" id="calcoloCFTitle">Calcola <b> Codice Fiscale</b></h4>
      </div>
      <div class="modal-body">
       <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_expertCF" for="expertCF_name">Nome</label>
<input class="kind_expertCF form-control" type="text" class="form-control" name="expertCF_name" id="expertCF_name" placeholder=""></title>
 </div>
 <div class="form-group col-md-6">
<label class="kind_expertCF" for="expertCF_surname">Cognome</label>
<input class="kind_expertCF form-control" type="text" class="form-control" name="expertCF_surname" id="expertCF_surname" placeholder=""></title>
 </div></div>
 <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_expertCF" for="expertCF_gender" >Sesso</label>
<select class="browser-default custom-select form-control" name="expertCF_gender" id="expertCF_gender">
  <option value="M" selected>M</option>
  <option value="F">F</option>
</select></div>
 <div class="form-group col-md-6">
<label class="kind_expertCF" for="expertCF_bornAt">Nato a</label>
<input class="kind_expertCF form-control" type="text" class="form-control" name="expertCF_bornAt" id="expertCF_bornAt" placeholder=""></title>
 </div></div>
  <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_expertCF" for="expertCF_bornDate">Data di nascita</label>
<input class="kind_expertCF form-control" type="date" class="form-control" name="expertCF_bornDate" id="expertCF_bornDate" placeholder=""></title>
 </div></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="calcolaCF">Calcola</button>
		<input class="kind_expertCF form-control" type="text" class="form-control" name="expertCF_Id" id="expertCF_Id" placeholder=""></title>
         <button type="button" id="copiaCF" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-arrow-right"></i></span>
        </button>
      </div>
    </div>
  </div>
</div>