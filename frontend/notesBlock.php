<?php header('Access-Control-Allow-Origin: *'); ?>
<style>
body{
    background: #f8f8f8;
}
.form-control {
    background-color: #fff;
    color: #363636;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    justify-content: flex-start;
    line-height: 1.5;
    padding: calc(.375em - 1px) calc(.625em - 1px);
    border: 1px transparent;
	display:inline-block !important;
}
label {
    color: #86868b;
    text-transform: uppercase;
    font-size: 11px;
    margin: 0px;
    position: relative;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    float: left;
}
.hiddenAbsolute{
visibility:hidden;
position:absolute;
}
</style>
<form id="notesBlock" class="">
 <div class="form-row" style="display:none;">
<label class="kind_notesBlock" for="notesBlock_id">Id</label>
<input class="kind_notesBlock form-control" type="text" class="form-control" name="notesBlock_id" id="notesBlock_id" placeholder=""></title>
 </div><div class="form-row">
<label class="kind_notesBlock" for="notesBlock_name">Identificativo nota</label>
<input class="kind_notesBlock form-control" required="true" type="text" class="form-control" name="notesBlock_name" id="notesBlock_name" placeholder=""></title>
 </div>
 <div class="form-row">
<label class="kind_notesBlock" for="notesBlock_block">Blocco testo</label>
<textarea rows="22" class="kind_notesBlock form-control" type="text" class="form-control" name="notesBlock_block" id="notesBlock_block" placeholder=""></textarea></title>
 </div>
 
</form>