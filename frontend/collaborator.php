<?php header('Access-Control-Allow-Origin: *'); ?>
<style>
.h3Selected{
	font-weight:bold;
	color:black;
	cursor: pointer;
}
.h3NotSelected{
	font-weight:unset;
	color:grey;
	cursor: pointer;
}
.labourRates{
	display:none;
}
body{
    background: #f8f8f8;
}
.form-control {
    background-color: #fff;
    color: #363636;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    justify-content: flex-start;
    line-height: 1.5;
    padding: calc(.375em - 1px) calc(.625em - 1px);
    border: 1px transparent;
	display:inline-block !important;
}
label {
    color: #86868b;
    text-transform: uppercase;
    font-size: 11px;
    margin: 0px;
    position: relative;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    float: left;
}
.form-group.col-md-6{
	display:inline-block !important;
}
.hiddenAbsolute{
visibility:hidden;
position:absolute;
}
.modal-content{
background-color:#f8f8f8;
}
</style>
<script>


function calcolaCF(nome, cognome, comune, data, sesso, targetFieldId) {
var ret = '';

$.ajax({
//url: 'http://webservices.dotnethell.it/codicefiscale.asmx/CalcolaCodiceFiscale',
url: 'https://wedat.it/myClaimDMS/calcolaCF.php',
type: "POST",
data: {
Nome: nome,
Cognome: cognome,
ComuneNascita: comune,
DataNascita: data,
Sesso: sesso
},
success: function (result) {
//console.log(result);
//ret = JSON.parse(JSON.stringify(result.getElementsByTagName('string')[0].innerHTML));
//$('#'+targetFieldId).val(ret);
$('#'+targetFieldId).val(result);
},
error: function (result) {
ret = 'Errore di connessione, riprovare';
$('#'+targetFieldId).val(ret);
}
});
}

$(function(){
$(document).ready(function(){
    $('#calcolaCF').on('click', function(){
		function month2digits(month)
    { 
        return (month < 10 ? '0' : '') + month;
    }
var dataDiNascita=$("#collaboratorCF_bornDate").val();
var formattedDate = new Date(dataDiNascita);
var d = formattedDate.getDate();
d= month2digits(d);
var m =  formattedDate.getMonth();
m += 1;  // JavaScript months are 0-11
m= month2digits(m);
var y = formattedDate.getFullYear();
var finalData=d + "/" + m + "/" + y;
        calcolaCF($("#collaboratorCF_name").val(), $("#collaboratorCF_surname").val(), $("#collaboratorCF_bornAt").val(), finalData, $("#collaboratorCF_gender").val(), 'collaboratorCF_Id');
    });

$(document).on('hide.bs.modal', '#frontEnd', function(e) {
	$("#calcoloCF").hide();
});
$(document).on('hide.bs.modal', '#calcoloCF', function(e) {
          console.log('pagehide');
         $("#collaborator_idVatNumber").val($("#collaboratorCF_Id").val());

      } );
	$("#copiaCF").on("click",function(){ 
         $("#collaborator_idVatNumber").val($("#collaboratorCF_Id").val());
	});	
$("#datiGeneraliH3").on("click",function(){
	$("#manodoperaH3").removeClass("h3Selected").addClass("h3NotSelected");
	$("#datiGeneraliH3").removeClass("h3NotSelected").addClass("h3Selected");
	$(".form-row").not('.labourRates').show();
	$(".form-row.labourRates").hide();
});
$("#manodoperaH3").on("click",function(){
	$("#manodoperaH3").removeClass("h3NotSelected").addClass("h3Selected");
	$("#datiGeneraliH3").removeClass("h3Selected").addClass("h3NotSelected");
	$(".form-row.labourRates").show();
	$(".form-row").not('.labourRates').hide();
});
});
});
</script>
<form id="collaborator" class="">
 <div class="form-row2" style="display:none;">
<label class="kind_collaborator" for="collaborator_id">Id</label>
<input class="kind_collaborator form-control" type="text" class="form-control" name="collaborator_id" id="collaborator_id" placeholder=""></title>
 </div>
 <div class="form-row">
        <div class="form-group col-md-6">
<label class="kind_collaborator" for="collaborator_name">Nome</label>
<input class="kind_collaborator form-control" type="text" class="form-control" name="collaborator_name" id="collaborator_name" placeholder=""></title>
 </div>
  <div class="form-group col-md-6">
<label class="kind_collaborator" for="collaborator_code">Ruolo nr.</label>
<input class="kind_collaborator form-control" type="text" class="form-control" name="collaborator_code" id="collaborator_code" placeholder=""></title>
</div>
      </div>
	  <div class="form-row">
        <div class="form-group col-md-6">
<label class="kind_collaborator" for="collaborator_city">Città</label>
<input class="kind_collaborator form-control" type="text" class="form-control" name="collaborator_city" id="collaborator_city" placeholder=""></title>
 </div></div>
 	  <div class="form-row">
  <div class="form-group col-md-6">
<label class="kind_collaborator" for="collaborator_zip">Cap</label>
<input class="kind_collaborator form-control" type="tel" maxlength="5" class="form-control" name="collaborator_zip" id="collaborator_zip" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_collaborator" for="collaborator_province"  >Provincia</label>
<input class="kind_collaborator form-control" type="text" maxlength="2" style="text-transform: uppercase;" onkeyup="this.value = this.value.toUpperCase();" class="form-control" name="collaborator_province" id="collaborator_province" placeholder=""></title>
</div>
      </div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_collaborator" for="collaborator_country">Stato</label>
<input class="kind_collaborator form-control" type="text" class="form-control" name="collaborator_country" id="collaborator_country" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_collaborator" for="collaborator_phone">Telefono privato</label>
<input class="kind_collaborator form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="collaborator_phone" id="collaborator_phone" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_collaborator" for="collaborator_mobilePhone">Telefono aziendale</label>
<input class="kind_collaborator form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="collaborator_mobilePhone" id="collaborator_mobilePhone" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_collaborator" for="collaborator_fax">Fax</label>
<input class="kind_collaborator form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="collaborator_fax" id="collaborator_fax" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_collaborator" for="collaborator_idVatNumber">Cod.Fiscale</label>
<input class="kind_collaborator form-control" type="text"  name="collaborator_idVatNumber" id="collaborator_idVatNumber" style="width:calc(100% - 40px);" placeholder=""><button style="width:34px;height:34px;float:right;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#calcoloCF">
 <i class="fas fa-id-card-alt" style="position:relative;top:-4px;left:-5px;"></i>
</button></title>
</div><div class="form-group col-md-6">
<label class="kind_collaborator" for="collaborator_vatNumber">P.Iva</label>
<input class="kind_collaborator form-control" type="text" class="form-control" name="collaborator_vatNumber" id="collaborator_vatNumber" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_collaborator" for="collaborator_mail">Email</label>
<input class="kind_collaborator form-control" type="text" class="form-control" name="collaborator_mail" id="collaborator_mail" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_collaborator" for="collaborator_pec">PEC</label>
<input class="kind_collaborator form-control" type="text" class="form-control" name="collaborator_pec" id="collaborator_pec" placeholder=""></title>
</div></div>
<div class="form-row"><div class="form-group col-md-6">
<label class="kind_collaborator" for="collaborator_notes">Note</label>
<i class="fas fa-pencil-alt prefix"></i>
<input class="kind_collaborator form-control md-textarea" type="text" class="form-control" name="collaborator_notes" id="collaborator_notes" placeholder=""></title>
</div></div>
 
</form>
<div class="modal  fade" id="calcoloCF" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content event-selector">
      <div class="modal-header">
        <h4 class="modal-title" id="calcoloCFTitle">Calcola <b> Codice Fiscale</b></h4>
      </div>
      <div class="modal-body">
       <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_collaboratorCF" for="collaboratorCF_name">Nome</label>
<input class="kind_collaboratorCF form-control" type="text" class="form-control" name="collaboratorCF_name" id="collaboratorCF_name" placeholder=""></title>
 </div>
 <div class="form-group col-md-6">
<label class="kind_collaboratorCF" for="collaboratorCF_surname">Cognome</label>
<input class="kind_collaboratorCF form-control" type="text" class="form-control" name="collaboratorCF_surname" id="collaboratorCF_surname" placeholder=""></title>
 </div></div>
 <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_collaboratorCF" for="collaboratorCF_gender" >Sesso</label>
<select class="browser-default custom-select form-control" name="collaboratorCF_gender" id="collaboratorCF_gender">
  <option value="M" selected>M</option>
  <option value="F">F</option>
</select></div>
 <div class="form-group col-md-6">
<label class="kind_collaboratorCF" for="collaboratorCF_bornAt">Nato a</label>
<input class="kind_collaboratorCF form-control" type="text" class="form-control" name="collaboratorCF_bornAt" id="collaboratorCF_bornAt" placeholder=""></title>
 </div></div>
  <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_collaboratorCF" for="collaboratorCF_bornDate">Data di nascita</label>
<input class="kind_collaboratorCF form-control" type="date" class="form-control" name="collaboratorCF_bornDate" id="collaboratorCF_bornDate" placeholder=""></title>
 </div></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="calcolaCF">Calcola</button>
		<input class="kind_collaboratorCF form-control" type="text" class="form-control" name="collaboratorCF_Id" id="collaboratorCF_Id" placeholder=""></title>
         <button type="button" id="copiaCF" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-arrow-right"></i></span>
        </button>
      </div>
    </div>
  </div>
</div>