<?php header('Access-Control-Allow-Origin: *'); ?>
<style>
    .h3Selected{
        font-weight:bold;
        color:black;
        cursor: pointer;
    }
    .h3NotSelected{
        font-weight:unset;
        color:grey;
        cursor: pointer;
    }
    body{
        background: #f8f8f8;
    }
    .form-control {
        background-color: #fff;
        color: #363636;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        justify-content: flex-start;
        line-height: 1.5;
        padding: calc(.375em - 1px) calc(.625em - 1px);
        border: 1px transparent;
        display:inline-block !important;
    }
    label {
        color: #86868b;
        text-transform: uppercase;
        font-size: 11px;
        margin: 0px;
        position: relative;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        float: left;
    }
    .form-group.col-md-6{
        display:inline-block !important;
    }
    .hiddenAbsolute{
        visibility:hidden;
        position:absolute;
    }
    .modal-content{
        background-color:#f8f8f8;
    }
</style>

<form id="costRate" class="">
    <div class="form-row costRates">
        <div class="form-group col-md-6">
            <label class="kind_costRate" for="costRate_rateSelected">Fascia</label>
            <input class="kind_costRate form-control" type="text" name="costRate_rateSelected" id="costRate_rateSelected" placeholder=""></title>
        </div>
    </div>
    <div class="form-row costRates">
        <div class="form-group col-md-6">
            <label class="kind_costRate" for="costRate_labourRatesBody">Mdo Carrozzeria</label>
            <input class="kind_costRate form-control" type="number" step=".01" class="form-control" name="costRate_labourRatesBody" id="costRate_labourRatesBody" placeholder=""></title>
        </div>
        <div class="form-group col-md-6">
            <label class="kind_costRate" for="costRate_labourRatesMec">Mdo Meccanica</label>
            <input class="kind_costRate form-control" type="number" step=".01" class="form-control" name="costRate_labourRatesMec" id="costRate_labourRatesMec" placeholder=""></title>
        </div>
    </div>
    <div class="form-row costRates">
        <div class="form-group col-md-6">
            <label class="kind_costRate" for="costRate_labourRatesTotalDiscount">Sconto su totale%</label>
            <input class="kind_costRate form-control" type="number"  class="form-control" name="costRate_labourRatesTotalDiscount" id="costRate_labourRatesTotalDiscount" placeholder=""></title>
        </div>
        <div class="form-group col-md-6">
            <label class="kind_costRate" for="costRate_labourRatesSPDiscount">Sconto su ricambi%</label>
            <input class="kind_costRate form-control" type="number"  class="form-control" name="costRate_labourRatesSPDiscount" id="costRate_labourRatesSPDiscount" placeholder=""></title>
        </div>
    </div>
    <div class="form-row costRates">
        <div class="form-group col-md-6">
            <label class="kind_costRate" for="costRate_labourRatesMonoCoat">Monostrato</label>
            <input class="kind_costRate form-control" type="number"  class="form-control" name="costRate_labourRatesMonoCoat" id="costRate_labourRatesMonoCoat" placeholder=""></title>
        </div>
        <div class="form-group col-md-6">
            <label class="kind_costRate" for="costRate_labourRatesMonoCoat15">Monostrato +15h</label>
            <input class="kind_costRate form-control" type="number"  class="form-control" name="costRate_labourRatesMonoCoat15" id="costRate_labourRatesMonoCoat15" placeholder=""></title>
        </div>
    </div>
    <div class="form-row costRates">
        <div class="form-group col-md-6">
            <label class="kind_costRate" for="costRate_labourRatesDoubleCoat">Doppiostrato</label>
            <input class="kind_costRate form-control" type="number"  class="form-control" name="costRate_labourRatesDoubleCoat" id="costRate_labourRatesDoubleCoat" placeholder=""></title>
        </div>
        <div class="form-group col-md-6">
            <label class="kind_costRate" for="costRate_labourRatesDoubleCoat15">Doppiostrato +15h</label>
            <input class="kind_costRate form-control" type="number"  class="form-control" name="costRate_labourRatesDoubleCoat15" id="costRate_labourRatesDoubleCoat15" placeholder=""></title>
        </div>
    </div>
    <div class="form-row costRates">
        <div class="form-group col-md-6">
            <label class="kind_costRate" for="costRate_labourRatesTripleCoat">Triplostrato</label>
            <input class="kind_costRate form-control" type="number"  class="form-control" name="costRate_labourRatesTripleCoat" id="costRate_labourRatesTripleCoat" placeholder=""></title>
        </div>
        <div class="form-group col-md-6">
            <label class="kind_costRate" for="costRate_labourRatesTripleCoat15">Triplostrato +15h</label>
            <input class="kind_costRate form-control" type="number"  class="form-control" name="costRate_labourRatesTripleCoat15" id="costRate_labourRatesTripleCoat15" placeholder=""></title>
        </div>
    </div>
    <div class="form-row costRates">
        <div class="form-group col-md-6">
            <label class="kind_costRate" for="costRate_labourRatesQuadCoat">Quadruplostrato</label>
            <input class="kind_costRate form-control" type="number"  class="form-control" name="costRate_labourRatesQuadCoat" id="costRate_labourRatesQuadCoat" placeholder=""></title>
        </div>
        <div class="form-group col-md-6">
            <label class="kind_costRate" for="costRate_labourRatesQuadCoat15">Quadruplostrato +15h</label>
            <input class="kind_costRate form-control" type="number"  class="form-control" name="costRate_labourRatesQuadCoat15" id="costRate_labourRatesQuadCoat15" placeholder=""></title>
        </div>
    </div>
    <div class="form-row costRates">
        <div class="form-group col-md-6">
            <label class="kind_costRate" for="costRate_labourRatesWasteDisposal">Smaltimento rifiuti %</label>
            <input class="kind_costRate form-control" type="number"   class="form-control" name="costRate_labourRatesWasteDisposal" id="costRate_labourRatesWasteDisposal" placeholder=""></title>
        </div>
        <div class="form-group col-md-6">
            <label class="kind_costRate" for="costRate_labourRatesWasteDisposalMax">Smaltimento rifiuti max €</label>
            <input class="kind_costRate form-control" type="number" step=".01" class="form-control" name="costRate_labourRatesWasteDisposalMax" id="costRate_labourRatesWasteDisposalMax" placeholder=""></title>
        </div>
    </div>
</form>

