<?php header('Access-Control-Allow-Origin: *'); ?>
<style>
.h3Selected{
	font-weight:bold;
	color:black;
	cursor: pointer;
}
.h3NotSelected{
	font-weight:unset;
	color:grey;
	cursor: pointer;
}
.labourRates{
	display:none;
}
body{
    background: #f8f8f8;
}
.form-control {
    background-color: #fff;
    color: #363636;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    justify-content: flex-start;
    line-height: 1.5;
    padding: calc(.375em - 1px) calc(.625em - 1px);
    border: 1px transparent;
	display:inline-block !important;
}
label {
    color: #86868b;
    text-transform: uppercase;
    font-size: 11px;
    margin: 0px;
    position: relative;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    float: left;
}
.form-group.col-md-6{
	display:inline-block !important;
}
.hiddenAbsolute{
visibility:hidden;
position:absolute;
}
.modal-content{
background-color:#f8f8f8;
}
</style>
<script>


function calcolaCF(nome, cognome, comune, data, sesso, targetFieldId) {
var ret = '';

$.ajax({
//url: 'http://webservices.dotnethell.it/codicefiscale.asmx/CalcolaCodiceFiscale',
url: 'https://wedat.it/myClaimDMS/calcolaCF.php',
type: "POST",
data: {
Nome: nome,
Cognome: cognome,
ComuneNascita: comune,
DataNascita: data,
Sesso: sesso
},
success: function (result) {
//console.log(result);
//ret = JSON.parse(JSON.stringify(result.getElementsByTagName('string')[0].innerHTML));
//$('#'+targetFieldId).val(ret);
$('#'+targetFieldId).val(result);
},
error: function (result) {
ret = 'Errore di connessione, riprovare';
$('#'+targetFieldId).val(ret);
}
});
}

$(function(){
$(document).ready(function(){
    $('#calcolaCF').on('click', function(){
		function month2digits(month)
    { 
        return (month < 10 ? '0' : '') + month;
    }
var dataDiNascita=$("#opponent_inspectorateCF_bornDate").val();
var formattedDate = new Date(dataDiNascita);
var d = formattedDate.getDate();
d= month2digits(d);
var m =  formattedDate.getMonth();
m += 1;  // JavaScript months are 0-11
m= month2digits(m);
var y = formattedDate.getFullYear();
var finalData=d + "/" + m + "/" + y;
        calcolaCF($("#opponent_inspectorateCF_name").val(), $("#opponent_inspectorateCF_surname").val(), $("#opponent_inspectorateCF_bornAt").val(), finalData, $("#opponent_inspectorateCF_gender").val(), 'opponent_inspectorateCF_Id');
    });

$(document).on('hide.bs.modal', '#frontEnd', function(e) {
	$("#calcoloCF").hide();
});
$(document).on('hide.bs.modal', '#calcoloCF', function(e) {
          console.log('pagehide');
         $("#opponent_inspectorate_idVatNumber").val($("#opponent_inspectorateCF_Id").val());

      } );
	$("#copiaCF").on("click",function(){ 
         $("#opponent_inspectorate_idVatNumber").val($("#opponent_inspectorateCF_Id").val());
	});	
$("#datiGeneraliH3").on("click",function(){
	$("#manodoperaH3").removeClass("h3Selected").addClass("h3NotSelected");
	$("#datiGeneraliH3").removeClass("h3NotSelected").addClass("h3Selected");
	$(".form-row").not('.labourRates').show();
	$(".form-row.labourRates").hide();
});
$("#manodoperaH3").on("click",function(){
	$("#manodoperaH3").removeClass("h3NotSelected").addClass("h3Selected");
	$("#datiGeneraliH3").removeClass("h3Selected").addClass("h3NotSelected");
	$(".form-row.labourRates").show();
	$(".form-row").not('.labourRates').hide();
});
});
});
</script>
<form id="opponent_inspectorate" class="">
 <div class="form-row2" style="display:none;">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_id">Id</label>
<input class="kind_opponent_inspectorate form-control" type="text" class="form-control" name="opponent_inspectorate_id" id="opponent_inspectorate_id" placeholder=""></title>
 </div>
 <div class="form-row">
        <div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_name">Nome</label>
<input class="kind_opponent_inspectorate form-control" type="text" class="form-control" name="opponent_inspectorate_name" id="opponent_inspectorate_name" placeholder=""></title>
 </div>
  <div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_code">Codice</label>
<input class="kind_opponent_inspectorate form-control" type="number" class="form-control" name="opponent_inspectorate_code" id="opponent_inspectorate_code" placeholder=""></title>
</div>
      </div>
	  <div class="form-row">
        <div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_city">Città</label>
<input class="kind_opponent_inspectorate form-control" type="text" class="form-control" name="opponent_inspectorate_city" id="opponent_inspectorate_city" placeholder=""></title>
 </div></div>
 	  <div class="form-row">
  <div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_zip">Cap</label>
<input class="kind_opponent_inspectorate form-control" type="tel" maxlength="5" class="form-control" name="opponent_inspectorate_zip" id="opponent_inspectorate_zip" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_province"  >Provincia</label>
<input class="kind_opponent_inspectorate form-control" type="text" maxlength="2" style="text-transform: uppercase;" onkeyup="this.value = this.value.toUpperCase();" class="form-control" name="opponent_inspectorate_province" id="opponent_inspectorate_province" placeholder=""></title>
</div>
      </div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_country">Stato</label>
<input class="kind_opponent_inspectorate form-control" type="text" class="form-control" name="opponent_inspectorate_country" id="opponent_inspectorate_country" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_phone">Telefono</label>
<input class="kind_opponent_inspectorate form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="opponent_inspectorate_phone" id="opponent_inspectorate_phone" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_mobilePhone">Cellulare</label>
<input class="kind_opponent_inspectorate form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="opponent_inspectorate_mobilePhone" id="opponent_inspectorate_mobilePhone" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_fax">Fax</label>
<input class="kind_opponent_inspectorate form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="opponent_inspectorate_fax" id="opponent_inspectorate_fax" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_idVatNumber">Cod.Fiscale</label>
<input class="kind_opponent_inspectorate form-control" type="text"  name="opponent_inspectorate_idVatNumber" id="opponent_inspectorate_idVatNumber" style="width:calc(100% - 40px);" placeholder=""><button style="width:34px;height:34px;float:right;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#calcoloCF">
 <i class="fas fa-id-card-alt" style="position:relative;top:-4px;left:-5px;"></i>
</button></title>
</div><div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_vatNumber">P.Iva</label>
<input class="kind_opponent_inspectorate form-control" type="text" class="form-control" name="opponent_inspectorate_vatNumber" id="opponent_inspectorate_vatNumber" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_mail">Email</label>
<input class="kind_opponent_inspectorate form-control" type="text" class="form-control" name="opponent_inspectorate_mail" id="opponent_inspectorate_mail" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_pec">PEC</label>
<input class="kind_opponent_inspectorate form-control" type="text" class="form-control" name="opponent_inspectorate_pec" id="opponent_inspectorate_pec" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_bank">Banca</label>
<input class="kind_opponent_inspectorate form-control" type="text" class="form-control" name="opponent_inspectorate_bank" id="opponent_inspectorate_bank" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_swift">SWIFT</label>
<input class="kind_opponent_inspectorate form-control" type="text" class="form-control" pattern="[A-Z]{6,6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3,3}){0,1}" name="opponent_inspectorate_iban" id="opponent_inspectorate_swift" placeholder=""></title>
</div></div>
<div class="form-row"><div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_iban">IBAN</label>
<input class="kind_opponent_inspectorate form-control" type="text" class="form-control" pattern="/^(?:(?:IT|SM)\d{2}[A-Z]\d{22}|CY\d{2}[A-Z]\d{23}|NL\d{2}[A-Z]{4}\d{10}|LV\d{2}[A-Z]{4}\d{13}|(?:BG|BH|GB|IE)\d{2}[A-Z]{4}\d{14}|GI\d{2}[A-Z]{4}\d{15}|RO\d{2}[A-Z]{4}\d{16}|KW\d{2}[A-Z]{4}\d{22}|MT\d{2}[A-Z]{4}\d{23}|NO\d{13}|(?:DK|FI|GL|FO)\d{16}|MK\d{17}|(?:AT|EE|KZ|LU|XK)\d{18}|(?:BA|HR|LI|CH|CR)\d{19}|(?:GE|DE|LT|ME|RS)\d{20}|IL\d{21}|(?:AD|CZ|ES|MD|SA)\d{22}|PT\d{23}|(?:BE|IS)\d{24}|(?:FR|MR|MC)\d{25}|(?:AL|DO|LB|PL)\d{26}|(?:AZ|HU)\d{27}|(?:GR|MU)\d{28})$/i" name="opponent_inspectorate_iban" id="opponent_inspectorate_iban" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_opponent_inspectorate" for="opponent_inspectorate_notes">Note</label>
<i class="fas fa-pencil-alt prefix"></i>
<input class="kind_opponent_inspectorate form-control md-textarea" type="text" class="form-control" name="opponent_inspectorate_notes" id="opponent_inspectorate_notes" placeholder=""></title>
</div></div>
 
</form>
<div class="modal  fade" id="calcoloCF" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content event-selector">
      <div class="modal-header">
        <h4 class="modal-title" id="calcoloCFTitle">Calcola <b> Codice Fiscale</b></h4>
      </div>
      <div class="modal-body">
       <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_opponent_inspectorateCF" for="opponent_inspectorateCF_name">Nome</label>
<input class="kind_opponent_inspectorateCF form-control" type="text" class="form-control" name="opponent_inspectorateCF_name" id="opponent_inspectorateCF_name" placeholder=""></title>
 </div>
 <div class="form-group col-md-6">
<label class="kind_opponent_inspectorateCF" for="opponent_inspectorateCF_surname">Cognome</label>
<input class="kind_opponent_inspectorateCF form-control" type="text" class="form-control" name="opponent_inspectorateCF_surname" id="opponent_inspectorateCF_surname" placeholder=""></title>
 </div></div>
 <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_opponent_inspectorateCF" for="opponent_inspectorateCF_gender" >Sesso</label>
<select class="browser-default custom-select form-control" name="opponent_inspectorateCF_gender" id="opponent_inspectorateCF_gender">
  <option value="M" selected>M</option>
  <option value="F">F</option>
</select></div>
 <div class="form-group col-md-6">
<label class="kind_opponent_inspectorateCF" for="opponent_inspectorateCF_bornAt">Nato a</label>
<input class="kind_opponent_inspectorateCF form-control" type="text" class="form-control" name="opponent_inspectorateCF_bornAt" id="opponent_inspectorateCF_bornAt" placeholder=""></title>
 </div></div>
  <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_opponent_inspectorateCF" for="opponent_inspectorateCF_bornDate">Data di nascita</label>
<input class="kind_opponent_inspectorateCF form-control" type="date" class="form-control" name="opponent_inspectorateCF_bornDate" id="opponent_inspectorateCF_bornDate" placeholder=""></title>
 </div></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="calcolaCF">Calcola</button>
		<input class="kind_opponent_inspectorateCF form-control" type="text" class="form-control" name="opponent_inspectorateCF_Id" id="opponent_inspectorateCF_Id" placeholder=""></title>
         <button type="button" id="copiaCF" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-arrow-right"></i></span>
        </button>
      </div>
    </div>
  </div>
</div>