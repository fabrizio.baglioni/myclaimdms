<?php header('Access-Control-Allow-Origin: *'); ?>
<style>
.h3Selected{
	font-weight:bold;
	color:black;
	cursor: pointer;
}
.h3NotSelected{
	font-weight:unset;
	color:grey;
	cursor: pointer;
}
.labourRates{
	display:none;
}
body{
    background: #f8f8f8;
}
.form-control {
    background-color: #fff;
    color: #363636;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    justify-content: flex-start;
    line-height: 1.5;
    padding: calc(.375em - 1px) calc(.625em - 1px);
    border: 1px transparent;
	display:inline-block !important;
}
label {
    color: #86868b;
    text-transform: uppercase;
    font-size: 11px;
    margin: 0px;
    position: relative;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    float: left;
}
.form-group.col-md-6{
	display:inline-block !important;
}
.hiddenAbsolute{
visibility:hidden;
position:absolute;
}
.modal-content{
background-color:#f8f8f8;
}
</style>
<script>


function calcolaCF(nome, cognome, comune, data, sesso, targetFieldId) {
var ret = '';

$.ajax({
//url: 'http://webservices.dotnethell.it/codicefiscale.asmx/CalcolaCodiceFiscale',
url: 'https://wedat.it/myClaimDMS/calcolaCF.php',
type: "POST",
data: {
Nome: nome,
Cognome: cognome,
ComuneNascita: comune,
DataNascita: data,
Sesso: sesso
},
success: function (result) {
//console.log(result);
//ret = JSON.parse(JSON.stringify(result.getElementsByTagName('string')[0].innerHTML));
//$('#'+targetFieldId).val(ret);
$('#'+targetFieldId).val(result);
},
error: function (result) {
ret = 'Errore di connessione, riprovare';
$('#'+targetFieldId).val(ret);
}
});
}

$(function(){
$(document).ready(function(){
    $('#calcolaCF').on('click', function(){
		function month2digits(month)
    { 
        return (month < 10 ? '0' : '') + month;
    }
var dataDiNascita=$("#co_ownerCF_bornDate").val();
var formattedDate = new Date(dataDiNascita);
var d = formattedDate.getDate();
d= month2digits(d);
var m =  formattedDate.getMonth();
m += 1;  // JavaScript months are 0-11
m= month2digits(m);
var y = formattedDate.getFullYear();
var finalData=d + "/" + m + "/" + y;
        calcolaCF($("#co_ownerCF_name").val(), $("#co_ownerCF_surname").val(), $("#co_ownerCF_bornAt").val(), finalData, $("#co_ownerCF_gender").val(), 'co_ownerCF_Id');
    });

$(document).on('hide.bs.modal', '#frontEnd', function(e) {
	$("#calcoloCF").hide();
});
$(document).on('hide.bs.modal', '#calcoloCF', function(e) {
          console.log('pagehide');
         $("#co_owner_idVatNumber").val($("#co_ownerCF_Id").val());

      } );
	$("#copiaCF").on("click",function(){ 
         $("#co_owner_idVatNumber").val($("#co_ownerCF_Id").val());
	});	
$("#datiGeneraliH3").on("click",function(){
	$("#manodoperaH3").removeClass("h3Selected").addClass("h3NotSelected");
	$("#datiGeneraliH3").removeClass("h3NotSelected").addClass("h3Selected");
	$(".form-row").not('.labourRates').show();
	$(".form-row.labourRates").hide();
});
$("#manodoperaH3").on("click",function(){
	$("#manodoperaH3").removeClass("h3NotSelected").addClass("h3Selected");
	$("#datiGeneraliH3").removeClass("h3Selected").addClass("h3NotSelected");
	$(".form-row.labourRates").show();
	$(".form-row").not('.labourRates').hide();
});
});
});
</script>
<form id="co_owner" class="">
 <div class="form-row2" style="display:none;">
<label class="kind_co_owner" for="co_owner_id">Id</label>
<input class="kind_co_owner form-control" type="text" name="co_owner_id" id="co_owner_id" placeholder=""></title>
 </div>
  <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_kind" >Tipo</label>
<select class="browser-default custom-select form-control" name="co_owner_kind" id="co_owner_kind">
  <option value="Locatario" selected>Locatario</option>
  <option value="Comproprietario">Comproprietario</option>
</select></div></div>
 <div class="form-row">
        <div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_name">Nome</label>
<input class="kind_co_owner form-control" type="text" name="co_owner_name" id="co_owner_name" placeholder=""></title>
 </div>
  <div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_surname">Cognome</label>
<input class="kind_co_owner form-control" type="text" name="co_owner_surname" id="co_owner_surname" placeholder=""></title>
</div>
      </div>
	  <div class="form-row">
      <div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_address">Indirizzo</label>
<input class="kind_co_owner form-control" type="text" name="co_owner_address" id="co_owner_address" placeholder=""></title>
 </div><div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_city">Città</label>
<input class="kind_co_owner form-control" type="text" name="co_owner_city" id="co_owner_city" placeholder=""></title>
 </div></div>
 	  <div class="form-row">
  <div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_zip">Cap</label>
<input class="kind_co_owner form-control" type="tel" maxlength="5" class="form-control" name="co_owner_zip" id="co_owner_zip" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_province"  >Provincia</label>
<input class="kind_co_owner form-control" type="text" maxlength="2" style="text-transform: uppercase;" onkeyup="this.value = this.value.toUpperCase();" class="form-control" name="co_owner_province" id="co_owner_province" placeholder=""></title>
</div>
      </div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_country">Stato</label>
<input class="kind_co_owner form-control" type="text" class="form-control" name="co_owner_country" id="co_owner_country" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_phone">Telefono</label>
<input class="kind_co_owner form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="co_owner_phone" id="co_owner_phone" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_mobilePhone">Cellulare</label>
<input class="kind_co_owner form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="co_owner_mobilePhone" id="co_owner_mobilePhone" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_fax">Fax</label>
<input class="kind_co_owner form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="co_owner_fax" id="co_owner_fax" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_idVatNumber">Cod.Fiscale</label>
<input class="kind_co_owner form-control" type="text"  name="co_owner_idVatNumber" id="co_owner_idVatNumber" style="width:calc(100% - 40px);" placeholder=""><button style="width:34px;height:34px;float:right;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#calcoloCF">
 <i class="fas fa-id-card-alt" style="position:relative;top:-4px;left:-5px;"></i>
</button></title>
</div><div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_vatNumber">P.Iva</label>
<input class="kind_co_owner form-control" type="text" class="form-control" name="co_owner_vatNumber" id="co_owner_vatNumber" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_mail">Email</label>
<input class="kind_co_owner form-control" type="text" class="form-control" name="co_owner_mail" id="co_owner_mail" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_pec">PEC</label>
<input class="kind_co_owner form-control" type="text" class="form-control" name="co_owner_pec" id="co_owner_pec" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_bank">Banca</label>
<input class="kind_co_owner form-control" type="text" class="form-control" name="co_owner_bank" id="co_owner_bank" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_swift">SWIFT</label>
<input class="kind_co_owner form-control" type="text" class="form-control" pattern="[A-Z]{6,6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3,3}){0,1}" name="co_owner_iban" id="co_owner_swift" placeholder=""></title>
</div></div>
<div class="form-row"><div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_iban">IBAN</label>
<input class="kind_co_owner form-control" type="text" class="form-control" pattern="/^(?:(?:IT|SM)\d{2}[A-Z]\d{22}|CY\d{2}[A-Z]\d{23}|NL\d{2}[A-Z]{4}\d{10}|LV\d{2}[A-Z]{4}\d{13}|(?:BG|BH|GB|IE)\d{2}[A-Z]{4}\d{14}|GI\d{2}[A-Z]{4}\d{15}|RO\d{2}[A-Z]{4}\d{16}|KW\d{2}[A-Z]{4}\d{22}|MT\d{2}[A-Z]{4}\d{23}|NO\d{13}|(?:DK|FI|GL|FO)\d{16}|MK\d{17}|(?:AT|EE|KZ|LU|XK)\d{18}|(?:BA|HR|LI|CH|CR)\d{19}|(?:GE|DE|LT|ME|RS)\d{20}|IL\d{21}|(?:AD|CZ|ES|MD|SA)\d{22}|PT\d{23}|(?:BE|IS)\d{24}|(?:FR|MR|MC)\d{25}|(?:AL|DO|LB|PL)\d{26}|(?:AZ|HU)\d{27}|(?:GR|MU)\d{28})$/i" name="co_owner_iban" id="co_owner_iban" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_co_owner" for="co_owner_notes">Note</label>
<i class="fas fa-pencil-alt prefix"></i>
<input class="kind_co_owner form-control md-textarea" type="text" class="form-control" name="co_owner_notes" id="co_owner_notes" placeholder=""></title>
</div></div>
 
</form>
<div class="modal  fade" id="calcoloCF" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content event-selector">
      <div class="modal-header">
        <h4 class="modal-title" id="calcoloCFTitle">Calcola <b> Codice Fiscale</b></h4>
      </div>
      <div class="modal-body">
       <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_co_ownerCF" for="co_ownerCF_name">Nome</label>
<input class="kind_co_ownerCF form-control" type="text" class="form-control" name="co_ownerCF_name" id="co_ownerCF_name" placeholder=""></title>
 </div>
 <div class="form-group col-md-6">
<label class="kind_co_ownerCF" for="co_ownerCF_surname">Cognome</label>
<input class="kind_co_ownerCF form-control" type="text" class="form-control" name="co_ownerCF_surname" id="co_ownerCF_surname" placeholder=""></title>
 </div></div>
 <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_co_ownerCF" for="co_ownerCF_gender" >Sesso</label>
<select class="browser-default custom-select form-control" name="co_ownerCF_gender" id="co_ownerCF_gender">
  <option value="M" selected>M</option>
  <option value="F">F</option>
</select></div>
 <div class="form-group col-md-6">
<label class="kind_co_ownerCF" for="co_ownerCF_bornAt">Nato a</label>
<input class="kind_co_ownerCF form-control" type="text" class="form-control" name="co_ownerCF_bornAt" id="co_ownerCF_bornAt" placeholder=""></title>
 </div></div>
  <div class="form-row">
<div class="form-group col-md-6">
<label class="kind_co_ownerCF" for="co_ownerCF_bornDate">Data di nascita</label>
<input class="kind_co_ownerCF form-control" type="date" class="form-control" name="co_ownerCF_bornDate" id="co_ownerCF_bornDate" placeholder=""></title>
 </div></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="calcolaCF">Calcola</button>
		<input class="kind_co_ownerCF form-control" type="text" class="form-control" name="co_ownerCF_Id" id="co_ownerCF_Id" placeholder=""></title>
         <button type="button" id="copiaCF" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-arrow-right"></i></span>
        </button>
      </div>
    </div>
  </div>
</div>