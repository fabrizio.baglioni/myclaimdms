<?php header('Access-Control-Allow-Origin: *'); ?>
<style>
body{
    background: #f8f8f8;
}
.form-control {
    background-color: #fff;
    color: #363636;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    justify-content: flex-start;
    line-height: 1.5;
    padding: calc(.375em - 1px) calc(.625em - 1px);
    border: 1px transparent;
	display:inline-block !important;
}
label {
    color: #86868b;
    text-transform: uppercase;
    font-size: 11px;
    margin: 0px;
    position: relative;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    float: left;
}
.hiddenAbsolute{
visibility:hidden;
position:absolute;
}
</style>
<form id="authority" class="">
 <div class="form-row" style="display:none;">
<label class="kind_authority" for="authority_id">Id</label>
<input class="kind_authority form-control" type="text" class="form-control" name="authority_id" id="authority_id" placeholder=""></title>
 </div><div class="form-row">
        <div class="form-group col-md-6">
<label class="kind_authority" for="authority_name">Nome</label>
<input class="kind_authority form-control" type="text" class="form-control" name="authority_name" id="authority_name" placeholder=""></title>
 </div>
  <div class="form-group col-md-6">
<label class="kind_authority" for="authority_code">Codice</label>
<input class="kind_authority form-control" type="number" class="form-control" name="authority_code" id="authority_code" placeholder=""></title>
</div>
      </div>
	  <div class="form-row">
        <div class="form-group col-md-6">
<label class="kind_authority" for="authority_city">Città</label>
<input class="kind_authority form-control" type="text" class="form-control" name="authority_city" id="authority_city" placeholder=""></title>
 </div></div>
 	  <div class="form-row">
  <div class="form-group col-md-6">
<label class="kind_authority" for="authority_zip" maxlength="5">Cap</label>
<input class="kind_authority form-control" type="text" class="form-control" name="authority_zip" id="authority_zip" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_authority" for="authority_province">Provincia</label>
<input class="kind_authority form-control" maxlength="2" type="text" style="text-transform: uppercase;" onkeyup="this.value = this.value.toUpperCase();" class="form-control" name="authority_province" id="authority_province" placeholder=""></title>
</div>
      </div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_authority" for="authority_country">Stato</label>
<input class="kind_authority form-control" type="text" class="form-control" name="authority_country" id="authority_country" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_authority" for="authority_phone">Telefono</label>
<input class="kind_authority form-control" type="tel" pattern="^[0-9-+\s()]*$" class="form-control" name="authority_phone" id="authority_phone" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_authority" for="authority_mobilePhone" >Cellulare</label>
<input class="kind_authority form-control" type="tel" pattern="^[0-9-+\s()]*$"  class="form-control" name="authority_mobilePhone" id="authority_mobilePhone" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_authority" for="authority_idVatNumber">Cod.Fiscale</label>
<input class="kind_authority form-control" type="text" class="form-control" name="authority_idVatNumber" id="authority_idVatNumber" placeholder=""><i class="fas fa-id-card-alt"></i></title>
</div><div class="form-group col-md-6">
<label class="kind_authority" for="authority_vatNumber">P.Iva</label>
<input class="kind_authority form-control" type="text" class="form-control" name="authority_vatNumber" id="authority_vatNumber" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_authority" for="authority_mail">Email</label>
<input class="kind_authority form-control" type="text" class="form-control" name="authority_mail" id="authority_mail" placeholder=""></title>
</div><div class="form-group col-md-6">
<label class="kind_authority" for="authority_pec">PEC</label>
<input class="kind_authority form-control" type="text" class="form-control" name="authority_pec" id="authority_pec" placeholder=""></title>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_authority" for="authority_iban">IBAN</label>
<input class="kind_authority form-control" type="text" class="form-control" name="authority_iban" id="authority_iban" placeholder=""></title>
</div></div>
</div></div><div class="form-row"><div class="form-group col-md-6">
<label class="kind_authority" for="authority_notes">Note</label>
<i class="fas fa-pencil-alt prefix"></i>
<input class="kind_authority form-control md-textarea" type="text" class="form-control" name="authority_notes" id="authority_notes" placeholder=""></title>

</div></div>
</form>