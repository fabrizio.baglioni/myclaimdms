<?php

if (isset($_GET['debug'])) {
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);
}
define('__ROOT__', dirname(__FILE__));
header('Access-Control-Allow-Origin: *');
require_once __ROOT__.'/engine/Utils.php';

//generates a request from GET and POST arrays
use DATConnector\DATConnector;
use Request\Request;
use Request\RequestNotValidException;
use Response\FailureResponse;
use Subject\ModularSubjectFactory as ModularSubjectFactory;
use Subject\SubjectNotValidException;

try {
    Request::validate();   //throws an exception and exit if validation fails
    $masterAcc  = Request::getMasterAccount();
    $kn         = Request::getCustomerNumber();
    $kn        .= getenv('env_suffix');
    $collName   = Request::getCollectionName();
    $fileName   = getenv('mcSbjFile');      //should depend on strategy

    $dossierId = DATConnector::findContractByRef($kn);
    $attachment = DATConnector::getSingleAttachment($dossierId, getenv('mcFldId'), $fileName);

    if (is_null($attachment))   //list of subjects is empty
    {
        echo json_encode([]);
        return;
    }

    /*
    $dossierIdFM  = DATConnector::findContractByRef($masterAcc);
    $definitions = DATConnector::getSingleAttachment($dossierIdFM, getenv('mcFldDef'), getenv('mcFileDef'));
    ModularSubjectFactory::init(json_decode($definitions));
    */

    $lc = \SubjectCollection\CollectionArrayFactory::buildFromJson($attachment);
    $sc = (array_key_exists($collName, $lc))?$lc[$collName]:new \SubjectCollection\SubjectCollection($collName);
    $sc->subCollection = ($collName == 'repairer')?$lc['costRate']:'';

    echo json_encode($sc);
}
catch (RequestNotValidException $e)
{
    echo $e->getMessage();
}
catch (SubjectNotValidException $e)
{
    echo $e->getMessage();
}
catch (\SubjectCollection\SubjectNotFoundException $e) {
    echo $e->getMessage();
}
catch (SoapFault $e)
{
    echo $e->getMessage();
}



