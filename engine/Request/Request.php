<?php

namespace Request;

define('REQ_MASTERACCOUNT',  'mast');
define('REQ_CUSTOMERNUMBER', 'cust');
define('REQ_COLLECTIONNAME', 'coll');
define('REQ_SUBJECT',        'subj');
define('REQ_SUBJECT_ID',     'sbid');


Class Request
{
    /**
     * @throws RequestNotValidException
     */
    public static function validate()
    {
        if (isset($_GET['mode']))
        {
            putenv('mode='.$_GET['mode']);
        }

        return true;
        throw new RequestNotValidException();
    }

    /**
     * @param $fieldName
     * @return mixed
     * @throws RequestNotValidException
     */
    public static function get($fieldName)
    {
        if (isset($_GET[$fieldName]))  return $_GET[$fieldName];
        if (isset($_POST[$fieldName])) return $_POST[$fieldName];

        throw new RequestNotValidException("Parameter $fieldName does not exist neither on POST nor GET arrays.");
    }

    public static function getMasterAccount()
    {
        return self::get(REQ_MASTERACCOUNT);
    }

    public static function getCollectionName()
    {
        return self::get(REQ_COLLECTIONNAME);
        //return $_POST[REQ_COLLECTIONNAME];
    }

    public static function getCustomerNumber()
    {
        return self::get(REQ_CUSTOMERNUMBER);
        //return $_POST[REQ_CUSTOMERNUMBER];
    }

    public static function getSubject()
    {
        return self::get(REQ_SUBJECT);
        //return $_POST[REQ_SUBJECT];
    }

    public static function getSubjectId()
    {
        return self::get(REQ_SUBJECT_ID);
        //return $_POST[REQ_SUBJECT_ID];
    }
}

class RequestNotValidException extends \Exception
{

}