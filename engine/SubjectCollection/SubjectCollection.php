<?php

namespace SubjectCollection;

use Subject\ModularSubjectFactory;
use Subject\ModularSubjectField;
use Subject\Subject;
use Subject\SubjectNotValidException;

/**
 * SUBJECT_TYPE_STRICT  does not allow joining collection from subject of the wrong type     #DEFAULT
 * SUBJECT_TYPE_LOOSE   does allow joining collection from subject of the wrong type
 * SUBJECT_TYPE_SKIP    when composing a collection, simply skips subject of wrong type
 */
define('SUBJECT_TYPE_STRICT',   0);
define('SUBJECT_TYPE_LOOSE',    1);
define('SUBJECT_TYPE_SKIP',     2);

class SubjectNotFoundException extends \Exception
{

}

class SubjectNotAllowedException extends \Exception
{

}

class SubjectCollectionFactory
{
    /**
     * @param $jsonString
     * @return SubjectCollection
     */
    public static function buildFromJson($jsonString, $filterByType):SubjectCollection
    {
        $obj = json_decode($jsonString);
        return self::buildFromJsonObj($obj->$filterByType);
    }

    public static function buildFromJsonObj(\stdClass $obj) {
        $sc = new SubjectCollection($obj->type);

        foreach ($obj->subjects as $id=>$subject)
        {
            $sc->addSubject(\Subject\SubjectFactory::buildFromJsonObj($obj->type, $id, $subject));
        }

        return $sc;
    }

}

class CollectionArrayFactory
{
    /**
     * @param $jsonString
     * @return SubjectCollection[]
     */
    public static function buildFromJson($jsonString):array
    {
        $obj = json_decode($jsonString);
        return self::buildFromJsonObj($obj);
    }

    public static function buildFromJsonObj($obj) {
        $lc = array();

        if (is_null($obj)) return $lc;

        foreach ($obj as $id=>$subject)
        {
            $lc[$id] = SubjectCollectionFactory::buildFromJsonObj($subject);
        }

        return $lc;
    }

}

class SubjectCollection implements \Iterator, \JsonSerializable
{
    private $_position = -1;
    private $_positions = array();
    private $_subjects = array();
    private $_currentPositionKey = 0;
    protected $_type;
    static protected $join = SUBJECT_TYPE_SKIP;
    static private $_maxId = 0;

    public $subCollection;

    public function __construct($type)
    {
        $this->_type = $type;
    }

    public static function joinModeStrict()
    {
        self::$join = SUBJECT_TYPE_STRICT;
    }
    public static function joinModeLoose()
    {
        self::$join = SUBJECT_TYPE_LOOSE;
    }
    public static function joinModeSkip()
    {
        self::$join = SUBJECT_TYPE_SKIP;
    }

    /**
     * @param $subjectId
     * @return mixed
     * @throws SubjectNotFoundException
     */
    public function getSubjectById($subjectId):Subject
    {
        if (!isset($this->_subjects[$subjectId]))
            throw new SubjectNotFoundException($subjectId);
        return $this->_subjects[$subjectId];
    }

    public function addSubjects($subjectsArray)
    {
        /**
         * @var Subject $subject
         */
        foreach ($subjectsArray as $subject) {
            $this->addSubject($subject);
        }

        $this->_position = $this->_positions[$this->_currentPositionKey];
    }

    public function addSubject(Subject $subject)
    {
        if ($subject->getType() != $this->_type)
        {
            switch (self::$join)
            {
                case SUBJECT_TYPE_LOOSE:
                    break;
                case SUBJECT_TYPE_STRICT:
                    throw new SubjectNotAllowedException($subject->getType()." instead of ".$this->_type);
                case SUBJECT_TYPE_SKIP:
                default:
                    return;
            }
        }

        $id = $subject->getId();
        self::$_maxId = max(self::$_maxId, $id);
        if ($id === 0)
        {
            $id = ++self::$_maxId;
            $subject->setId($id);
        }
        $this->_positions[] = $id;
        $this->_subjects[$subject->getId()] = $subject;
        $this->rewind();
    }

    public function removeSubject($subjectId)
    {
        unset($this->_subjects[$subjectId]);
    }

    public function current()
    {
        return $this->_subjects[$this->_position];
    }

    public function next()
    {
        $this->_currentPositionKey++;
        $this->_position = isset($this->_positions[$this->_currentPositionKey])?$this->_positions[$this->_currentPositionKey]:-1;
        return $this->_position;
    }

    public function key()
    {
        return $this->_position;
    }

    public function valid()
    {
        return isset($this->_subjects[$this->_position]);
    }

    public function rewind()
    {
        $this->_currentPositionKey = 0;
        $this->_position = $this->_positions[$this->_currentPositionKey];
    }

    public function jsonSerialize()
    {
        return [
            'type'      => $this->_type,
            'subjects'  => $this->_subjects,
            'subcoll'   => ($this->subCollection!==null)?$this->subCollection:''
        ];
    }
}