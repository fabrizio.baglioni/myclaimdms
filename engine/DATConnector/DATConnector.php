<?php

namespace DATConnector;
require_once __ROOT__.'/vendor/datgroup/postmandat/src/PostmanDAT.php';

use DAT\PostmanDAT\PostmanDAT;

class OfflineConnector extends PostmanDAT
{
    public function mclistContracts($requestData){

        $folder = $requestData->attributes['referenceNumber'];

        if (!is_dir("testJson/".$folder)) $folder = '-1';

        $ret = new \stdClass();
        $ret->return = array();
        $ret->return[0] = new \stdClass();
        $ret->return[0]->contractId = $folder;

        return $ret;
    }

    public function mcCreateOrUpdateContract($requestData) {
        foreach ($requestData->templateData as $obj) {
            if ($obj->key == getenv('mcReferenceName'))
                mkdir("testJson/".$obj->value);
                return $obj->value;
        }
        return '-1';    //error
    }

    public function mcListAttachmentsOfContract(int $contractId, $documentTypes = null, $newerThan = null) {
        $ret = new \stdClass();

        $folderName = "testJson/$contractId/$documentTypes";

        $attachments = new \stdClass();

        if (!$list = @scandir($folderName)) return $attachments;

        array_splice($list,0,2);

        foreach ($list as $attachment)
        {
            $attachments->return[$attachment] = new \stdClass();
            $attachments->return[$attachment]->fileName   = $attachment;
            $attachments->return[$attachment]->binaryData = file_get_contents($folderName.'/'.$attachment);
        }

        return $attachments;
    }

    public function mcUploadAttachmentByIdentification($attachment) {
        $folderName = "testJson/".$attachment->contractId."/".$attachment->attachmentItem->documentType."/";
        file_put_contents($folderName.$attachment->attachmentItem->fileName, $attachment->attachmentItem->binaryData);
    }

    public function mcChangeContractStatus(\stdClass $requestData) {
        echo "<hr><pre>Change Status To: $requestData->statusType</br><hr>";
    }
}

class DATConnector
{
    private static $_postman = null;

    public static function getConnection():PostmanDAT
    {
        if (getenv("mode") == 'offline')
        {
            self::$_postman = new OfflineConnector();
            return self::$_postman;
        }

        if (self::$_postman == null)
        {
            $postman = new PostmanDAT();
            $postman->setCustomerNumber(getenv('mcCstNum'));
            $postman->setLogin(getenv('mcCstLogin'));
            $postman->setCustomerSignature(getenv('mcCstSgn'));
            $postman->setInterfacePartner(getenv('mcIntPtNum'));
            $postman->setInterfacePartnerSignature(getenv('mcIntPtSqn'));
            $postman->setHost(getenv('mcHost'));

            $postman->setPassword(getenv('mcCstPwd'));


            self::$_postman = $postman;
        }
        return self::$_postman;
    }

    public static function findContractByRef($referenceNumber)
    {
        $c = self::getConnection();

        $contract = new \stdClass();
        $contract->pageLimit = 0;
        $contract->pageOffset = 0;
        $contract->attributes = ['referenceNumber' => $referenceNumber];

        $obj = $c->mclistContracts($contract);
        return (empty((array)$obj))?'-1':$obj->return[0]->contractId;
    }

    public static function getClaimAttachments($dossierId, $folderName)
    {
        $c = self::getConnection();

        return $c->mcListAttachmentsOfContract($dossierId, $folderName);
    }

    public static function getSingleAttachment($dossierId, $folderName, $attachmentName)
    {
        $attachments = self::getClaimAttachments($dossierId, $folderName);

        if (empty((array)$attachments)) return null;

        $result = null;

        foreach ($attachments->return as $attachment)
        {
            if (strtolower($attachment->fileName) != strtolower($attachmentName)) continue;

            $result = $attachment->binaryData;
            //since we identified the file with correct name, why not escaping this loop ?
            //because there can be more than one attachment with the same name; going on with the loop ensures to take
            //the last that has been uploaded to the folder
        }

        return $result;
    }

    public static function uploadAttachment($dossierId, $fileName, $mimeType, $folderId, $binarydata)
    {
        $c = self::getConnection();

        $attachment = new \stdClass();
        $attachment->contractId = $dossierId;
        $attachment->attachmentItem = new \stdClass();
        $attachment->attachmentItem->fileName     = $fileName;
        $attachment->attachmentItem->mimeType     = $mimeType;
        $attachment->attachmentItem->binaryData   = $binarydata;
        $attachment->attachmentItem->documentType = $folderId;

        return $c->mcUploadAttachmentByIdentification($attachment);
    }

    public static function changeStatus($dossierId, $destinationStatus)
    {
        $c = self::getConnection();

        $requestData = new \stdClass();
        $requestData->contractId   = $dossierId;
        $requestData->statusType   = $destinationStatus;
        $requestData->templateData = [];

        $c->mcChangeContractStatus($requestData);
    }

    public static function createDossier($referenceNummber) {
        $c = self::getConnection();

        $requestData = new \stdClass();
        $requestData->networkType  = getEnv('mcNetwork');
        $requestData->contractType = getenv('mcCtrType');
        $requestData->templateId   = getenv('mcTemplId');

        $requestData->Dossier = new \stdClass();
        $requestData->Dossier->Country  = getenv('mcCountry');
        $requestData->Dossier->Language = getenv('mcLang');
        $requestData->Dossier->Currency = getenv('mcCurr');

        $requestData->templateData   = [];
        $requestData->templateData[] = $c->createEntry( getenv('mcReferenceName'), $referenceNummber);

        $obj = $c->mcCreateOrUpdateContract($requestData);
        return $obj->return;
    }


}

