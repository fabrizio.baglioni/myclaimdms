<?php

namespace Utils;

switch ($_SERVER['HTTP_HOST']) {
    case 'datgroupsolutions.eu':
        require_once __ROOT__.'/../DatAuthenticator/DatAuthenticator.php';
        break;
    case 'wedat.it':
        require_once __ROOT__.'/../../DatAuthenticator/DatAuthenticator.php';
        break;
    case 'localhost':
    default:
        require_once __ROOT__.'/../DatAuthenticator/DatAuthenticator.php';
}

require_once __ROOT__.'/engine/DATConnector/DATConnector.php';
require_once __ROOT__.'/engine/configurator.php';
require_once __ROOT__.'/engine/SubjectCollection/SubjectCollection.php';
require_once __ROOT__.'/engine/Subject/Subject.php';
require_once __ROOT__.'/engine/Request/Request.php';


if (!function_exists('array_key_first')) {
    function array_key_first(array $arr) {
        foreach($arr as $key => $unused) {
            return $key;
        }
        return NULL;
    }
}

function var_dump($var, $exit=false, $return=false)
{
    $result = sprintf('<pre>%s</pre>', print_r($var, true));

    if($exit)
    {
        echo $result;
        exit;
    }
    if($return)
    {
        return $result;
    }
    echo $result;
}

function json_dump($var, $exit=false, $return=false)
{
    $result = sprintf('<pre>%s</pre>', print_r(json_encode($var), true));

    if($exit)
    {
        echo $result;
        exit;
    }
    if($return)
    {
        return $result;
    }
    echo $result;
}