<?php


namespace Subject;

abstract class Subject implements \JsonSerializable
{
    protected $_type;
    protected $_id;

    abstract function decodeObject(\stdClass $obj);

    public function __construct($type, $id=0)
    {
        $this->_type = $type;
        $this->_id   = $id;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getType()
    {
        return $this->_type;
    }

    public function setType($type)
    {
        $this->_type = $type;
    }

    public function setId($id)
    {
        $this->_id = $id;
    }
}

class SubjectFactory
{
    /**
     * Build a subject starting from a string and a type
     * @param $jsonString
     * @return Subject
     * @throws SubjectNotValidException
     */
    /*
    public static function buildFromJsonString($type, $jsonString):Subject
    {
        $obj = json_decode($jsonString);
        if ($err = json_last_error() != JSON_ERROR_NONE) throw new SubjectNotValidException($err);

        return self::buildFromJsonObj($type, $obj->id, $obj);
    }*/

    public static function buildFromJsonObj($type, $id, $jsonObj):Subject
    {
        $subject = new ModularSubject($type, $id);
        $subject->decodeObject($jsonObj);
        return $subject;


        switch ($type) {
            case 'insurance':
                $sub = new Insurance($type, $id);
                $sub->decodeObject($jsonObj);
                \Utils\var_dump($sub);die;
                return $sub;
            case 'owner':
                $sub = new Owner($type, $id);
                $sub->decodeObject($jsonObj);
                return $sub;
        }


    }

    public static function build($type):Subject
    {
        return new ModularSubject($type);

        switch ($type) {
            case 'insurance':
                return new Insurance($type);
            case 'owner':
                return new Owner($type);
        }
    }

    public static function buildFromModularDefinition(ModularSubject $modularSubject):Subject {
        return new RealSubject($modularSubject);
    }
}


class SubjectNotValidException extends \Exception
{

}

class RealSubject extends Subject {
    private $_definition = null;
    private $_returnJson = [];

    public function __construct(ModularSubject $modularSubject)
    {
        $this->_definition = $modularSubject;
        parent::__construct($modularSubject->getKind());
    }

    function decodeObject(\stdClass $obj)
    {
        try {
            /**
             * @var ModularSubjectField[]
             */
            foreach ($this->_definition->getFields() as $field)
            {
                $name = $field->getName();

                if (!isset($obj->$name)) continue;

                $this->$name = $this->_returnJson[$name] = $obj->$name;
            }

        } catch (\Exception $e) {
            throw new SubjectNotValidException('Invalid JSON object: '.$e->getMessage());
        }
    }

    public function jsonSerialize()
    {
        return $this->_returnJson;
    }
}

class Owner extends Subject
{
    public $firstName;
    public $lastName;

    function decodeObject(\stdClass $obj)
    {
        try {
            //$this->_id       = $obj->id;
            $this->firstName = $obj->firstName;
            $this->lastName  = $obj->lastName;
        } catch (\Exception $e) {
            throw new SubjectNotValidException('Invalid JSON object: '.$e->getMessage());
        }
    }

    public function jsonSerialize()
    {
        return [
            //'id'      => $this->_id,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName
        ];
    }
}

class Insurance extends Subject
{
    public $code;
    public $name;

    /**
     * @param \stdClass $obj
     * @throws SubjectNotValidException
     */
    function decodeObject(\stdClass $obj)
    {
        try {
            //$this->_id  = $obj->id;
            $this->code = $obj->code;
            $this->name = $obj->name;
        } catch (\Exception $e) {
            throw new SubjectNotValidException('Invalid JSON object: '.$e->getMessage());
        }
    }

    public function jsonSerialize()
    {
        return [
            //'id'   => $this->_id,
            'code' => $this->code,
            'name' => $this->name
        ];
    }

}

class ModularSubjectField {
    private $_name;
    private $_type;
    private $_description;
    private $_tooltip;
    private $_value;

    public function __construct($name, $type = 'string', $description = '', $tooltip ='')
    {
        $this->_name        = $name;
        $this->_type        = $type;
        $this->_description = $description;
        $this->_tooltip     = $tooltip;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->_value = $value;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->_name;
    }
}

class ModularSubject extends Subject {
    /**
     * @var ModularSubjectField[]
     */
    private $_fields = array();
    private $_kind = null;
    private $_returnJson = array();

    /**
     * @return null
     */
    public function getKind()
    {
        return $this->_kind;
    }

    /**
     * @param null $kind
     */
    public function setKind($kind)
    {
        $this->_kind = $kind;
    }

    public function addField(ModularSubjectField $field) {
        $this->_fields[$field->getName()] = $field;
    }

    public function getFields()
    {
        return $this->_fields;
    }

    //function decodeObject(\stdClass $obj)
    function decodeObject($obj)
    {
        foreach ($obj as $name=>$field)
        {
            $this->$name = $this->_returnJson[$name] = $field;
        }
    }

    public function jsonSerialize()
    {
        return $this->_returnJson;
    }
}

class ModularSubjectFieldFactory {
    public static function buildFromJsonObj($jsonObj):ModularSubjectField {
        try {
            return new ModularSubjectField(
                $jsonObj->name,
                isset($jsonObj->type)             ?$jsonObj->type:'string',
                isset($jsonObj->description) ?$jsonObj->description:'',
                isset($jsonObj->tooltip)        ?$jsonObj->tooltip:''
            );
        } catch (\Exception $e) {
            throw new SubjectNotValidException('Invalid JSON object for Modular Subject: '.$e->getMessage());
        }
    }
}

class ModularSubjectFactory {

    private static $_init = false;
    private static $_definitionObj;

    public static function init(\stdClass $definitionObj)
    {
        self::$_init = true;
        self::$_definitionObj = $definitionObj;
    }

    public static function getDefinition($kind)
    {
        return self::$_definitionObj->$kind;
    }

    /**
     * @param $kind
     * @return ModularSubject
     * @throws SubjectNotValidException
     */
    public static function buildFromJsonObj($kind):ModularSubject {
        if (!self::$_init)
            throw new SubjectNotValidException('Subject must be first initialized using on '.getenv('mcFileDef').' file');

        $ms = New ModularSubject($kind);
        try {
            foreach (self::getDefinition($kind) as $field)
            {
                $ms->addField(ModularSubjectFieldFactory::buildFromJsonObj($field));
            }
        } catch (\Exception $e) {
            throw new SubjectNotValidException('Invalid JSON object for Modular Subject: '.$e->getMessage());
        }
        return $ms;
    }
}