var mancataRicezione = 'button.core-button.statusActionButton.MancataRicezioneAuto';
var confermaMancataRicezione = '<button id="ConfermaMancataRicezioneAuto" class="statusActionButton" title="Mancata Ricezione Auto" style="position: relative; visibility: visible;" onclick="ConfermaMancataRicezione();"><span class="icon icon-status-out">Mancata Ricezione Auto</span></button>';

var template = null;
var form = null;

$("button.icon.icon-licence-number").hide();

/*FB 20200831 - start*/
function checkNewVsOld(old_field, new_field)
{
	prefix = "#customField-input-";
	old_field = prefix + old_field;
	new_field = prefix + new_field;
	old_field_val = $(old_field).val().replace(".", ",");
	new_field_val = $(new_field).val();
	
	if ($(old_field).val().length>0 && 
		(new_field_val == "" || new_field_val == "0,00"))
		{
			$(new_field).val(old_field_val).change();
		}
}
/*FB 20200831 - end*/

function ConfermaMancataRicezione()
{
	if (confirm("Sicuri di volere cancellare l’incarico?")) {
		$("#ConfermaMancataRicezioneAuto").hide();
		$(mancataRicezione).show().click();
	} else {
		//do nothing
	}
}

function setRecuperoIVA(recupero = false, percentuale = 0)
{
	var val  = "NO";
	var perc = 0;
	if (recupero == true) 
	{
		val  = "SI";
		perc = percentuale;
        $("#customField-control-vatEntitled input[type='radio']:eq(1)").trigger("click");
        //console.log("SI");
	}
	else
    {
        $("#customField-control-vatEntitled input[type='radio']:eq(0)").trigger("click");
        //console.log("NO");
    }

	$("#customField-input-string_vatEntitled").val(val).change();
    $("#customField-input-VATDeduction2").val(perc).change();
}

setTimeout(function(){
	if($(".statusActionsBox").length){
		if ($(mancataRicezione).length)
		{
			$(confermaMancataRicezione).insertBefore(mancataRicezione);
			$(mancataRicezione).hide();
		}
	}
},800);

/* SIMONE LATINA - alternative spare Parts selection NEW VERSION */
var $chPartName;
var $motivation = [];
firstArray = [];
changedArray = [];
if ($("#customField-input-memoAscale2").val() != "" && $("#customField-input-memoAchanged").val() != "") {
    firstArray = JSON.parse($("#customField-input-memoAscale2").val())
    changedArray = JSON.parse($("#customField-input-memoAchanged").val())
}
var flag = false
// on SPO optimization button click
$("#customField-button-SPOptimization").on("click", function() {
    if (!firstArray || firstArray.length === 0) {
        firstArray = [];
    }
    var timeout1 = setInterval(function() {
        if ($("#dialogSPOParts").is(":visible")) {
            $(".spoPartRow td:not(.field-radio)").css("pointer-events", "none");
            defaultArray = []
            if (!flag) {
                firstArray = createArray()
                $("#customField-input-memoAscale2").val(JSON.stringify(firstArray)).change();
                flag = true
            }
            clearInterval(timeout1)
            if ($(".triggerBtn").length != 1) {
                $("#dialogSPOParts").parent().find(".ui-dialog-buttonset").children().eq(1).css({
                    "visibility": "hidden",
                    "position": "absolute"
                });
                $("#dialogSPOParts").parent().find(".ui-dialog-buttonset").before("<button  type='button' class='ui-button ui-corner-all ui-widget triggerBtn'  style='float:right' >Ok</button>")
            }
            //---default suggested parts---
            startArray = createArray()
            //motivation dialog window ---------- 26072019
            $("#dialogSPOContent td.field-radio").on("click", function() {
                $(".warning").remove();
                $chPartName = $(this).closest(".spoPartRow").find(".field-description").text();
                dvn = $(this).parent().prevAll(".spoOE").first().find(".field-dvn").text();
                flagDiff = false;
                for (i = 0; i < startArray.length; i++) {
                    if (startArray[i].indexOf($chPartName) != -1 || firstArray[i].indexOf($chPartName.toUpperCase()) != -1 || firstArray[i].indexOf($chPartName) != -1) {
                        flagDiff = true
                    }
                }
                if (flagDiff == false) {
                    $("#customField-control-memoAscale").before("<div id='changeMotivationDialog' class='motivationDialog' title='Motiva la tua scelta' style='display:none'>");
                    $("#changeMotivationDialog").dialog({
                        modal: true,
                        resizable: true,
                        draggable: true,
                        width: 520,
                        height: "auto",
                        left: 605,
                    });
                    $("#changeMotivationDialog").append("<div class='motivationDialog-content'>");
                    $(".motivationDialog-content").append("<form><p>La preghiamo di motivare la variazione del ricambio proposto<fieldset class='motivation-radio' id='motivation'><input type='radio' name='motivation' id='asked' value='asked'><label for='asked'>Il cliente non accetta il ricambio proposto</label><br/><input type='radio' name='motivation' id='mount' value='mountProblem'><label for='mount'>Il ricambio proposto non pu\u00F2 essere installato sul veicolo</label><br/><input type='radio' name='motivation' id='time' value='timeProblem'><label for='time'>Il ricambio proposto richiede pi\u00F9 tempo per il montaggio sul veicolo</label><br/><input type='radio' name='motivation' id='availability' value='notAvailable'><label for='availability'>Il ricambio non \u00E8 reperibile sul mercato nelle tempistiche standard</label><br/>");
                    $(".motivationDialog").append("<div class='motivationDialog-footer'><button value='ok' class='motivation-btn-ok' id='motivation-ok' style='float: right; margin-top: 5px; width: 46px;'>OK</button></div>");
                    $(".motivationDialog-content").css({
                        "border": "1px solid #213683",
                        "background-color": "#F1F1F1",
                        "padding-right": "20px"
                    });
                    setTimeout(function() {
                        $("#changeMotivationDialog").dialog("open");
                    }, 300);
					/*
                    $(".motivation-radio input").on("change", function() {
                        if ($(".motivation-radio input:checked").val() == "other") {
                            $(".motivation-text").show();
                        } else {
                            $(".motivation-text").hide();
                            $(".motivation-text").removeAttr("placeholder");
                        }
                    });*/
					
                    $("#motivation-ok").on("click", function() {
                        motivDialogArr = [];
                        motivDialogArr.push($chPartName);
                        if (!$(".motivation-radio input").is(":checked")) {
                            $(".motivation-radio").before("<p class='warning' style='color:red'>scegli una motivazione</p>");
                            return;
                        } else if ($(".motivation-radio input:checked").next().text() != "Altro") {
                            motivDialogArr.push($(".motivation-radio input:checked").next().text());
                        } else {
                            if ($(".motivation-text").val() == "") {
                                $(".motivation-text").attr("placeholder", "indica una motivazione");
                                return;
                            } else {
                                motivDialogArr.push($(".motivation-text").val());
                            }
                        }
                        $("#changeMotivationDialog").remove();
                        $motivation.push(motivDialogArr);
                    });
                    $("div[aria-describedby='changeMotivationDialog'] .ui-dialog-titlebar-close").on("click", function() {
                        $("#changeMotivationDialog").remove();
                        returnToStart(dvn, startArray);
                    });
                }
            });
            // trigger ok button
            $(".triggerBtn").on("click", function() {
                firstArray = compareArray(firstArray, startArray);
                $("#customField-input-memoAscale2").val(JSON.stringify(firstArray)).change();
                changedArray = createArray()
                $("#customField-input-memoAchanged").val(JSON.stringify(changedArray)).change();
                isChanged(firstArray, changedArray);
                $("#dialogSPOParts").parent().find(".ui-dialog-buttonset").children().eq(1).click();
            });
        }
    }, 600);
})
//spo button and Dialog window
$("#customField-button-btnSpoChangeDialog").css({
    "color": "white",
    "margin-left": "20px",
    "height": "36px",
    "border": "1px solid red",
    "background-color": "#0d64a7"
});
$("#customField-control-memoAscale").before("<div id='spoChangeDialog' class='spoDialog' title='Voci AlphaScale modificate'>");
$("#customField-button-btnSpoChangeDialog").on("click", function() {
    var intervalB = setInterval(function() {
        if ($("#spoChangeDialog")) {}
        $(".spoDialog-content").remove();
        $("#spoChangeDialog").append("<div class='spoDialog-content'><p><ul>" + $("#customField-input-memoAscale").val() + "</ul></p></div>");
        $(".spoDialog-content").css({
            "border": "1px solid #213683",
            "background-color": "#F1F1F1",
            "padding-right": "20px"
        });
        clearInterval(intervalB)
        $("#spoChangeDialog").dialog({
            modal: true,
            resizable: true,
            draggable: true,
            width: 520,
            height: "auto",
            left: 605,
        });
    }, 200)
});

function updateFromPartSelection(firstArray, changedArray) {
    var rowCounter = $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr").length;
    repairPositions = module.findView("claim").data.claim.activeCalculation.rearrangedCalculationPositions;
    dvnRepairPositions2 = [];
    partNRepairPositions2 = [];
    dvnRepairPositionsDiff = [];
    firstArrayTemp = [];
    for (i = 0; i < repairPositions.length; i++) {
        if (repairPositions[i].dat_process_id != null && repairPositions[i].spare_part_number != null) {
            dvnRepairPositions2.push(repairPositions[i].dat_process_id.toString())
            partNRepairPositions2.push(repairPositions[i].spare_part_number.replace(/\s/g, ''))
        }
    }
    for (i = 0; i < rowCounter; i++) {
        if (partNRepairPositions2.indexOf($("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + (i + 1) + ") > td:nth-child(1) > input").val().replace(/\s/g, '')) != -1) {
            delete dvnRepairPositions2[partNRepairPositions2.indexOf($("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + (i + 1) + ") > td:nth-child(1) > input").val().replace(/\s/g, ''))];
        }
    }
    for (j = 0; j < dvnRepairPositions2.length; j++) {
        if (dvnRepairPositions2[j] != null) {
            dvnRepairPositionsDiff.push(dvnRepairPositions2[j]);
        }
    }
    firstArrayTemp = [];
    for (i = 0; i < firstArray.length; i++) {
        if (dvnRepairPositionsDiff.indexOf(firstArray[i][1]) == -1) {
            firstArrayTemp.push(firstArray[i]);
        }
    }
    firstArray = firstArrayTemp;
    $("#customField-input-memoAscale2").val(JSON.stringify(firstArray)).change();
    isChanged(firstArray, changedArray);
    return firstArray
}
// crete the current list elements Array
function createArray() {
    k = 0
    var partsArray = [];
    for (i = 0; i < ($('#dialogSPOContent > table.table.table-hover > tbody > tr').length); i++) {
        if (!$('#dialogSPOContent > table.table.table-hover > tbody > tr:nth-child(' + (i + 1) + ')').hasClass("noalternatives")) {
            if ($("#dialogSPOContent > table.table.table-hover > tbody > tr:nth-child(" + (i + 1) + ") > td > input").is(":checked")) {
                //alert((i+1)+"checked")
                var dvn = $("#dialogSPOContent > table.table.table-hover > tbody > tr:nth-child(" + (i + 1) + ") > td ").parent().prevAll(".spoOE").first().find(".field-dvn").text()
                partsArray[k] = [];
                for (j = 0; j < ($('#dialogSPOContent > table.table.table-hover > thead > tr > th').length); j++) {
                    var partField = $('tr.spoPartRow:nth-child(' + (i + 1) + ') > td:nth-child(' + (j + 1) + ')').text()
                    partsArray[k][j] = partField;
                    if (partsArray[k][1] == "") {
                        partsArray[k][1] = dvn
                    }
                }
                k++;
            }
        }
    }
    return partsArray;
}
// check if some parts was deleted from part selection an replaced again modifying firstArray
function checkIfDeletedPart(firstArray) {
    repairPositions = module.findView("claim").data.claim.activeCalculation.rearrangedCalculationPositions;
    if (repairPositions != null) {
        dvnRepairPositions = [];
        partNRepairPositions = [];
        dvnFirstArray = [];
        firstArray2 = [];
        for (i = 0; i < repairPositions.length; i++) {
            if (repairPositions[i].dat_process_id != null && repairPositions[i].spare_part_number != null) {
                dvnRepairPositions.push(repairPositions[i].dat_process_id.toString())
                partNRepairPositions.push(repairPositions[i].spare_part_number.replace(/\s/g, ''))
            }
        }
        for (i = 0; i < firstArray.length; i++) {
            dvnFirstArray.push(firstArray[i][1])
        }
        for (i = 0; i < firstArray.length; i++) {
            if (dvnRepairPositions.indexOf(firstArray[i][1]) != -1) {} else {
                delete firstArray[i]
            }
        }
        for (i = 0; i < firstArray.length; i++) {
            if (firstArray[i] != null) {
                firstArray2.push(firstArray[i])
            }
        }
        $("#customField-input-memoAscale2").val(JSON.stringify(firstArray2)).change();
        return firstArray2
    }
    return firstArray;
}
// verify changes in the array containing default elements
function compareArray(firstArray, startArray) {
    dvnStartArray = []
    dvnFirstArray = []
    firstArray2 = []
    for (i = 0; i < startArray.length; i++) {
        dvnStartArray.push(startArray[i][1])
    }
    for (i = 0; i < firstArray.length; i++) {
        dvnFirstArray.push(firstArray[i][1])
    }
    for (i = 0; i < startArray.length; i++) {
        if (dvnFirstArray.indexOf(startArray[i][1]) != -1) {} else {
            firstArray.splice(i, 0, startArray[i])
        }
    }
    for (i = 0; i < firstArray.length; i++) {
        if (dvnStartArray.indexOf(firstArray[i][1]) != -1) {} else {
            delete firstArray[i];
        }
    }
    for (i = 0; i < firstArray.length; i++) {
        if (firstArray[i] != null) {
            firstArray2.push(firstArray[i]);
        }
    }
    $("#customField-input-memoAscale2").val(JSON.stringify(firstArray2)).change();
    return firstArray2;
}
// verify and ricalculate dimension of changedArray  after dimension changes in firstArray
function compareChangedArray(firstArray, changedArray) {
    fromChArray = []
    for (i = 0; i < changedArray.length; i++) {
        for (k = 0; k < firstArray.length; k++) {
            if (changedArray[i][1].indexOf(firstArray[k][1]) != -1) {
                fromChArray.push(changedArray[i]);
            }
        }
    }
    $("#customField-input-memoAchanged").val(JSON.stringify(fromChArray)).change();
    return fromChArray;
}
// compare the default items list with last last list midified by user
function isChanged(firstArray, changedArray) {
    var message = "";
    $("#customField-input-memoAscale").val(message);
    if (firstArray.length !== 0 && changedArray.length !== 0) {
        if (firstArray.length != changedArray.length) {
            changedArray = compareChangedArray(firstArray, changedArray);
        }
        var rowCounter = $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr").length
        for (k = 0; k < firstArray.length; k++) {
            if (changedArray[k][1].indexOf(changedArray[k][1]) != -1 && firstArray[k][2] != changedArray[k][2]) {
                if ($('li[aria-controls="tab-graphicalPartSelection"]').attr("aria-selected") == "false") {
                    // ----26072019-------
                    var flagChanged = false;
                    for (i = 0; i < $motivation.length; i++) {
                        if ($motivation[i][0].indexOf(changedArray[k][3]) != -1) {
                            changedArray[k][13] = $motivation[i][1];
                            $("#customField-input-memoAchanged").val(JSON.stringify(changedArray)).change();
                            flagChanged = true;
                        }
                    }
                    if (flagChanged = true) {
                        message = message + "<li>Il particolare " + firstArray[k][3] + "<br/> \u00E8 stato SOSTITUITO con : " + changedArray[k][3] + "<br/>Motivazione: " + changedArray[k][13];
                    } else {
                        message = message + "<li>Il particolare " + firstArray[k][3] + "<br/> \u00E8 stato SOSTITUITO con : " + changedArray[k][3];
                    }
                    $("#customField-input-memoAscale").val(message);
                    //------- 26072019 end----------
                } else {
                    for (i = 0; i < rowCounter; i++) {
                        if ($("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + (i + 1) + ") > td:nth-child(1) > input").val().replace(/\s/g, '') == changedArray[k][2]) {
                            if ($("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + (i + 1) + ") > td:nth-child(2) > div").text().indexOf("*") == -1) {
                                $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + (i + 1) + ") > td:nth-child(2) > div").append("*")
                            }
                        }
                    }
                }
            } else if (firstArray == changedArray) {
                $("#customField-input-memoAscale").val("");
            }
        }
    }
    return changedArray;
}
//this function return the selction at the prevous status if ther's no motivation added ---------- 26072019
function returnToStart(dvn, startArray) {
    for (i = 0; i < startArray.length; i++) {
        if (startArray[i].indexOf(dvn) != -1) {
            $(" #dialogSPOContent > table.table-hover > tbody > tr > td:contains(" + startArray[i][2] + ")").prev().prev().click();
        }
    }
}

if($("#core-language").val()!="it_IT")
{
	$("#core-language").val("it_IT").change();
	var languageInterval = setInterval(function(){
		if ($(".ui-dialog").contents().find(".ui-dialog-buttonset > button.ui-button:nth-child(1)").is(":visible")) {
			clearInterval(languageInterval);
			$(".ui-dialog").contents().find(".ui-dialog-buttonset > button.ui-button:nth-child(1)").last().click();
		}
	},100);
}

function naManodopera(interval)
{
	if ($(".qaStatusWrapper a:contains(N.A.)").length) {
		if ($("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_type-button").is(":visible") || $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find(".ManufacturerLacquerFactor_wageModePerHour").is(":visible") || $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_type-button").is(":visible")) {
			
			$("iframe.sphinxPage").contents().find("#CalculationFactorChapter > div.chapterBody > ul > li.CalculationFactor_discount_label > span:nth-child(1) > label").text("Sconto sul totale").change();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_showLongWorkStrings_label").hide();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_showAllIncludedWork_label").hide();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_calculationWithoutWork_label").hide();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_biwOptimizationMode_label").hide();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_preDamageTotal_label").hide();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_newForOld_label").hide();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_appreciation_label").hide();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_cfTimeUnit_label").hide();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_vatRate_label").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_increaseDecrease_label").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_surchargeInProtocolOly_label").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_surchargeSeparated_label").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_sparePartsDisposalCosts_label").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_bracketSetRentCost_label").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_acquisitionCosts_label").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_bracketSetProcurementCost_label").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_procurementCost_label").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_bodyInWhiteProcurementCost_label").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_discountBiw_label").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_sparePartLumpSum_label").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_title1_label").hide();
			$("iframe.sphinxPage").contents().find("#LabourCostFactor_electricWage2").hide();
			$("iframe.sphinxPage").contents().find("#LabourCostFactor_electricWage3").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_dentsCountInProtocol_label").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_isSuppressCavityProtection_label").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_discount_label").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_discountBiw_label").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_labourLumpSum_label").hide();
			$("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_lacquerColorListGrp").contents().find("#ManufacturerLacquerFactor_color2ndName").parent().parent().hide();
			$("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_lacquerColorListGrp").contents().find("#ManufacturerLacquerFactor_color2ndCode").parent().parent().hide();
			$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_addition_label").hide();
			$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_preparationTimePercent_label").hide();
			$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_isSurchargeForSecondColor_label").hide();
			$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_isLacquerAssemblyWhenRemoved_label").hide();
			$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_isSurchargeForMatt_label").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_electricWage1_label").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_dentWage_label").hide();
			
			$("iframe.sphinxPage").contents().find(".SparePartFactor_increaseDecrease").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_surchargeInProtocolOly").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_surchargeSeparated").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_sparePartsDisposalCosts").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_bracketSetRentCost").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_acquisitionCosts").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_bracketSetProcurementCost").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_procurementCost").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_bodyInWhiteProcurementCost").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_smallPartsChargeTitle").hide();
			$("iframe.sphinxPage").contents().find("#SparePartFactor_smallSparePartCalculationModel").hide();
			$("iframe.sphinxPage").contents().find("#SparePartFactor_discountMode0").hide();
			$("iframe.sphinxPage").contents().find("#SparePartFactor_discountMode1").hide();
			$("iframe.sphinxPage").contents().find("label[for='SparePartFactor_discountMode0']").hide();
			$("iframe.sphinxPage").contents().find("label[for='SparePartFactor_discountMode1']").text("%");
			$("iframe.sphinxPage").contents().find("br").hide();
			$("iframe.sphinxPage").contents().find("#DiscountRuleFactorChapter").hide();
			$("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_addition").hide();
			$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_preparationTimePercent").hide();
			$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_isLacquerAssemblyWhenRemoved").hide();
			$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_addition").hide();
			$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_discountWage").hide();
			$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_discountWageBiw").hide();
			$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_repairWageLacquerMaterialFlatAmount").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_dentsCountInProtocol").hide();
			$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_discountMaterialBiw").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_discountBiw").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_labourLumpSum").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_addition").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_preparationTimePercent").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isReducedPreparationTime").hide();
			$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_preparationTimePercent_preparationTimePercent").hide();
			$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_isLacquerAssemblyWhenRemoved").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isFrameworkUse").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isApplyUndercoatLacquerWhenRemoved").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isFrameworkUse").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isSurchargeForSecondColor").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isLacquerPlasticWhenFitted").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isLacquerAssemblyWhenRemoved").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_maskingWorkGlassCount").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_maskingWorkPlasticCount").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_matBlackWindowFrameCount").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_materialSpecialLacquer").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_repairWageLacquerMaterialFlatRateAbsolute").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_repairWageLacquerMaterialFlatRatePercent").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_discountBiw").hide();
			$("iframe.sphinxPage").contents().find(".SparePartFactor_sparePartLumpSum").hide();
			$("iframe.sphinxPage").contents().find("#GarageRulesFactorChapter").hide();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_showLongWorkStrings").hide();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_showAllIncludedWork").hide();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_biwOptimizationMode").hide();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_preDamageTotal").hide();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_newForOld").hide();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_appreciation").hide();
			$("iframe.sphinxPage").contents().find(".CalculationFactor_vatRate").hide();
			$("iframe.sphinxPage").contents().find(".chapterHeader").hide();
			$("iframe.sphinxPage").contents().find("#CalculationFactor_discountMode0").hide();
			$("iframe.sphinxPage").contents().find("label[for='CalculationFactor_discountMode0']").hide();
			$("iframe.sphinxPage").contents().find("label[for='CalculationFactor_discountMode1']").text("%");
			$("iframe.sphinxPage").contents().find("#CalculationFactor_discountMode1").hide();
			$("iframe.sphinxPage").contents().find("label[for='CalculationFactor_discount']").text("Extra Sconto %");
			$("iframe.sphinxPage").contents().find("label[for='SparePartFactor_discount']").text("Sconto Ricambi %");
			$("iframe.sphinxPage").contents().find("input#LabourCostFactor_mechanicWage1").on("focusout", function() {
				var newInput = $("iframe.sphinxPage").contents().find("input#LabourCostFactor_mechanicWage1").val();
				$("iframe.sphinxPage").contents().find("input#LabourCostFactor_mechanicWage1").val(newInput).change();
				$("iframe.sphinxPage").contents().find("input#LabourCostFactor_electricWage1").val(newInput).change();
				$("iframe.sphinxPage").contents().find("input#LabourCostFactor_dentWage").val(newInput).change();
			});
			$("iframe.sphinxPage").contents().find("input#LabourCostFactor_bodyWage1").on("focusout", function() {
				var newInput2 = $("iframe.sphinxPage").contents().find("input#LabourCostFactor_bodyWage1").val();
				$("iframe.sphinxPage").contents().find("input#LabourCostFactor_bodyWage1").val(newInput2).change();
				$("iframe.sphinxPage").contents().find("input#EuroLacquerFactor_wage").val(newInput2).change();
				$("iframe.sphinxPage").contents().find("input#ManufacturerLacquerFactor_wage").val(newInput2).change();
			});
			$("iframe.sphinxPage").contents().find("label[for='LabourCostFactor_mechanicWage1']").text("Manodopera Meccanica");
			$("iframe.sphinxPage").contents().find("label[for='LabourCostFactor_bodyWage1']").text("Manodopera Carrozzeria");
			$("iframe.sphinxPage").contents().find("#LabourCostFactor_mechanicWage2").hide();
			$("iframe.sphinxPage").contents().find("#LabourCostFactor_mechanicWage3").hide();
			//$("iframe.sphinxPage").contents().find(".LabourCostFactor_bodyWage1").hide();
			$("iframe.sphinxPage").contents().find("input#LabourCostFactor_bodyWage2").hide();
			$("iframe.sphinxPage").contents().find("input#LabourCostFactor_bodyWage3").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_electricWage1").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_dentWage").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_wage").hide();
			$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_wage").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_discount").hide();
			$("iframe.sphinxPage").contents().find(".EuroLacquerFactor_calculationType").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_title1").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_title2").hide();
			$("iframe.sphinxPage").contents().find(".LabourCostFactor_title3").hide();
			$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerMono").on("focusout", function() {
				var newInput = $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerMono").val();
				$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Mono").val(newInput).change();
				$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerDouble").val(newInput).change();
				$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Double").val(newInput).change();
				$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Triple").val(newInput).change();
				$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerTriple").val(newInput).change();
				$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerQuad").val(newInput).change();
				$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Quad").val(newInput).change();
			});
			$("iframe.sphinxPage").contents().find("label[for='DomusCalculationFactor_layerAdd15Mono']").parent().hide();
			$("iframe.sphinxPage").contents().find("label[for='DomusCalculationFactor_layerAdd15Triple']").parent().hide();
			$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Mono").parent().hide();
			$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Triple").parent().hide();
			$("iframe.sphinxPage").contents().find(".DomusCalculationFactor_layerDouble").hide();
			$("iframe.sphinxPage").contents().find(".DomusCalculationFactor_layerTriple").hide();
			$("iframe.sphinxPage").contents().find(".DomusCalculationFactor_layerQuad").hide();
			$("iframe.sphinxPage").contents().find("#EuroLacquerFactor_calculationType").hide();
			$("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_calculationType").hide();
			$("iframe.sphinxPage").contents().find("li[aria-controls='euroLacquerFactor']").parent().hide();
			$("iframe.sphinxPage").contents().find("label[for='SparePartFactor_priceDate-button']").text("Listino ricambi al");
			$("iframe.sphinxPage").contents().find("label[for='DomusCalculationFactor_layerMono']").text("Materiale di consumo");
			$("iframe.sphinxPage").contents().find(".ui-widget.ui-widget-content").css("border", "0px solid");
			$("iframe.sphinxPage").contents().find("#DomusCalculationFactor_wasteDisposalCostsPercValue").val("1,50").change();
			$("iframe.sphinxPage").contents().find("#DomusCalculationFactor_wasteDisposalCostsMaxValue").val("25,00").change();
			$("iframe.sphinxPage").contents().find("#EuroLacquerFactor_materialFlatRatePercent").val("100").change();
			$("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_materialFlatRatePercent").val("100").change();
			$("iframe.sphinxPage").contents().find("#GarageRulesFactorChapter div ul li>span, #aztLacquerFactor ul li>span, #euroLacquerFactor ul li>span, #manufacturerLacquerFactor ul li>span").css("width", "267px");
			$("iframe.sphinxPage").contents().find("label.increaseIndent").css("width", "271px");
			$("iframe.sphinxPage").contents().find("label[for='DomusCalculationFactor_wasteDisposalCostsMaxValue']").css("width", "275px");
			$("#manodoperaPopup > legend").text("Configurazione Manodopera");
			$(".loadingDialog").show();
			$('#tab-contractOpening').css("display", "block");
			$('#tab-contractOpening').css("position", "absolute");
			clearInterval(interval);
			$("#activityRelatedSelection").show();
			$('#tab-activityRelatedSelection').css({
				"visibility": "visible",
				"left": "0px"
			});
			$("#manodoperaPopup").addClass("overlayVisibleDialog");
			$("#manodoperaPopup").removeClass("overlayHiddenDialogVehSel");
			$("#activityRelatedSelection").css("visibility", "visible");
		}
		return true;
	} else {
		return false;
	}
}

function clickOnCheck(checkObj) {
    if (checkObj.attr("data-checked") == 0) {
        checkObj.trigger("click");
    }
}
(function($) {
    $.each(['show', 'hide'], function(i, ev) {
        var el = $.fn[ev];
        $.fn[ev] = function() {
            this.trigger(ev);
            return el.apply(this, arguments);
        };
    });
})(jQuery);
$('a[href*=tab-graphicalPartSelection]').on("click", function() {
    $("#tab-graphicalPartSelection").removeClass("hiddenAbsolute");
});

setTimeout(function() {
    $(".qaStatus,#statusActionsContainer").bind("DOMSubtreeModified", function() {
        if ($(".qaStatus > a").text() == "N.A. - Perizia conclusa") {
            $("#claim-back").click();
            setTimeout(function() {
                $(".ui-dialog:visible").contents().find(".ui-dialog-buttonset > button:nth-child(2)").click();
            }, 100);
        }
        if ($(".qaStatus > a").text() == "Nuovo Incarico") {
            if ($("li[aria-controls='tab-calculationPreview']").attr("aria-selected") == "true") {
                $(".MancataRicezioneAuto").css({
                    "position": "absolute",
                    "visibility": "hidden"
                });
            } else {
                $(".MancataRicezioneAuto").removeClass("hiddenAbsolute");
                $(".MancataRicezioneAuto").css({
                    "position": "relative",
                    "visibility": "visible"
                });
            }
        }
    });
}, 1000);
/*SIDEBAR NUOVA UI*/
//REPLACING PRINTER
var title = $(".printManager >i").attr("title");
$("#customField-button-printer").text(title);
$("#customField-button-printer").on("click", function() {
    $("a.printManager").trigger("click");
});
//REPLACING MAIL
var title = $(".sendClaimMessage >i").attr("title");
$("#customField-button-chat").text(title);
$("#customField-button-chat").on("click", function() {
    $("a.sendClaimMessage").trigger("click");
});
//REPLACING PRINTER
var title = $(".printManager >i").attr("title");
$("#customField-button-printer").text(title);
$("#customField-button-printer").on("click", function() {
    $("a.printManager").trigger("click");
});
//REPLACING CHAT     
/*var title = $(".sendClaimEmail >i").attr("title");*/

//REPLACING STATUS CHANGE
/*$("#customField-button-statuses").on("click",function(){
	$("#statusActions").dialog();
});*/
$("#customField-button-flag").on("click", function() {
    $("#toggler-right").click();
});
/*FINE SIDEBAR NUOVA UI*/
/*FINE SIDEBAR NUOVA UI*/
$(".view.view-inbox").on("show",function(){ 
	$("link[href='https://datgroupsolutions.eu/JS/AXA/AXA20201006.01.js']").remove();
	/*
	setTimeout(function(){
		if (window.location.href.contains("gold"))
			window.location.href = 'https://gold.dat.de/myClaim/inbox.jsp#inbox';
		else
			window.location.href = 'https://www.dat.de/myClaim/inbox.jsp#inbox';
		//location.reload();
		//$("link[href='https://datgroupsolutions.eu/JS/AXA/AXA.js']").remove(); $("link[href='https://datgroupsolutions.eu/JS/AXA/AXA.css']").remove();
	},800);
    */
});
/*IMPOSTAZIONI MANODOPERA*/
$("#customField-button-LabourSetting").on("click", function() {
    $('a[href*=tab-activityRelatedSelection]').trigger("click");
    $('#tab-activityRelatedSelection').css("visibility", "hidden");
    $('#tab-activityRelatedSelection').css({
        "visibility": "hidden",
        "left": "20000px"
    });
    $('#manodoperaPopup').css("position", "absolute");
    $("#manodoperaPopup").removeClass("overlayVisibleDialog");
    $("#manodoperaPopup").addClass("overlayHiddenDialogVehSel");
	var Autored = setInterval(function() {
		
		naManodopera(Autored);
		
        if ($("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_type-button").is(":visible") || $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find(".ManufacturerLacquerFactor_wageModePerHour").is(":visible") || $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_type-button").is(":visible")) {
            $("iframe.sphinxPage").contents().find(".SparePartFactor_increaseDecrease").hide();
            $("iframe.sphinxPage").contents().find(".SparePartFactor_surchargeInProtocolOly").hide();
            $("iframe.sphinxPage").contents().find(".SparePartFactor_surchargeSeparated").hide();
            $("iframe.sphinxPage").contents().find(".SparePartFactor_sparePartsDisposalCosts").hide();
            $("iframe.sphinxPage").contents().find(".SparePartFactor_bracketSetRentCost").hide();
            $("iframe.sphinxPage").contents().find(".SparePartFactor_acquisitionCosts").hide();
            $("iframe.sphinxPage").contents().find(".SparePartFactor_bracketSetProcurementCost").hide();
            $("iframe.sphinxPage").contents().find(".SparePartFactor_procurementCost").hide();
            $("iframe.sphinxPage").contents().find(".SparePartFactor_bodyInWhiteProcurementCost").hide();
            $("iframe.sphinxPage").contents().find(".SparePartFactor_smallPartsChargeTitle").hide();
            $("iframe.sphinxPage").contents().find("#SparePartFactor_smallSparePartCalculationModel").hide();
            $("iframe.sphinxPage").contents().find("#SparePartFactor_discountMode0").hide();
            $("iframe.sphinxPage").contents().find("#SparePartFactor_discountMode1").hide();
            $("iframe.sphinxPage").contents().find("label[for='SparePartFactor_discountMode0']").hide();
            $("iframe.sphinxPage").contents().find("label[for='SparePartFactor_discountMode1']").text("%");
            $("iframe.sphinxPage").contents().find("br").hide();
            $("iframe.sphinxPage").contents().find("#DiscountRuleFactorChapter").hide();
            $("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_addition").hide();
            $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_preparationTimePercent").hide();
            $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_isLacquerAssemblyWhenRemoved").hide();
            $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_addition").hide();
            $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_discountWage").hide();
            $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_discountWageBiw").hide();
            $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_repairWageLacquerMaterialFlatAmount").hide();
            $("iframe.sphinxPage").contents().find(".LabourCostFactor_dentsCountInProtocol").hide();
            $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_discountMaterialBiw").hide();
            $("iframe.sphinxPage").contents().find(".LabourCostFactor_discountBiw").hide();
            $("iframe.sphinxPage").contents().find(".LabourCostFactor_labourLumpSum").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_addition").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_preparationTimePercent").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isReducedPreparationTime").hide();
            $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_preparationTimePercent_preparationTimePercent").hide();
            $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_isLacquerAssemblyWhenRemoved").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isFrameworkUse").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isApplyUndercoatLacquerWhenRemoved").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isFrameworkUse").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isSurchargeForSecondColor").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isLacquerPlasticWhenFitted").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isLacquerAssemblyWhenRemoved").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_maskingWorkGlassCount").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_maskingWorkPlasticCount").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_matBlackWindowFrameCount").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_materialSpecialLacquer").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_repairWageLacquerMaterialFlatRateAbsolute").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_repairWageLacquerMaterialFlatRatePercent").hide();
            $("iframe.sphinxPage").contents().find(".SparePartFactor_discountBiw").hide();
            $("iframe.sphinxPage").contents().find(".SparePartFactor_sparePartLumpSum").hide();
            $("iframe.sphinxPage").contents().find("#GarageRulesFactorChapter").hide();
            $("iframe.sphinxPage").contents().find(".CalculationFactor_showLongWorkStrings").hide();
            $("iframe.sphinxPage").contents().find(".CalculationFactor_showAllIncludedWork").hide();
            $("iframe.sphinxPage").contents().find(".CalculationFactor_biwOptimizationMode").hide();
            $("iframe.sphinxPage").contents().find(".CalculationFactor_preDamageTotal").hide();
            $("iframe.sphinxPage").contents().find(".CalculationFactor_newForOld").hide();
            $("iframe.sphinxPage").contents().find(".CalculationFactor_appreciation").hide();
            $("iframe.sphinxPage").contents().find(".CalculationFactor_vatRate").hide();
            $("iframe.sphinxPage").contents().find(".chapterHeader").hide();
            $("iframe.sphinxPage").contents().find("#CalculationFactor_discountMode0").hide();
            $("iframe.sphinxPage").contents().find("label[for='CalculationFactor_discountMode0']").hide();
            $("iframe.sphinxPage").contents().find("label[for='CalculationFactor_discountMode1']").text("%");
            $("iframe.sphinxPage").contents().find("#CalculationFactor_discountMode1").hide();
            $("iframe.sphinxPage").contents().find("label[for='CalculationFactor_discount']").text("Extra Sconto %");
            $("iframe.sphinxPage").contents().find("label[for='SparePartFactor_discount']").text("Sconto Ricambi %");
            $("iframe.sphinxPage").contents().find("input#LabourCostFactor_mechanicWage1").on("focusout", function() {
                var newInput = $("iframe.sphinxPage").contents().find("input#LabourCostFactor_mechanicWage1").val();
                $("iframe.sphinxPage").contents().find("input#LabourCostFactor_mechanicWage1").val(newInput).change();
                $("iframe.sphinxPage").contents().find("input#LabourCostFactor_electricWage1").val(newInput).change();
                $("iframe.sphinxPage").contents().find("input#LabourCostFactor_dentWage").val(newInput).change();
                $("iframe.sphinxPage").contents().find("input#EuroLacquerFactor_wage").val(newInput).change();
                $("iframe.sphinxPage").contents().find("input#ManufacturerLacquerFactor_wage").val(newInput).change();
            });
            $("iframe.sphinxPage").contents().find("label[for='LabourCostFactor_mechanicWage1']").text("Manodopera Meccanica");
            $("iframe.sphinxPage").contents().find("label[for='LabourCostFactor_bodyWage1']").text("Manodopera Carrozzeria");
            $("iframe.sphinxPage").contents().find("#LabourCostFactor_mechanicWage2").hide();
            $("iframe.sphinxPage").contents().find("#LabourCostFactor_mechanicWage3").hide();
            //$("iframe.sphinxPage").contents().find(".LabourCostFactor_bodyWage1").hide();
            $("iframe.sphinxPage").contents().find("input#LabourCostFactor_bodyWage2").hide();
            $("iframe.sphinxPage").contents().find("input#LabourCostFactor_bodyWage3").hide();
            $("iframe.sphinxPage").contents().find(".LabourCostFactor_electricWage1").hide();
            $("iframe.sphinxPage").contents().find(".LabourCostFactor_dentWage").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_wage").hide();
            $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_wage").hide();
            $("iframe.sphinxPage").contents().find(".LabourCostFactor_discount").hide();
            $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_calculationType").hide();
            $("iframe.sphinxPage").contents().find(".LabourCostFactor_title1").hide();
            $("iframe.sphinxPage").contents().find(".LabourCostFactor_title2").hide();
            $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerMono").on("focusout", function() {
                var newInput = $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerMono").val();
                $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Mono").val(newInput).change();
                $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerDouble").val(newInput).change();
                $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Double").val(newInput).change();
                $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Triple").val(newInput).change();
                $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerTriple").val(newInput).change();
                $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerQuad").val(newInput).change();
                $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Quad").val(newInput).change();
            });
            $("iframe.sphinxPage").contents().find("label[for='DomusCalculationFactor_layerAdd15Mono']").parent().hide();
            $("iframe.sphinxPage").contents().find("label[for='DomusCalculationFactor_layerAdd15Triple']").parent().hide();
            $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Mono").parent().hide();
            $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Triple").parent().hide();
            $("iframe.sphinxPage").contents().find(".DomusCalculationFactor_layerDouble").hide();
            $("iframe.sphinxPage").contents().find(".DomusCalculationFactor_layerTriple").hide();
            $("iframe.sphinxPage").contents().find(".DomusCalculationFactor_layerQuad").hide();
            $("iframe.sphinxPage").contents().find("#EuroLacquerFactor_calculationType").hide();
            $("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_calculationType").hide();
            $("iframe.sphinxPage").contents().find("li[aria-controls='euroLacquerFactor']").parent().hide();
            $("iframe.sphinxPage").contents().find("label[for='SparePartFactor_priceDate-button']").text("Listino ricambi al");
            $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerMono").addClass("mandatory");
            $("iframe.sphinxPage").contents().find("label[for='DomusCalculationFactor_layerMono']").text("Materiale di consumo");
            $("iframe.sphinxPage").contents().find(".ui-widget.ui-widget-content").css("border", "0px solid");
            $("iframe.sphinxPage").contents().find("#DomusCalculationFactor_wasteDisposalCostsPercValue").val("1,50").change();
            $("iframe.sphinxPage").contents().find("#DomusCalculationFactor_wasteDisposalCostsMaxValue").val("25,00").change();
            $("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_materialFlatRatePercent").val("100").change();
            $("iframe.sphinxPage").contents().find("#GarageRulesFactorChapter div ul li>span, #aztLacquerFactor ul li>span, #euroLacquerFactor ul li>span, #manufacturerLacquerFactor ul li>span").css("width", "267px");
            $("iframe.sphinxPage").contents().find("label.increaseIndent").css("width", "271px");
            $("iframe.sphinxPage").contents().find("label[for='DomusCalculationFactor_wasteDisposalCostsMaxValue']").css("width", "275px");
            $("#manodoperaPopup > legend").text("Configurazione Manodopera");
            $(".loadingDialog").show();
            $('#tab-contractOpening').css("display", "block");
            $('#tab-contractOpening').css("position", "absolute");
            clearInterval(Autored);
        } else {
            $('#tab-contractOpening').css("display", "block");
            $('#tab-contractOpening').css("position", "absolute");
        }
    }, 50);
    $("#activityRelatedSelection").show();
    $('#tab-activityRelatedSelection').css({
        "visibility": "visible",
        "left": "0px"
    });
    $("#manodoperaPopup").addClass("overlayVisibleDialog");
    $("#manodoperaPopup").removeClass("overlayHiddenDialogVehSel");
    $("#activityRelatedSelection").css("visibility", "visible");
});

/* IMPORT DA PERITO PER PROCESSO NOT AGREED*/
$("#customField-button-importAXA").on("click", function() {
    $(".frameHeader > .caption:contains(Import Incarico)").parent().parent().contents().find("div.fa-file-upload").click();
    $("#claim-attachments").removeClass("hiddenAbsolute");
    $("#claim-attachments").show();
    var intervalPublish = setInterval(function() {
        if ($("button[data-caption='inbox.attachments.publish']").is(":visible") && !$(".loading").is(":visible")) {
            clearInterval(intervalPublish);
            setTimeout(function() {
                $("button[data-caption='inbox.attachments.publish']").click();
                $("#claim-attachments").hide();
                $("#claim-attachments").addClass("hiddenAbsolute");
				//$("#customField-button-VehicleSelSearch").removeClass("hiddenAbsolute");
            }, 400);
        }
    }, 500);
});
/*RIMUOVE SPAZI AL CAMBIO DI AGGIUNTA NOTE - QUESTO EVITA ALL ADZ DI ESSERE CORROTTO - DA FARE FISSARE DA KIRYL */
$("#customField-input-remarks").on("change", function() {
    $("#customField-input-remarks").val($("#customField-input-remarks").val().replace(/\n/g, '').replace("\u00F2", "o'").replace("\u00F2", "o'").replace("\xE8", "e'").replace("\xE8", "e'").replace("\xE8", "e'").replace("\xE0", "a'").replace("\xE0", "a'").replace("\xF9", "u'").replace("\xF9", "u'").replace('\u20AC', 'E').replace('\u20AC', 'E').replace('\u20AC', 'E'));
});
/*ORDINE RICAMBI - NON PIU UTILIZZATO*/
/*	$("#customField-button-OrderPartsReminder").on("click",function(){
	   $("#customField-button-OrderParts").click();
	   $("#claim-saveDraft").click();
	   $(this).parent().dialog("close");
	}); */
setTimeout(function() {
    if ($(".qaStatus > a").text() == "Nuovo Incarico") {
        if ($("li[aria-controls='tab-calculationPreview']").attr("aria-selected") == "true") {
            $(".MancataRicezioneAuto").css({
                "position": "absolute",
                "visibility": "hidden"
            });
        } else {
            $(".MancataRicezioneAuto").removeClass("hiddenAbsolute");
            $(".MancataRicezioneAuto").css({
                "position": "relative",
                "visibility": "visible"
            });
        }
    }/*
    var bsId = $("#customField-input-AspisBodyshopId").val();
    var damageNr = $("#customField-input-DamageNumber").val();
    var numberPlate = $("#customField-input-LicenseNumber").val();
    var assessmentNumber = damageNr + "IT" + bsId + numberPlate;
    if ($("#customField-input-AspisAssessmentNumber").val() != "") {
        if ($("#customField-input-AspisAssessmentNumber").val() != assessmentNumber) {
            $("#customField-input-AspisAssessmentNumber").val(assessmentNumber).change();
        }
    }*/
    $("#vroContractOpeningTab > ul > li > a").on("click", function() {
        if (!$("#vroContractOpeningTab > ul > li > a[href='#tab-calculationPreview']").parent().hasClass("ui-state-active")) {
            if ($("#layout-right").is(":visible")) {
                $("#customField-button-flag").click();
            }
        }
    });
    $("#customField-input-repairerIBAN2").on("change", function() {
        var IBAN = $("#customField-input-repairerIBAN2").val();
        if ($("#customField-input-repairerABI2").val() == "") {
            var ABI = IBAN.substring(5, 10);
            var CAB = IBAN.substring(10, 15);
            $("#customField-input-repairerABI2").val(ABI).change();
            $("#customField-input-repairerCAB2").val(CAB).change();
        }
    });
    var IBAN = $("#customField-input-repairerIBAN").val();
    if ($("#customField-input-repairerABI").val() == "") {
        var ABI = IBAN.substring(5, 10);
        var CAB = IBAN.substring(10, 15);
        $("#customField-input-repairerABI").val(ABI).change();
        $("#customField-input-repairerCAB").val(CAB).change();
    }
    $("#menu-help-contactSupport").parent().hide();
    $("#layout-menu-availableHelps").hide();
    if (!$("#layout-menu-availableHelps1").length) {
        //$('#layout-menu-login > li:nth-child(1) > ul').append('<li id="layout-menu-availableHelps1"><a style="cursor:default;" class="icon icon-partnerList">Manuale Myclaim (AXA)</a><ul style="display: none;"></ul></li><li><a id="layout-menu-availableHelps4" class="icon icon-person">Assistenza Remota</a><ul style="display: none;"></li>');
        $('#help-menu').append('<li id="layout-menu-availableHelps1"><a style="cursor:default;" class="icon icon-partnerList">Manuale Myclaim (AXA)</a><ul style="display: none;"></ul></li><li><a id="layout-menu-availableHelps4" class="icon icon-person">Assistenza Remota</a><ul style="display: none;"></li>');
    }
    $("#layout-menu-availableHelps1").on("click", function() {
        window.open("https://www.dat.de/fileadmin/it/Guida_myClaim_AXA_v1.2.pdf");
    });
    $("#layout-menu-availableHelps2").on("click", function() {
        window.open("https://www.dat.de/fileadmin/media/download-Ausland/download-IT/FAQ/Manuale_carrozzeria_SOL_20180710.pdf");
    });
    $("#layout-menu-availableHelps3").on("click", function() {
        window.open("https://www.dat.de/fileadmin/media/download-Ausland/download-IT/FAQ/Memorandum_Processo_Authority_DAT_2018.pdf");
    });
    $("#layout-menu-availableHelps4").on("click", function() {
        window.open("https://www.dat.de/fileadmin/it/AssistenzaDAT.exe");
    });
    if ($("#customField-input-ASPISOrderBodyshop").is(":checked")) {
        $("#customField-button-ASPISOrderBodyshop2").css("background-image", "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAiCAYAAAA6RwvCAAAACXBIWXMAAA+QAAAPkAF7Vr0pAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAANnSURBVHjavNhdaNV1HMfx1zk7Z+essSRaObTRA0pZUZgMsyQUbyoKL4JkEbPnAvFi2Bhm9nSjBsPoSboQRCxKupAoR0gG1VYzccIaUbToadDSlcxlez5d7Lc66vmfp7a+8Lv4ne/5/8/79/19f9/v53difvd/Wg0ewz4MZDtiBUHiqJoFhEmMaMGL+BlteB3jhUEq8AeOIPYfIea51M16ZdRmeT7GRvQmCkbjr7CGMwGsVIvhNDbZYrVaZ87yrsbn2J4fZAILsBLdSJUBMYIVlmv0hNHIvFmYHyQT8mN+2NXqEkGmUCnpQa+oVnlONGbsVzyfKPiyCdyEBqRLBBlFg0fdqCECAp7Fb7Gijm8srK5Uy7hYwlcy6mRyfuN4WOJEosgXlpeoPJUHAjaHmIvNYUG7FsfypPh7WDszSZg7eyESImlYxtPZH80VyN24JzLffrBFWk9pJb50mxeqzpXneVLo06vNMnGj2dU6PgfRaM0JMXPyPtRq3KhMmIcx21uzGM05PWl86X09PshVGGcb5LmcZS+OIaPaPWNypt/OHcga3JfTk0CvNyR0WxJVM2cnWeP4BLdG/sqIG4zrj8rKRI5OuBLtJYKsj4SYrswbpfW7IN9KUmFXk6AJB7Ef9UVCLMT2PP5P8ZZMEEgRo8IqfI/TLjHfmzJqcB0ag5o4XqDlvZwnGhO4P4iIAnu7Hutw1GZpC7J8dXgVHbg94vlbQhSjbC86i9NQy3CZxXbpVq3aZKTqfClswcmsEnUonJZcNoSlId5FZPsAHrFDbSTEjIzeFGT0A+GU3JsHAnYUCzG9qiZr7XbAcNAdxdlHuByLIvw/4noMF3/+LzLiWz3SJZW3NXkgprChFIjpkFfo84U9hpxwleWqVJUlC/+1/dhW6kMVFqHShK91OeZdS02pscRUyVKZtD+lNEoalAy1qciROOtaOaDPKc3qvIYWPFy0Wk3iqDYnfVOOvk2cN5vuBd/hcbyDrVj1j4jJldCVOOEXG+zUX14rLfTIYRwWc6cxrVJukzinjcdCzHbZKu6Ua8pLrOLYUw7q0K7TOg/Zpt4VxjAWbn9duuy211TZ144iQaZlXkant/3kkBWa3KFZvXqDxu3xpKvLSu8yhFEsdOkRgw7YqcM+d2kxZsgRn7kw/HNQpv09AAHd4dj0qhdhAAAAAElFTkSuQmCC')");
    }
    // RETRIEVING REPAIRER COMPANY INFO
    if (user.role.match(/REPAIRER/)) {
        $("#customField-button-ASPISOrderBodyshop2").addClass("hiddenAbsolute");
        /*
        $.ajax({
          type: "GET",
          url: " https://www.dat.de/myClaim/json/core/GetCompanyInfo",

        }).done(function(data, textStatus, jqXHR){
               
                var info2=jqXHR.responseText; 
                 var IBAN=info2.substring(info2.indexOf('bankIban":"')+11,info2.indexOf('","bankBic'));
         var BIC=info2.substring(info2.indexOf('bankBic":"')+11,info2.indexOf('","districtCourt'));
         var CompanyInfo=info2.substring(info2.indexOf(',"name":"')+9,info2.indexOf('","nameLong'));
         var CompanyStreet= info2.substring(info2.indexOf(',"street":"')+11,info2.indexOf('","StreetNr":')) + " " + info2.substring(info2.indexOf(',"StreetNr":"')+13,info2.indexOf('","zip"'));
         var CompanyCity=  info2.substring(info2.indexOf('"city":"')+8,info2.indexOf('","country"'));
         var CompanyZipCode= info2.substring(info2.indexOf(',"zip":"')+8,info2.indexOf('","city":'));
          var BankName= info2.substring(info2.indexOf(',"bankName"')+13,info2.indexOf('","bankIban'));
         var CompanyVATNumber= info2.substring(info2.indexOf(',"taxNumber":"')+14,info2.indexOf('","accountNumber"'));

         $("#customField-input-companyName").val(CompanyInfo).change();
         $("#customField-input-street").val(CompanyStreet).change();
         $("#customField-input-city").val(CompanyCity).change();
         $("#customField-input-repairerBankName").val(BankName).change();
          $("#customField-input-repairerBIC").val(BIC).change();
         $("#customField-input-zipCode").val(CompanyZipCode).change();
         $("#customField-input-taxNr").val(CompanyVATNumber).change();
         $("#customField-input-repairerIBAN").val(IBAN).change();
          });*/
    }
}, 1200);
$("#customField-input-FinalTotal").on("change", function() {
    $("#customField-input-FinalTotal2").val($("#customField-input-FinalTotal").val().replace(".", "").replace(",", ".")).change();
});
$('a[href*=tab-graphicalPartSelection]').on("click", function() {
    $("#graphicalPartSelection").css("filter", "blur(0px)");
    $(".loadingGrapa").hide();
    var inter = setInterval(function() {
        if ($("iframe").is(":visible")) {
            if ($("#claim-attachments").is(":visible")) {
                $("#customField-button-attachment").click();
            }
            //CHIUDE IL MENU LATERALE DESTRO IN GRAFICA
            if ($("#layout-right").is(":visible")) {
                $("#toggler-right").click();
            }
            clearInterval(inter);
        }
    }, 500);
});

var statoCheck = $(".qaStatusWrapper a:contains(Comunicazione da Authority)").length;
var statoCheck2 = $(".qaStatusWrapper a:contains(Comunicazione da Carrozzeria)").length;
var statoCheck3 = $(".qaStatusWrapper a:contains(In lavorazione da Authority)").length;
if (statoCheck == "1" && user.role.match(/REPAIRER/)) {
    $("#customField-button-SPOptimization").addClass("disabled disableLink");
    $("#datiVeicolo,#DatiPratica").addClass("disabled disableLink");
    $("#Assicurato,#Locatario,#driver").addClass("disabled disableLink");
    $("#DatiSinistro").addClass("disabled disableLink");
    $("#DinamicaSinistro").addClass("disabled disableLink");
    $('a[href*=tab-graphicalPartSelection]').addClass("disabled disableLink");
}
if (statoCheck2 == "1" && user.role.match(/REPAIRER/)) {
    $("#customField-button-SPOptimization").addClass("disabled disableLink");
    $("#datiVeicolo,#DatiPratica").addClass("disabled disableLink");
    $("#Assicurato,#Locatario,#driver").addClass("disabled disableLink");
    $("#DatiSinistro").addClass("disabled disableLink");
    $("#DinamicaSinistro").addClass("disabled disableLink");
    $('a[href*=tab-graphicalPartSelection]').addClass("disabled disableLink");
}
if (statoCheck3 == "1" && user.role.match(/REPAIRER/)) {
    $("#customField-button-SPOptimization").addClass("disabled disableLink");
    $("#datiVeicolo,#DatiPratica").addClass("disabled disableLink");
    $("#Assicurato,#Locatario,#driver").addClass("disabled disableLink");
    $("#DatiSinistro").addClass("disabled disableLink");
    $("#DinamicaSinistro").addClass("disabled disableLink");
    $('a[href*=tab-graphicalPartSelection]').addClass("disabled disableLink");
    $('a[href*=tab-calculationPreview]').addClass("disabled disableLink");
}

function controlloGrafico() {
    $("#calculationPreview > div").css("visibility", "visible");
    $("#customField-input-Price").val("");

    if ($("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr").is(":visible")) {} else {
        $("#graphicalPartSelection > iframe").contents().find("#btnCalculation").click();
    }

    var rowCounter = $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr").length;
    for (i = 1; i <= rowCounter; ++i) {
        var price = $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + i + ") > td:nth-child(12) > div").text();
        var me = $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + i + ") > td:nth-child(10) > div").text();
        var ve = $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + i + ") > td:nth-child(8) > div").text();
        var la = $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + i + ") > td:nth-child(6) > div").text();
        var sr = $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + i + ") > td:nth-child(4) > div").text();
        if (price == "#") {
            $("#customField-input-Price").val("m").change();
        } else {
            if ($("#customField-input-Price").val() != "m") {
                $("#customField-input-Price").val("original").change();
            }
        }
    }
    // Total Button Click - all other buttons became white and only the data is yellow. It triggers on TotalTab
    $('a[href*=tab-calculationPreview]').trigger("click");
    $("#tab-calculationPreview").show();
    $("#customField-button-SPOptimization").addClass("hiddenAbsolute");
    if (user.role.match(/EXPERT/) || user.role.match(/INSURANCE/)) {
        $("#customField-button-Liquidazione").click();
    }
    $("#tab-graphicalPartSelection").addClass("hiddenAbsolute");
    $("#wait-overlay").addClass("hiddenAbsolute");
    $("#waitingOverlay").addClass("hiddenAbsolute");
    $("#waitingMessageOverlay").addClass("hiddenAbsolute");
    if ($(".qaStatusWrapper a:contains(N.A.)").length) {
        $(".loading.tabs-overlay").addClass("hiddenAbsolute");
        $(".qaStatusWrapper").addClass("hiddenAbsolute");
    }
    var inter = setInterval(function() {
        if ($("#calculationPreview").is(":visible") && $('a[href*=calculationPreview-positions]').length) {
            $("#customField-button-SPOptimization").removeClass("hiddenAbsolute");
            $(".qaStatusWrapper").removeClass("hiddenAbsolute");
            $("#wait-overlay").removeClass("hiddenAbsolute");
            $("#waitingOverlay").removeClass("hiddenAbsolute");
            $("#waitingMessageOverlay").removeClass("hiddenAbsolute");
            if (!$("#layout-right").is(":visible")) {
                $("#customField-button-flag").click();
            }
            if ($("#claim-attachments").is(":visible")) {
                $("#customField-button-attachment").click();
            }
            clearInterval(inter);
        }
    }, 700);
}

$(".qaStatus").addClass("disabled disableLink");
setTimeout(function() {
    if (!$("td.field-eCode").text() == "") {
        $("#customField-button-LabourSetting").text($("#customField-input-LabourChosen").val());
        $("#customField-button-kindOfPaint").text($("#customField-input-paintKind").val());
        $(".infoVehicle2").show();
        if (user.role.match(/REPAIRER/)) {
            if ($(".qaStatus > a").text() == "Nuovo Incarico") {
                /*
                $("#customField-control-EnteringDate > label").addClass("required");
                $("#customField-input-EnteringDate").addClass("required");
                $("#customField-control-deliveryDate > label").addClass("required");
                $("#customField-input-deliveryDate").addClass("required");
                */
            }
        }
        if ($(".qaStatusWrapper a:contains(N.A.)").length) {
            if ($("#customField-input-AspisBodyshopId").val() == "") {
                $("#customField-input-AspisBodyshopId").val($("#customField-input-AspisExpertId").val()).change();
            } else {
                $("#customField-input-AspisExpertId").val($("#customField-input-AspisBodyshopId").val()).change();
            }
            $(".infoVehicle3").show();
        }
    }
    if (!$("#customField-input-LettoCarrozzeria").is(":checked")) {
        if (user.role.match(/REPAIRER/)) {
            $("#customField-button-mail").css("background-color", "#ff6a00");
        }
    }
    if (!$("#customField-input-LettoPerito").is(":checked")) {
        if (user.role.match(/EXPERT/)) {
            $("#customField-button-mail").css("background-color", "#ff6a00");
        }
    }
}, 800);
$("#customField-input-VATDeduction2").on("change", function() {
    var t = $("#customField-input-VATDeduction2").val();
    $("#customField-input-float_VATDeduction").val(t).change();
});

if (user.role.match(/EXPERT/) || user.role.match(/MANUFACTURER/) || user.role.match(/INSURANCE/)) {
    $("#calculationPreview").css("margin-left", "165px");
    $(".lastPage").css("margin-left", "165px");
    $("#verticalMenu2").show();
}
$("#customField-button-SendComment").on("click", function() {
    if (user.role.match(/EXPERT/)) {
        $("#customField-input-historyComment").val($("#customField-input-historyComment").val() + "\n" + $.datepicker.formatDate('dd/mm/yy', new Date()) + " (" + new Date().toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1").substring(0, 5) + ")" + " - Authority : " + $("#customField-input-Comment").val());
        $("#customField-input-Comment").val("");
        if ($("#customField-input-LettoCarrozzeria").is(":checked")) {
            $("#customField-input-LettoCarrozzeria").click();
        }
    }
    if (user.role.match(/REPAIRER/)) {
        $("#customField-input-historyComment").val($("#customField-input-historyComment").val() + "\n" + $.datepicker.formatDate('dd/mm/yy', new Date()) + " (" + new Date().toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1").substring(0, 5) + ")" + " - Carrozzeria : " + $("#customField-input-Comment").val());
        $("#customField-input-Comment").val("");
        if ($("#customField-input-LettoPerito").is(":checked")) {
            $("#customField-input-LettoPerito").click();
        }
        $(".statusActionButton:contains('Invia Comunicazione')").trigger("click");
    }
    if (user.role.match(/INSURANCE/)) {
        $("#customField-input-historyComment").val($("#customField-input-historyComment").val() + "\n" + $.datepicker.formatDate('dd/mm/yy', new Date()) + " (" + new Date().toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1").substring(0, 5) + ")" + " - Authority : " + $("#customField-input-Comment").val());
        $("#customField-input-Comment").val("");
    }
    if (user.role.match(/MANUFACTURER/)) {
        $("#customField-input-historyComment").val($("#customField-input-historyComment").val() + "\n" + $.datepicker.formatDate('dd/mm/yy', new Date()) + " (" + new Date().toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1").substring(0, 5) + ")" + " - Amministratore : " + $("#customField-input-Comment").val());
        $("#customField-input-Comment").val("");
    }
});
$("button[data-caption*='inbox.claim.newMessage.send']").on("click", function() {
    if (user.role.match(/EXPERT/)) {
        $("#customField-input-internalComunication").val("2").change();
    }
    if (user.role.match(/REPAIRER/)) {
        $("#customField-input-internalComunication").val("1").change();
    }
    if (user.role.match(/INSURANCE/)) {
        $("#customField-input-internalComunication").val("3").change();
    }
});
$("#customField-button-contaFoto").on("click", function() {
    var contaJPG2 = $(".filename:contains(.JPG)").length;
    var contaJPG = $(".filename:contains(.jpg)").length;
    var contaPNG2 = $(".filename:contains(.PNG)").length;
    var contaPNG = $(".filename:contains(.png)").length;
    var contaJPEG2 = $(".filename:contains(.JPEG)").length;
    var contaJPEG = $(".filename:contains(.jpeg)").length;
    var contaBMP = $(".filename:contains(.bmp)").length;
    var contaBMP2 = $(".filename:contains(.BMP)").length;
    var sommaFoto = parseInt(contaJPG) + parseInt(contaPNG) + parseInt(contaJPEG) + parseInt(contaBMP) + parseInt(contaJPG2) + parseInt(contaPNG2) + parseInt(contaJPEG2) + parseInt(contaBMP2);
    $("#customField-input-attachmentNumber").val(sommaFoto).change();
});

//GRAPASEL
$.expr[':'].textEquals = $.expr.createPseudo(function(arg) {
    return function(elem) {
        return $(elem).text().match("^" + arg + "$");
    };
});
var z1 = 0;
var cO = 0;
var cP = 0;
var check5 = 0;
var a = 0;
var f = 0;
var z = 0;
var intervalloGrapa = 0;

if ($('a[href*=tab-graphicalPartSelection]').parent().attr("aria-disabled") == "true") {
        $("#customField-button-buttonCalculation").css("opacity", "0.35");
} else {
        $("#customField-button-buttonCalculation").css("opacity", "1");
}
$('a[href*=tab-graphicalPartSelection]').on('click', function(){
    if ($('a[href*=tab-graphicalPartSelection]').parent().attr("aria-disabled") == undefined) {
        $("#customField-button-buttonCalculation").css("opacity", "1");
    }

    setTimeout(function(){$("#graphicalPartSelection").height($("#graphicalPartSelection").height()+58);},1000);

    intervalloGrapa = setInterval(function() {
    if ($('div[aria-describedby*="EligibilityDialog"]').is(":visible")) {
        var str = $("#EligibilityDialog").html();
        var res = str.replace("service.privacy@axa.com", "privacy@axa.com");
        $("#EligibilityDialog").html(res);
    }

    if ($("#graphicalPartSelection").is(":visible")) {
        $("iframe").contents().find("#footer").hide();

        if ($("iframe").contents().find("#sumWorkTable_discount_0").is(":visible")) {
            var rowCounter = $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr").length;
            /* simone Latina 11-06-2019   alpha scale optimization change     */
            if (firstArray.length !== 0 && changedArray.length !== 0) {
                isChanged(firstArray, changedArray);
                changedArray = JSON.parse($("#customField-input-memoAchanged").val());
            }
            $("#graphicalPartSelection > iframe").contents().find("#saveButton,#btnDeleteSelected").on("click", function() {
                setTimeout(function() {
                    var intervalC = setInterval(function() {
                        if ($("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody").is(":visible")) {
                            clearInterval(intervalC);
                            if (module.findView("claim").data.claim.activeCalculation.rearrangedCalculationPositions != null) {
                                firstArray = updateFromPartSelection(firstArray, changedArray);
                            }
                        }
                    }, 300);
                }, 1000);
            });
            $("#customField-button-buttonCalculation").on("click", function() {
                if (module.findView("claim").data.claim.activeCalculation.rearrangedCalculationPositions != null) {
                    firstArray = updateFromPartSelection(firstArray, changedArray);
                }
            });
            /*  end Simone Latina code       */
            for (i = 1; i <= rowCounter; ++i) {
                var sparePart = $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + i + ") > td:nth-child(12) > div").text();
                if ($("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + i + ") > td:nth-child(1)  input").val().contains("ASCR")) {
                    $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + i + ") > td:nth-child(1)  input").attr("disabled", "disabled");
                    $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + i + ") > td:nth-child(2)  input").attr("disabled", "disabled");
                    $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + i + ") > td:nth-child(3)  input").attr("disabled", "disabled");
                    $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + i + ") > td:nth-child(13)  input").attr("disabled", "disabled");
                }
                //GESTIONE ASCR
                $("#graphicalPartSelection > iframe").contents().find("#italiaPositions > tbody > tr:nth-child(" + i + ") > td:nth-child(1)  input").on("input", function() {
                    if ($(this).val().contains("ASCR")) {
                        alert('Non \xE8 possibile inserire righe manuali contenenti testo "ASCR" nel codice ricambio');
                        $(this).val($(this).val().replace('ASCR', '')).change();
                    }
                });
            }
            $("iframe").contents().find("#sumWorkTable_wage_0").attr('readonly', true);
            $("iframe").contents().find("#sumWorkTable_wage_1").attr('readonly', true);
            $("iframe").contents().find("#sumWorkTable_wage_2").attr('readonly', true);
            $("iframe").contents().find("#sumWorkTable_discount_0").attr('readonly', true);
            $("iframe").contents().find("#sumWorkTable_discount_0Type").attr('disabled', true);
            $("iframe").contents().find("#sumWorkTable_discount_1Type").attr('disabled', true);
            $("iframe").contents().find("#sumWorkTable_discount_2Type").attr('disabled', true);
            $("iframe").contents().find("#sumWorkTable_discount_1").attr('readonly', true);
            $("iframe").contents().find("#sumWorkTable_discount_2").attr('readonly', true);
            $("iframe").contents().find("#sumTotalsTable_discount_0").attr('readonly', true);
            $("iframe").contents().find("#sumTotalsTable_discount_0Type").attr('disabled', true);
            $("iframe").contents().find("#sumTotalsTable_discount_1Type").attr('disabled', true);
            $("iframe").contents().find("#sumTotalsTable_discount_2Type").attr('disabled', true);
            $("iframe").contents().find("#sumTotalsTable_discount_3Type").attr('disabled', true);
            $("iframe").contents().find("#sumTotalsTable_discount_4Type").attr('disabled', true);
            $("iframe").contents().find("#sumTotalsTable_discount_1").attr('readonly', true);
            $("iframe").contents().find("#sumTotalsTable_discount_2").attr('readonly', true);
            $("iframe").contents().find("#sumTotalsTable_discount_3").attr('readonly', true);
            $("iframe").contents().find("#sumTotalsTable_discount_4").attr('readonly', true);
            $("iframe").contents().find("#sumLacquerTable_percentage_1").attr('readonly', true);
            $("iframe").contents().find("#sumLacquerTable_percentage_2").attr('readonly', true);


            if (z == 0) {
                if (user.role.match(/REPAIRER/) || user.role.match(/EXPERT/)) {
                    $("iframe").contents().find("#btnCalculation").remove();
                }
                $("iframe").contents().find("#btnVisualInfo").remove();
                $("iframe").contents().find("#graPaActBtn_aircTips").remove();
                z = 1;
                $("#graphicalPartSelection > iframe").contents().find("#graPaItaliaAddPos").on("click", function() {
                    var CounterPrice = $("#graphicalPartSelection > iframe").contents().find("[id*='input_partPrice_']").length;
                    var a = 1;
                    for (i = 1; i <= CounterPrice; i++) {
                        if ($("#graphicalPartSelection > iframe").contents().find("tr:nth-child(" + i + ")").contents().find("input[id*='input_partNo_']").val().indexOf("#") > -1) {
                            a = a + 1;
                        }
                        if (i == CounterPrice) {
                            var b = "#" + a;
                            $("#graphicalPartSelection > iframe").contents().find("tr:nth-child(" + i + ")").contents().find("input[id*='input_partNo_']").val(b);
                        }
                    }
                });
            }
        } else {
            z = 0;
        }


        if ($("iframe").contents().find("#btnImages").is(":visible")) {
            $("iframe").contents().find("#btnGrapaAdd").remove();
            $("iframe").contents().find("#btnPrint").remove();
            $("iframe").contents().find("#btnPrint").remove();
            $("iframe").contents().find("#btnTires").remove();
            $("iframe").contents().find("#graPaActBtn_tires").remove();
            $("iframe").contents().find("#btnCalculation").remove();
            $("iframe").contents().find("#btnImages").css({"visibility": "hidden","position": "absolute"});
            $("iframe").contents().find("#graPaTbCalcResult").remove();
            $("iframe").contents().find("#btnHailGraphics").remove();
            $("iframe").contents().find(".graPaToolbarSpacer").hide();
            $("iframe").contents().find("#btnAdditionalPositionsEditEnabled").remove();
            $("iframe").contents().find("#openPartSelectionLegend").remove();
            $("iframe").contents().find("#graPaItaliaCalcDAT").remove();
        }

        /*versione 10-2019 - begin*/
        $("iframe").contents().find("#graPaItaliaCalcDAT").hide();
        $("iframe").contents().find("#btnGrapaMainMenu").hide();
        $("iframe").contents().find(".graPaScrollPanel").css("left","5px");
        $("iframe").contents().find("#graPaActBtn_damageReport").hide();
        $("iframe").contents().find("#graPaActBtn_evaluation").hide();
        $("iframe").contents().find("#graPaActBtn_calcComment").hide();
        $("iframe").contents().find("#graPaActBtn_graPaSettings").hide();
        $("iframe").contents().find("#graPaActBtn_toolbarSettings").hide();
        $("iframe").contents().find("#graPaActBtn_print").hide();
        /*versione 10-2019 - end*/

        /*versione 10-2019 - begin*/
        if ($("iframe").contents().find("#italiaRcDialog").is(":visible")) {
            $("iframe").contents().find(".btnFlatCalculateDAT").hide();
            $("iframe").contents().find("#btnOpenVI").hide();
            $("iframe").contents().find(".graPaTextButtons").css("width", "calc(100% - 40px)");
            //added on 21/05/2020
            $("iframe").contents().find("#btnQuickSelRepairPedia").hide();
            $("iframe").contents().find("#btnCalculateDAT").hide();
            $("iframe").contents().find("#btnHighlight").hide();
        }

        if ($("iframe").contents().find("#graPaImagesTitle").is(":visible")) $("iframe").contents().find("#graPaImgClose").click()
        /*versione 10-2019 - end*/

        if ($("iframe").contents().find(".graPaExtSelDialog").length > 0) {
            /*versione 10-2019 - begin*/
            $("iframe").contents().find("#btnOpenCommentToDVN").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#btnOpenVI").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find(".graPaTextButtons").css("width", "calc(100% - 40px)");
            /*versione 10-2019 - end*/
            //added on 21/05/2020
            $("iframe").contents().find("#btnQuickSelRepairPedia").css({
                "visibility": "hidden",
                "position": "absolute"
            });
            $("iframe").contents().find("#btnCalculateDAT").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#btnHighlight").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#btnShowInGroup").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#graPaOpenCommentLeft").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#graPaOpenCommentRight").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#graPaOpenImgsRight").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#graPaOpenImgsLeft").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#graPaOpenImgsRight").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#btnSelItalia").css({"visibility": "hidden", "position": "absolute"});

            $("iframe").contents().find("#extSelworkLevelRow").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#graPaExtSelOther").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#extSelnfaRow").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#extSelusedPartRow").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#extSelpreDamageRow").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#extSelworkTypeRow").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#extSelsupplementDifficultyRow").css({
                "visibility": "hidden",
                "position": "absolute"
            });
            $("iframe").contents().find("#extSellocationRow").css({"visibility": "hidden", "position": "absolute"});
            $("iframe").contents().find("#extSellacquerMaterialRow").css({
                "visibility": "hidden",
                "position": "absolute"
            });
            $("iframe").contents().find("#extSellacquerPercentageRow").css({
                "visibility": "hidden",
                "position": "absolute"
            });
            $("iframe").contents().find("#extSellacquerPercentageRow").css({
                "visibility": "hidden",
                "position": "absolute"
            });
            $("iframe").contents().find("#nullRow").css({"visibility": "hidden", "position": "absolute"});
            if ($("iframe").contents().find("#extSellacquerLevel").val() == "-1") {
                $("iframe").contents().find("#extSellacquerLevel").val("3").change();
                $("iframe").contents().find("#extSellacquerLevelRow").css({
                    "visibility": "hidden",
                    "position": "absolute"
                });
            } else {
                $("iframe").contents().find("#extSellacquerLevelRow").css({
                    "visibility": "hidden",
                    "position": "absolute"
                });
            }
            $("iframe").contents().find(".graPaExtSelTitle:contains(Lavoro)").css({
                "visibility": "hidden",
                "position": "absolute"
            });
            $("iframe").contents().find(".graPaExtSelTitle:contains(Pezzo di ricambio)").css({
                "visibility": "hidden",
                "position": "absolute"
            });
            $("iframe").contents().find(".graPaExtSelTitle:contains(Verniciatura)").css({
                "visibility": "hidden",
                "position": "absolute"
            });

            if ($("iframe").contents().find("#extSelprice").length > 0) {
                $("iframe").contents().find("#extSelprice").val("").change();
                $("iframe").contents().find("#extSelprice").parent().css({
                    "visibility": "hidden",
                    "position": "absolute"
                });
            }

            if ($("iframe").contents().find("#extSeldiscount").length > 0) {
                $("iframe").contents().find("#extSeldiscount").val("").change();
                $("iframe").contents().find("#extSeldiscount").parent().css({
                    "visibility": "hidden",
                    "position": "absolute"
                });
            }
            if ($("iframe").contents().find(".graPaRcState:textEquals(SOST)").length > 0 || $("iframe").contents().find(".graPaRcState:textEquals(RIS)").length > 0 || $("iframe").contents().find(".graPaRcState:textEquals(Spese)").length > 0 || $("iframe").contents().find(".graPaRcState:textEquals(LA)").length > 0 || $("iframe").contents().find(".graPaRcState:textEquals(SR)").length > 0 || $("iframe").contents().find(".graPaRcState:textEquals(VE)").length > 0 || $("iframe").contents().find(".graPaRcState:textEquals(V.Sm)").length > 0 || $("iframe").contents().find(".graPaRcState:textEquals(SPOT)").length > 0) {
            } else {
                $("iframe").contents().find(".graPaRcState:textEquals(E)").text("SOST");
                //$("iframe").contents().find(".graPaRcState:textEquals(E)").parent().css("right":"0px !important","left':'10px");
                //$("iframe").contents().find(".graPaRcState:textEquals(R)").text("RIS");
                $("iframe").contents().find(".graPaRcHail").parent().css({
                    "visibility": "hidden",
                    "position": "absolute"
                });

                $("iframe").contents().find(".graPaRcState:textEquals(K)").parent().css({
                    "visibility": "hidden",
                    "position": "absolute"
                });

                $("iframe").contents().find(".graPaRcState:textEquals(R)").parent().css({
                    "visibility": "hidden",
                    "position": "absolute"
                });
                $("iframe").contents().find(".graPaRcState:textEquals(/)").parent().css({
                    "visibility": "hidden",
                    "position": "absolute"
                });
                $("iframe").contents().find(".graPaRcState:textEquals(Z)").parent().css({
                    "visibility": "hidden",
                    "position": "absolute"
                });
                $("iframe").contents().find(".graPaRcState:textEquals(P)").parent().css({
                    "visibility": "hidden",
                    "position": "absolute"
                });


                $("iframe").contents().find(".graPaRcState:textEquals(M)").parent().css({
                    "visibility": "hidden",
                    "position": "absolute"
                });
                $("iframe").contents().find(".graPaRcState:textEquals(C)").parent().css({
                    "visibility": "hidden",
                    "position": "absolute"
                });


                $("iframe").contents().find(".graPaRcState:textEquals(S)").parent().css({
                    "visibility": "hidden",
                    "position": "absolute"
                });

                $("iframe").contents().find(".graPaRcState:textEquals(I)").text("LA");
                $("iframe").contents().find(".graPaRcState:textEquals(A)").text("SR");
                $("iframe").contents().find(".graPaRcState:textEquals(L)").text("VE");
                $("iframe").contents().find(".graPaRcTabRight > label.graPaExtSelCheckbox").css("left", "8px");
                //$("iframe").contents().find(".graPaRcTabLeft > label.graPaExtSelCheckbox").css("right","9px");
                //$("iframe").contents().find(".graPaRcTabLeft > .graPaRcState").css({"right":"5px","text-align":"left"});
                $("iframe").contents().find(".graPaRcTabRight > .graPaRcState").css({
                    "right": "8px",
                    "text-align": "left"
                });
                z1 = "prova";
            }
        } else {
            z1 = 0;
        }

        var z2 = 0;
        if ($("iframe").contents().find(".graPaQuickSelectionHeader.graPaQuickSelectionRcCol").is(":visible")) {
            if (z2 == 0) {
                $("iframe").contents().find(".graPaQuickSelectionLabel").not(":eq(0)").not("[text*='*']").css("font-weight", "normal");
                $("iframe").contents().find(".graPaQuickSelectionLabel:contains(*)").css("font-weight", "bold");
                $("iframe").contents().find(".graPaQuickSelectionLabel:contains(*)").css("text-transform", "uppercase");
                $("iframe").contents().find(".graPaQuickSelectionHeader.graPaQuickSelectionRcCol:contains(E)").text("Sost.");
                $("iframe").contents().find(".graPaQuickSelectionLcCol").remove();
                z2 = 1;
            }
        } else {
            z2 = 0;
        }
    }
}, 1000);

});


//GRAPASEL
$("#customField-button-Comunicazioni").removeClass("disabled disableLink");
$("#customField-button-attachment").removeClass("disabled disableLink");
$("#customField-button-buttonContractOpening").removeClass("disabled disableLink");
$("#customField-button-buttonCalculation").removeClass("disabled disableLink");
$("#customField-button-Comunicazioni").removeAttr("disabled");
$("#customField-button-attachment").removeAttr("disabled");
$("#customField-button-buttonContractOpening").removeAttr("disabled");
$("#customField-button-buttonCalculation").removeAttr("disabled");
if ($("#claim-attachments").is(":visible")) {
    $("#claim-attachments").toggle();
}
//ATTACHMENT PREVIEW
$("#claim-saveButtons").on("click", function() {
    $(".core-button.publish").click();
});
$(".core-button.publish").on("click", function() {
    var contaJPG2 = $(".filename:contains(.JPG)").length;
    var contaJPG = $(".filename:contains(.jpg)").length;
    var contaPNG2 = $(".filename:contains(.PNG)").length;
    var contaPNG = $(".filename:contains(.png)").length;
    var contaJPEG2 = $(".filename:contains(.JPEG)").length;
    var contaJPEG = $(".filename:contains(.jpeg)").length;
    var contaBMP = $(".filename:contains(.bmp)").length;
    var contaBMP2 = $(".filename:contains(.BMP)").length;
    var sommaFoto = parseInt(contaJPG) + parseInt(contaPNG) + parseInt(contaJPEG) + parseInt(contaBMP) + parseInt(contaJPG2) + parseInt(contaPNG2) + parseInt(contaJPEG2) + parseInt(contaBMP2);
    $("#customField-input-attachmentNumber").val(sommaFoto).change();
    if ($("#customField-input-attachmentNumber").val() >= 3 && $("#tab-calculationPreview").is(":visible")) {
        $("#customField-control-MissingConditions").css("display", "none");
    }
});
$("#customField-button-attachment").on("click", function() {
    if ($("#vroContractOpeningTab").css("marginLeft") == '320px') {
        if ($("#claim-attachments").is(":visible")) {
            $("#claim-attachments").hide();
            $("#vroContractOpeningTab").css("margin-left", "48px");
            $("#vroContractOpeningTab").css("width", "calc(100% - 48px)");
			$("#claim-attachments").addClass("hiddenAbsolute");
        } else {
            $("#claim-attachments").show();
			$("#claim-attachments").removeClass("hiddenAbsolute");
        }
    } else {
        $("#vroContractOpeningTab").css("margin-left", "320px");
        $("#vroContractOpeningTab").css("width", "calc(100% - 320px)");
        $("#claim-attachments").show();
		$("#claim-attachments").removeClass("hiddenAbsolute");
    }
    var contaJPG2 = $(".filename:contains(.JPG)").length;
    var contaJPG = $(".filename:contains(.jpg)").length;
    var contaPNG2 = $(".filename:contains(.PNG)").length;
    var contaPNG = $(".filename:contains(.png)").length;
    var contaJPEG2 = $(".filename:contains(.JPEG)").length;
    var contaJPEG = $(".filename:contains(.jpeg)").length;
    var contaBMP = $(".filename:contains(.bmp)").length;
    var contaBMP2 = $(".filename:contains(.BMP)").length;
    var sommaFoto = parseInt(contaJPG) + parseInt(contaPNG) + parseInt(contaJPEG) + parseInt(contaBMP) + parseInt(contaJPG2) + parseInt(contaPNG2) + parseInt(contaJPEG2) + parseInt(contaBMP2);
    $("#customField-input-attachmentNumber").val(sommaFoto).change();
    if ($("#customField-input-attachmentNumber").val() >= 3) {
        $("#customField-control-MissingConditions").css("display", "none");
        //  $(".statusActionButton").show();
    }
});

//END OF ATTACHMENT PREVIEW
function ConfirmDialog2(message) {
    $('<div></div>').appendTo('body').html('<div><h6>' + message + '</h6></div>').append($("#ConfermaDetrIVA")).append(" SI RICORDA DI ALLEGARE EVENTUALE DICHIARAZIONE SU SOL.").dialog({
        modal: true,
        title: 'Detrazione IVA',
        zIndex: 10000,
        autoOpen: true,
        width: '450px',
        resizable: false,
        buttons: {
            Conferma: function() {
                if ($("#customField-input-string_vatEntitled").val() == "NO") {
                    $(".loadingDialog").hide();
                    $(this).dialog("close");
                    $("#customField-button-MissingConditions2").css("display", "none");
                } else {
                    if (!$("#customField-input-VATDeductionDialog").val() == "") {
                        $(".loadingDialog").hide();
                        $(this).dialog("close");
                        $("#customField-button-MissingConditions2").css("display", "none");
                    } else {
                        alert("Si prega di inserire la percentuale IVA da detrarre");
                    }
                }
            },
            Annulla: function() {
                $("#customField-input-VATDeductionDialog").val("").change();
                $("#ConfermaDetrIVA").appendTo("#ConfermaDetrIVAContainer");
                $(".loadingDialog").hide();
                $(this).dialog("close");
            }
        },
        close: function(event, ui) {
            $(".loadingDialog").hide();
            $(this).remove();
        }
    });
    $("#customField-button-Si2").on("click", function() {
        $("#customField-button-Si2").css({
            "background-color": "#da2825",
            "color": "white"
        });
        $("#customField-button-No2").css({
            "background-color": "#EEE",
            "color": "black"
        });

        setRecuperoIVA(true);
        $("#customField-control-VATDeduction2").parent().removeClass("hiddenAbsolute");
    });
    $("#customField-button-No2").on("click", function() {
        $("#customField-button-No2").css({
            "background-color": "#da2825",
            "color": "white"
        });
        $("#customField-button-Si2").css({
            "background-color": "#EEE",
            "color": "black"
        });
        //$("#customField-control-vatEntitled input[type='radio']:eq(0)").trigger("click");
        //$("#customField-input-VATDeduction2").val("").change();
        setRecuperoIVA();
        $("#customField-control-VATDeduction2").parent().addClass("hiddenAbsolute");
    });
}

$("#customField-input-VATDeduction2").on("change", function(){
    $("#customField-input-VATDeductionDialog").val($(this).val()).change();
});
//Handle the Manual optimisation Button - START
$("#customField-button-SPOptimization").on("click", function() {
    require(["templates/ASPISContractOpening"], function(ASPISContractOpening) {
        var arr = $("#vroContractOpeningTab").closest(".template").template("form").included.filter(function(o) {
            return o instanceof ASPISContractOpening;
        })
        if (arr.length) {
            arr[0].getEligibilitySigned().done(function(confirmed) {
                if (confirmed) {
                    $(".optimizeCalc").click();
                }
            });
        }
    });
});
//Handle the Manual optimisation Button - END
var x = 0;
var y = 0;
setInterval(function() {
    if (x == 0) {
        $("#graphicalPartSelection > iframe").on("load", function() {
            x = 1;
        });
    }
    if (!$("#graphicalPartSelection").is(":visible")) {
        x = 0;
    }
}, 600);

function convertImgToBase64URL(url, callback, outputFormat) {
    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function() {
        var canvas = document.createElement('CANVAS'),
            ctx = canvas.getContext('2d'),
            dataURL;
        canvas.height = img.height;
        canvas.width = img.width;
        /* canvas.width = 237;
         var  height = img.height;
    	var ratio = img.width / 237;  
    	  canvas.height= img.height / ratio; */
        ctx.drawImage(img, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        callback(dataURL);
        canvas = null;
    };
    img.src = url;
}
$("#customField-button-OrderParts").on("click", function() {
    if ($("#customField-input-ASPISOrderBodyshop").is(":checked")) {} else {
        $("#customField-button-ASPISOrderBodyshop2").css("background-image", "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAiCAYAAAA6RwvCAAAACXBIWXMAAA+QAAAPkAF7Vr0pAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAANnSURBVHjavNhdaNV1HMfx1zk7Z+essSRaObTRA0pZUZgMsyQUbyoKL4JkEbPnAvFi2Bhm9nSjBsPoSboQRCxKupAoR0gG1VYzccIaUbToadDSlcxlez5d7Lc66vmfp7a+8Lv4ne/5/8/79/19f9/v53difvd/Wg0ewz4MZDtiBUHiqJoFhEmMaMGL+BlteB3jhUEq8AeOIPYfIea51M16ZdRmeT7GRvQmCkbjr7CGMwGsVIvhNDbZYrVaZ87yrsbn2J4fZAILsBLdSJUBMYIVlmv0hNHIvFmYHyQT8mN+2NXqEkGmUCnpQa+oVnlONGbsVzyfKPiyCdyEBqRLBBlFg0fdqCECAp7Fb7Gijm8srK5Uy7hYwlcy6mRyfuN4WOJEosgXlpeoPJUHAjaHmIvNYUG7FsfypPh7WDszSZg7eyESImlYxtPZH80VyN24JzLffrBFWk9pJb50mxeqzpXneVLo06vNMnGj2dU6PgfRaM0JMXPyPtRq3KhMmIcx21uzGM05PWl86X09PshVGGcb5LmcZS+OIaPaPWNypt/OHcga3JfTk0CvNyR0WxJVM2cnWeP4BLdG/sqIG4zrj8rKRI5OuBLtJYKsj4SYrswbpfW7IN9KUmFXk6AJB7Ef9UVCLMT2PP5P8ZZMEEgRo8IqfI/TLjHfmzJqcB0ag5o4XqDlvZwnGhO4P4iIAnu7Hutw1GZpC7J8dXgVHbg94vlbQhSjbC86i9NQy3CZxXbpVq3aZKTqfClswcmsEnUonJZcNoSlId5FZPsAHrFDbSTEjIzeFGT0A+GU3JsHAnYUCzG9qiZr7XbAcNAdxdlHuByLIvw/4noMF3/+LzLiWz3SJZW3NXkgprChFIjpkFfo84U9hpxwleWqVJUlC/+1/dhW6kMVFqHShK91OeZdS02pscRUyVKZtD+lNEoalAy1qciROOtaOaDPKc3qvIYWPFy0Wk3iqDYnfVOOvk2cN5vuBd/hcbyDrVj1j4jJldCVOOEXG+zUX14rLfTIYRwWc6cxrVJukzinjcdCzHbZKu6Ua8pLrOLYUw7q0K7TOg/Zpt4VxjAWbn9duuy211TZ144iQaZlXkant/3kkBWa3KFZvXqDxu3xpKvLSu8yhFEsdOkRgw7YqcM+d2kxZsgRn7kw/HNQpv09AAHd4dj0qhdhAAAAAElFTkSuQmCC')");
        $("#customField-input-ASPISOrderBodyshop").click().change();
    }
});
setTimeout(function() {
    if (user.role.match(/INSURANCE/) || user.role.match(/MANUFACTURER/)) {
        if ($(".qaStatusWrapper a:contains(Nuovo)").length) {
            setTimeout(function() {
                var mailExpert = user.email;
                $("#customField-input-expertMail").val(mailExpert).change();
                // $("#customField-input-NotAgreed").val("1").change();
                if ($(".qaStatusWrapper a:contains(N.A.)").length) {}
                // $(".statusActionButton").hide();
            }, 600);
        }
    }
    if (user.role.match(/EXPERT/)) {
        if ($("#customField-input-assignmentID").val() == "") {
            var DamageNumber = $("#customField-input-DamageNumber").val();
            $("#customField-input-assignmentID").val(DamageNumber).change();
        }
        $("div.role.role-EXPERT").hide();
        $("#customField-input-Utente").val("perito");
        if ($(".qaStatusWrapper a:contains(Nuovo)").length) {
            setTimeout(function() {
                var mailExpert = user.email;
                $("#customField-input-expertMail").val(mailExpert).change();
                $("#customField-input-NotAgreed").val("1").change();
                if ($(".qaStatusWrapper a:contains(N.A.)").length) {}
                //  $(".statusActionButton").hide();
            }, 600);
        } else {
            //   if($(".qaStatusWrapper:contains(N.A.)").length){
            //if($("#customField-input-AspisBodyshopId").val()=="" || $("#customField-input-AspisBodyshopId").val()=="61922"){
            //  var ASPISExpID=$("#customField-input-AspisExpertId").val();
            // $("#customField-input-AspisBodyshopId").val(ASPISExpID).change();
            // }
            //   }
            /*  else{
                 var ASPISBodyShopID=$("#customField-input-AspisBodyshopId").val();
                 var ASPISExpID=$("#customField-input-AspisExpertId").val();
                 if(ASPISExpID!=ASPISBodyShopID){
                 	$("#customField-input-AspisExpertId2").val(ASPISExpID).change();
                 	$("#customField-input-AspisExpertId").val(ASPISBodyShopID).change();
                 } 
              }*/
        }
        $("#customField-input-AuthorityCheck").val("1");
        $("#customField-input-AuthorityCheck").change();
        $("#customField-input-Utente").change();
        $("#claim-saveDraft").click();
    }
    if (user.role.match(/REPAIRER/)) {
        /*	$("#customField-input-EnteringDate").on("change",function(){
  $("#claim-saveDraft").click();
}); */
        $("div.role.role-REPAIRER").hide();
        $("#customField-control-ania_calculations").css({
            "visibility": "hidden",
            "position": "absolute"
        });
        $("#customField-input-Utente").val("carrozzeria");
        $("#customField-input-Utente").change();
        $("#customField-input-AuthorityCheck").val("0");
        $("#customField-input-AuthorityCheck").change();
        $("#customField-control-assignmentREPAIRER").hide();
    }
    if (user.role.match(/INSURANCE/)) {
        $("div.role.role-INSURANCE").hide();
        $("#customField-input-Utente").val("assicurazione");
        $("#customField-input-Utente").change();
    }
    if (user.role.match(/MANUFACTURER/)) {
        $("#customField-input-Utente").val("costruttore");
        $("#customField-input-Utente").change();
    }
}, 800);
setTimeout(function() {
    $("#customField-button-SendComment").removeAttr("disabled");
    $("#customField-input-Comment").removeClass("disabled disableLink");
    $("#customField-input-Comment").removeAttr("disabled");
    //	$("#customField-button-OrderPartsReminder").removeClass("disabled disableLink");
    //	$("#customField-button-OrderPartsReminder").removeAttr("disabled");
    $("#customField-button-OrderParts").removeClass("disabled disableLink");
    $("#customField-button-OrderParts").removeAttr("disabled");
    $("#customField-button-ASPISOrderBodyshop2").removeClass("disabled disableLink");
    $("#customField-button-ASPISOrderBodyshop2").removeAttr("disabled");
    $("#customField-button-DatiPratica").removeClass("disabled disableLink");
    $("#customField-button-DatiPratica").removeAttr("disabled");
    $("#customField-button-Totali").removeClass("disabled disableLink");
    $("#customField-button-Totali").removeAttr("disabled");
    $("#customField-button-Liquidazione").removeClass("disabled disableLink");
    $("#customField-button-Liquidazione").removeAttr("disabled");
    $("#customField-button-DatiSinistro").removeClass("disabled disableLink");
    $("#customField-button-DatiSinistro").removeAttr("disabled");
    $("#customField-button-DatiPolizza").removeClass("disabled disableLink");
    $("#customField-button-DatiPolizza").removeAttr("disabled");
    $("#customField-button-DinamicaSinistro").removeClass("disabled disableLink");
    $("#customField-button-DinamicaSinistro").removeAttr("disabled");
    $("#customField-button-NumberPlateSearch").removeClass("disabled");
    $("#customField-button-VINSearch").removeClass("disabled");
    
	/*if ($("#customField-button-importAXA").is(":visible"))
	{
		$("#customField-button-VehicleSelSearch").addClass("hiddenAbsolute");
	}*/
	$("#customField-button-VehicleSelSearch").removeClass("disabled");
	$("#customField-button-Equipments").removeClass("disabled");
    $("#customField-button-Comunicazioni").removeClass("disabled disableLink");
    $("#customField-button-attachment").removeClass("disabled disableLink");
    $("#customField-button-buttonContractOpening").removeClass("disabled disableLink");
    $("#customField-button-buttonCalculation").removeClass("disabled disableLink");
    $("#customField-button-Comunicazioni").removeAttr("disabled");
    $("#customField-button-attachment").removeAttr("disabled");
    $("#customField-button-printer").removeAttr("disabled");
    $("#customField-button-flag").removeAttr("disabled");
    $("#customField-button-mail").removeAttr("disabled");
    $("#customField-button-printer").removeClass("disabled disableLink");
    $("#customField-button-mail").removeClass("disabled disableLink");
    $("#customField-button-flag").removeClass("disabled disableLink");
    $("#customField-button-buttonContractOpening").removeAttr("disabled");
    $("#customField-button-buttonCalculation").removeAttr("disabled");
    $(".lastPage").removeClass("disabled disableLink");
}, 1000);
$(".document.editable:contains('Perizie')").hide();
//VERTICAL MENU ACTIONS
$("#customField-button-DatiPratica").addClass("btnSelected");
$("#customField-button-Totali").addClass("btnSelected");
$("#customField-button-DatiSinistro").on("click", function() {
    $("[class*='btnSelected']").removeClass("btnSelected");
    $(this).removeClass('btnDeselected').addClass('btnSelected');
    $("#DatiSinistro").css("display", "inline-block");
    $("#DinamicaSinistro").css("display", "inline-block");
    $("#DatiPolizza").css("display", "inline-block");
    $("#Comunicazione").hide();
    $("#datiVeicolo,#DatiPratica").hide();
    $("#Assicurato,#Locatario,#driver").hide();
    $("#ProprietarioAssicurato").hide();
    $("#NotePerito").hide();
});
$("#customField-button-DatiPratica").on("click", function() {
    $("[class*='btnSelected']").removeClass("btnSelected");
    $(this).removeClass('btnDeselected').addClass('btnSelected');
    $("#datiVeicolo,#DatiPratica").show();
    $("#Assicurato,#Locatario,#driver").show();
    $("#DinamicaSinistro").hide();
    $("#Comunicazione").hide();
    $("#DatiPolizza").hide();
    $("#DatiSinistro").hide();
    $("#ProprietarioAssicurato").show();
    $("#NotePerito").hide();
});
$("#customField-button-DatiPolizza").on("click", function() {
    $("[class*='btnSelected']").removeClass("btnSelected");
    $(this).removeClass('btnDeselected').addClass('btnSelected');
    $("#DatiPolizza").show();
    $("#Assicurato,#Locatario,#driver").hide();
    $("#DinamicaSinistro").hide();
    $("#Comunicazione").hide();
    $("#datiVeicolo,#DatiPratica").hide();
    $("#DatiSinistro").hide();
    $("#ProprietarioAssicurato").hide();
    $("#NotePerito").hide();
});
$("#customField-button-NotePerito").on("click", function() {
    $("[class*='btnSelected']").removeClass("btnSelected");
    $(this).removeClass('btnDeselected').addClass('btnSelected');
    $("#DatiPolizza").hide();
    $("#Assicurato,#Locatario,#driver").hide();
    $("#DinamicaSinistro").hide();
    $("#Comunicazione").hide();
    $("#datiVeicolo,#DatiPratica").hide();
    $("#DatiSinistro").hide();
    $("#ProprietarioAssicurato").hide();
    $("#NotePerito").show();
});
$("#Liquidazione").addClass("hiddenAbsolute");


$("#customField-button-Liquidazione").on("click", function() {
    /*
	if ($("#customField-control-vatEntitled input[type='radio']:eq(0)").is(":checked")) {
        $("#customField-control-VATDeduction").hide();
        $("#customField-input-VATDeduction").val("");
    };
    var TotalSum = parseFloat($("tr.active > td.field-totalPrice.BigCurrency").text().replace(".", "").replace(",", ".")).toFixed(2);
    $("#customField-input-totalImp").val(TotalSum).change();
	*/
    $("[class*='btnSelected']").removeClass("btnSelected");
    $(this).removeClass('btnDeselected').addClass('btnSelected');
    $("#Liquidazione").removeClass("hiddenAbsolute");
    $("#calculationPreview").css({
        "visibility": "hidden",
        "position": "absolute"
    });
    $(".lastPage").hide();
});

$("#customField-button-Totali").on("click", function() {
    $("[class*='btnSelected']").removeClass("btnSelected");
    $(this).removeClass('btnDeselected').addClass('btnSelected');
    $("#Liquidazione").addClass("hiddenAbsolute");
    $("#calculationPreview").css({
        "visibility": "visible",
        "position": "relative"
    });
    $(".lastPage").show();
});
$("#customField-button-DinamicaSinistro").on("click", function() {
    $("[class*='btnSelected']").removeClass("btnSelected");
    $(this).removeClass('btnDeselected').addClass('btnSelected');
    $("#Comunicazione").hide();
    $("#DinamicaSinistro").show();
    $("#Assicurato,#Locatario,#driver").hide();
    $("#datiVeicolo,#DatiPratica").hide();
    $("#DatiPolizza").hide();
    $("#DatiSinistro").hide();
});
$("#customField-button-mail").text("Comunicazioni");
$("#customField-button-mail").on("click", function() {
    if (user.role.match(/EXPERT/)) {
        if (!$("#customField-input-LettoPerito").is(":checked")) {
            $("#customField-input-LettoPerito").click();
        }
    }
    if (user.role.match(/REPAIRER/)) {
        if (!$("#customField-input-LettoCarrozzeria").is(":checked")) {
            $("#customField-input-LettoCarrozzeria").click();
        }
    }
    $(".loadingDialog").show();
    $("#Comunicazione").removeClass("hiddenAbsolute");
});
$("#customField-button-closeComunicazione").on("click", function() {
    $(".loadingDialog").hide();
    $("#Comunicazione").addClass("hiddenAbsolute");
});
//REMOVING OF LAYOUT-LEFT
if ($("#layout-left").is(":visible")) {
    $("#toggler-left").click();
}
setTimeout(function() {
    if ($("#customField-input-vtype").val() == "") {
        $(".tools").css("visibility","hidden");
    }
    var immagine = $("#customField-input-srcImage").val();
    if (immagine != "") {
        document.getElementById('customField-input-VehicleImage').style.background = 'url(' + immagine + ')';
        var imageSrc = document.getElementById('customField-input-VehicleImage').style.backgroundImage.replace(/url\((['"])?(.*?)\1\)/gi, '$2').split(',')[0];
        // I just broke it up on newlines for readability        
        var image = new Image();
        image.src = imageSrc;
        var maxWidth = 230;
        var width = image.width;
        var height = image.height;
        var ratio = width / maxWidth; // get ratio for scaling image
        var finalHeight = height / ratio;
        $("#customField-input-VehicleImage").css("height", finalHeight, "important");
    }
}, 300);
if ($("td.field-status > a:nth-child(1)").text() == "") {
    $("#customField-input-addressFirstNameInsurant").change();
    $("#claim-saveDraft").click();
}
//	$("#customField-button-buttonContractOpening").appendTo("#vroContractOpeningTab > ul:nth-child(1)");
$("#customField-button-buttonContractOpening").appendTo('#vroContractOpeningTab > ul >li[aria-controls="tab-contractOpening"]');
$("#customField-button-buttonCalculation").appendTo('#vroContractOpeningTab > ul >li[aria-controls="tab-calculationPreview"]');
// $("#customField-button-buttonCalculation").appendTo('#vroContractOpeningTab > ul:nth-child(1)');
$("#customField-button-buttonContractOpening").on('click', function() {

    clearInterval(intervalloGrapa);

    var inter2 = setInterval(function() {
        if ($("#tab-contractOpening").is(":visible")) {
            if ($("#claim-attachments").is(":visible")) {
                $("#customField-button-attachment").click();
            }
            clearInterval(inter2);
        }
    }, 600);
    var contaJPG2 = $(".filename:contains(.JPG)").length;
    var contaJPG = $(".filename:contains(.jpg)").length;
    var contaPNG2 = $(".filename:contains(.PNG)").length;
    var contaPNG = $(".filename:contains(.png)").length;
    var contaJPEG2 = $(".filename:contains(.JPEG)").length;
    var contaJPEG = $(".filename:contains(.jpeg)").length;
    var contaBMP = $(".filename:contains(.bmp)").length;
    var contaBMP2 = $(".filename:contains(.BMP)").length;
    var sommaFoto = parseInt(contaJPG) + parseInt(contaPNG) + parseInt(contaJPEG) + parseInt(contaBMP) + parseInt(contaJPG2) + parseInt(contaPNG2) + parseInt(contaJPEG2) + parseInt(contaBMP2);
    $("#customField-input-attachmentNumber").val(sommaFoto).change();

    $('a[href*=tab-contractOpening]').trigger("click");
    $("#tab-contractOpening").css("position", "relative");
    var interval5 = setInterval(function() {
        if ($('#tab-contractOpening').is(":visible")) {
            setTimeout(function() {
                $("#customField-input-ColoreVeicolo").change();
                $("#customField-input-paintKind").change();
                $("#customField-input-srcImage").change();
                clearInterval(interval5);
            }, 500);
        }
    }, 600);

});
//PARTE DI CALCOLO

$("#customField-input-attachmentNumber").on("change", function() {
    if ($("#customField-input-attachmentNumber").val() == "0") {
        $("#customField-input-attachmentNumber").val("  ").change();
    }
});

var check3 = 1;
$("#customField-button-buttonCalculation").on('click', function() {

    if (!user.role.match(/EXPERT/)) {
        hideManufacturerCalculation();
        contaFoto();
        checkMissingConditions();
        if (check3 == "1") {
            $("#customField-button-MissingConditions2").text("CONTROLLO DETRAIBILITA' IVA");
            check3 = 0;
        }
    }

    if (user.role.match(/EXPERT/)) {
        if ($(".qaStatusWrapper a:contains(N.A. - In lavorazione)").length) {
            $(".notAgreedStyle").show();
        }
        $("#customField-control-vatEntitled > div.radioLabelcontrol > label").addClass("required");
    }


    clearInterval(intervalloGrapa);
	
	/*FB 20200831 - start - calcoli per preservare valori inseriti nel vecchio template*/
	if ($("#customField-control-vatEntitled input[type='radio']").eq(1).is(":checked"))
	{
		$("#customField-input-string_vatEntitled").val("SI").change();
	}
	if ($("#customField-control-vatEntitled input[type='radio']").eq(0).is(":checked"))
	{
		$("#customField-input-string_vatEntitled").val("NO").change();
	}
	if ($("#customField-control-CoeffRiduzione input[type='radio']").eq(0).is(":checked"))
	{
		$("#customField-input-coefficienteRiduzioneSiNo").val("SI").change();
	}
	if ($("#customField-control-CoeffRiduzione input[type='radio']").eq(1).is(":checked"))
	{
		$("#customField-input-coefficienteRiduzioneSiNo").val("NO").change();
	}

	checkNewVsOld("commercialValue","float_commercialValue");
	checkNewVsOld("InsuredValue","float_insuredValue");
	checkNewVsOld("replacementValue","float_replacementValue");
	checkNewVsOld("insuranceFailPercent","float_insuranceFailPercent");
	checkNewVsOld("reductionCoefficient","float_reductionCoefficientShown");
	checkNewVsOld("totaleRicambi","float_totaleRicambi");
	checkNewVsOld("deductionParts","float_deductionParts");
	checkNewVsOld("insuranceFailure","float_insuranceFailure");
	checkNewVsOld("totalImp","float_NetValueFinal");
	checkNewVsOld("commercialValue2","float_commercialValue");
	checkNewVsOld("PercentageUncovered2","float_percentualeScoperto");
	checkNewVsOld("FinalTotal","float_amountAgreed");
	checkNewVsOld("VATDeduction","float_VATDeduction");
	checkNewVsOld("PolicyMinimalUnconvered","float_valoreFranchigia");
	/*FB 20200831 - end*/
	
    /*FB 20190716 - start*/
    var intervalPositions = setInterval(function() {
        if ($('a[href="#calculationPreview-positions"]').length) {
            $('a[href="#calculationPreview-positions"]').on("click", function() {
                $("#calculationPreview-positions th.field-lacquer_working_time.Float > span").text("VE");
                $("#calculationPreview-positions th.field-spare_part_number > span").text("Codice Ricambio");
            });
            clearInterval(intervalPositions);
        }
    }, 600);
    /*FB 20190716 - end*/
    /* simone latina 11-06-2019 alphascale optimization change   */
    var intervalD = setInterval(function() {
        if ($("#vroContractOpeningTab li[aria-controls='tab-calculationPreview']").attr("aria-selected") == "true") {
            clearInterval(intervalD)
            if ($("#customField-input-memoAscale").val() == "" || $("#customField-input-memoAscale").val() == undefined) {
                $("#customField-button-btnSpoChangeDialog").hide();
            }
        }
    }, 200)
    $("#claim-summary").on("refresh_calculation", function(event, calculation) {
        if ($("#customField-input-memoAscale").val() == "" || $("#customField-input-memoAscale").val() == undefined) {
            $("#customField-button-btnSpoChangeDialog").hide();
        } else {
            $("#customField-button-btnSpoChangeDialog").show();
        }
        if (firstArray && firstArray.length !== 0) {
            firstArray = checkIfDeletedPart(firstArray);
            $("#customField-input-memoAscale2").val(JSON.stringify(firstArray)).change();
            isChanged(firstArray, changedArray)
            changedArray = JSON.parse($("#customField-input-memoAchanged").val());
        }
    });
    /* simone Latina end code  */
    //$(".MancataRicezioneAuto").addClass("hiddenAbsolute");
    var contaJPG2 = $(".filename:contains(.JPG)").length;
    var contaJPG = $(".filename:contains(.jpg)").length;
    var contaPNG2 = $(".filename:contains(.PNG)").length;
    var contaPNG = $(".filename:contains(.png)").length;
    var contaJPEG2 = $(".filename:contains(.JPEG)").length;
    var contaJPEG = $(".filename:contains(.jpeg)").length;
    var contaBMP = $(".filename:contains(.bmp)").length;
    var contaBMP2 = $(".filename:contains(.BMP)").length;
    var sommaFoto = parseInt(contaJPG) + parseInt(contaPNG) + parseInt(contaJPEG) + parseInt(contaBMP) + parseInt(contaJPG2) + parseInt(contaPNG2) + parseInt(contaJPEG2) + parseInt(contaBMP2);
    $("#customField-input-attachmentNumber").val(sommaFoto).change();
    if ($("#graphicalPartSelection").is(":visible")) {
		
        if (!$(".readOnlyOverlay").is(":visible")) {
			controlloGrafico();
        } else {
            $('a[href*=tab-calculationPreview]').trigger("click");
            var inter = setInterval(function() {
                if ($("#calculationPreview").is(":visible")) {
                    if (!$("#layout-right").is(":visible")) {
                        $("#customField-button-flag").click();
                    }
                    if ($("#claim-attachments").is(":visible")) {
                        $("#customField-button-attachment").click();
                    }
                    clearInterval(inter);
                }
            }, 500);
        }
    } else {
        $('a[href*=tab-calculationPreview]').trigger("click");
        var inter = setInterval(function() {
            if ($("#calculationPreview").is(":visible")) {
                if (!$("#layout-right").is(":visible")) {
                    $("#customField-button-flag").click();
                }
                if ($("#claim-attachments").is(":visible")) {
                    $("#customField-button-attachment").click();
                }
                clearInterval(inter);
            }
        }, 500);
    }
    var interval1 = setInterval(function() {
        if ($('a[href*=calculationPreview-positions]').is(":visible") && !$(".loading").is(":visible")) {
            $('a[href*=calculationPreview-positions]').on("click", function() {
                setTimeout(function() {
                    $(".statusActionButton:contains('Concludi Pratica (Autorizzata)')").show();
                    $(".statusActionButton:contains('Richiedi Autorizzazione')").show();
                    $(".statusActionButton:contains('Sblocca a Carrozzeria')").show();
                    $(".statusActionButton:contains('Autorizza senza pagamento')").show();
                    $(".statusActionButton:contains('Rifiuta Incarico')").show();
                }, 100);
            });
            $('a[href*=calculationPreview-price]').on("click", function() {
                setTimeout(function() {
                    $(".statusActionButton:contains('Concludi Pratica (Autorizzata)')").show();
                    $(".statusActionButton:contains('Richiedi Autorizzazione')").show();
                    $(".statusActionButton:contains('Sblocca a Carrozzeria')").show();
                    $(".statusActionButton:contains('Autorizza senza pagamento')").show();
                    $(".statusActionButton:contains('Rifiuta Incarico')").show();
                }, 100);
            });
            clearInterval(interval1);
        }
    }, 600);
});
this.vroDomusCalculationOptions = {
    tabGraphicalPartSelection: $("#tab-graphicalPartSelection"),
    tabActivityRelated: $("#tab-activityRelatedSelection"),
};
/* DMS DIALOG MASK */

$("#customField-button-closeTechnicalData").addClass("closeButton");
$("#customField-button-closeTechnicalData").on("click", function() {
    $("#technicalDataPopup").addClass("overlayHiddenDialog");
    $("#technicalDataPopup").removeClass("overlayVisibleDialog");
	$(".loadingDialog").hide();
});
$("#customField-button-TechnicalData").on("click", function() {
    $("#technicalDataPopup").addClass("overlayVisibleDialog");
    $("#technicalDataPopup").removeClass("overlayHiddenDialog");
	$(".loadingDialog").show();
});
/*OWNER*/
$("#customField-button-closeOwner").addClass("closeButton");
$("#customField-button-closeOwner").on("click", function() {
    $("#ownerPopup").addClass("overlayHiddenDialog");
    $("#ownerPopup").removeClass("overlayVisibleDialog");
	$(".loadingDialog").hide();
});
$("#customField-button-datiFiscali").on("click", function() {
    $("#ownerPopup").addClass("overlayVisibleDialog");
    $("#ownerPopup").removeClass("overlayHiddenDialog");
	$(".loadingDialog").show();
});
//OTHER POLICY DATA
$("#customField-button-closePolicy").addClass("closeButton");
$("#customField-button-closePolicy").on("click", function() {
    $("#policyDataPopup").addClass("overlayHiddenDialog");
    $("#policyDataPopup").removeClass("overlayVisibleDialog");
	$(".loadingDialog").hide();
});
$("#policyDataPopup").addClass("overlayHiddenDialog");
$("#customField-button-AltriDatiPolizza").on("click", function() {
    $("#policyDataPopup").addClass("overlayVisibleDialog");
    $("#policyDataPopup").removeClass("overlayHiddenDialog");
	$(".loadingDialog").show();
});
$("#ownerPopup").contents().find("#addressBook-clear").on("click", function() {
    $("#customField-input-ownerData").val("");
});
/*CUSTOMER*/
$("#customField-button-close").addClass("closeButton");
$("#customField-button-close").on("click", function() {
    $("#customerPopup").addClass("overlayHiddenDialog");
    $("#customerPopup").removeClass("overlayVisibleDialog");
	$(".loadingDialog").hide();
});
$("#customField-control-customerData").on("click", function() {
    $("#customerPopup").addClass("overlayVisibleDialog");
    $("#customerPopup").removeClass("overlayHiddenDialog");
	$(".loadingDialog").show();
});
$("#customField-button-VehVisButton").on("click", function() {
    $("#vehicleVisibilityPopup").addClass("overlayVisibleDialog");
    $("#vehicleVisibilityPopup").removeClass("overlayHiddenDialog");
	$(".loadingDialog").show();
});
$("#customField-button-closeVehVis").addClass("closeButton");
$("#customField-button-closeVehVis").on("click", function() {
    $("#vehicleVisibilityPopup").addClass("overlayHiddenDialog");
    $("#vehicleVisibilityPopup").removeClass("overlayVisibleDialog");
	$(".loadingDialog").hide();
});
$("#customerPopup").contents().find("#addressBook-clear").on("click", function() {
    $("#customField-input-customerData").val("");
});
/*EXPERT*/
$("#customField-button-closeExpert").addClass("closeButton");
$("#customField-button-closeExpert").on("click", function() {
    $("#expertPopup").addClass("overlayHiddenDialog");
    $("#expertPopup").removeClass("overlayVisibleDialog");
	$(".loadingDialog").hide();
});
$("#customField-control-expertData").on("click", function() {
    $("#expertPopup").addClass("overlayVisibleDialog");
    $("#expertPopup").removeClass("overlayHiddenDialog");
	$(".loadingDialog").show();
});
$("#expertPopup").contents().find("#addressBook-clear").on("click", function() {
    $("#customField-input-expertData").val("");
});
/*EQUIPMENTS*/
$("#customField-button-closeEquipments").addClass("closeButton");
$("#customField-button-closeEquipments").on("click", function() {
    $('a[href*=tab-contractOpening]').trigger("click");
    $("#tab-contractOpening").css("position", "relative");
});

/*ACTIVITY RELATED DATA */
$("#customField-button-closeManodopera").addClass("closeButton");
$("#customField-button-closeManodopera").on("click", function() {
    /*SALVO LISTINO RICAMBI AGGIORNATO AL*/
    var listinoRicambi = $("iframe").contents().find("#SparePartFactor_priceDate-button").text().substring(3);
    $("#customField-input-sparePartsPriceList").val(listinoRicambi).change();
    if ($("#customField-input-vtype").val() != "") {
        $(".tools").css("visibility","visible");
    }
    if ($("#activityRelatedSelection > div > iframe").contents().find("#GarageRulesFactor_selectedGarageRule > option:selected").val() == "") {
        var manodoperaSelected = $("iframe").contents().find("#GarageRulesFactor_baseGarageRule").val();
        $("#customField-input-LabourChosen").val(manodoperaSelected).change();
    } else {
        var manodoperaSelected = $("iframe").contents().find("#GarageRulesFactor_selectedGarageRule-button").text();
        $("#customField-input-LabourChosen").val(manodoperaSelected).change();
    }
    $(".infoVehicle2").show();
    if ($(".qaStatusWrapper a:contains(N.A.)").length) {
        $(".infoVehicle3").show();
    }
    $('#tab-activityRelatedSelection').css({
        "visibility": "hidden",
        "left": "20000px"
    });
    if ($("iframe").contents().find(".ManufacturerLacquerFactor_wageModePerHour").is(":visible") || $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_type-button").is(":visible")) {
        var verniciaturaDAT = $("iframe").contents().find("#ManufacturerLacquerFactor_type-button").text();
    } else {
        var verniciaturaDAT = $("iframe").contents().find("#EuroLacquerFactor_type-button").text();
    }
    var colore = $("iframe.sphinxPage").contents().find("input[id*=colorName]").val().replace("\xDF", "ss").replace("\xDF", "ss").replace("\xFC", "u").replace("\xFC", "u").replace("\xF6", "o").replace("\xF6", "o").replace("\xE4", "a").replace("\xE4", "a").replace("\xE8", "e").replace("\xE9", "e");
    $("iframe.sphinxPage").contents().find("input[id*=colorName]").val(colore).change();
    var lacquerTypeVal;
    if (verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("doppio") || verniciaturaDAT.toLowerCase().contains("past.doppio")) {
        lacquerTypeVal = "pastel/doppio";
    } else {
        if (verniciaturaDAT.toLowerCase().contains("metallizato") && verniciaturaDAT.toLowerCase().contains("triplo") || verniciaturaDAT.toLowerCase().contains("met.triplo")) {
            lacquerTypeVal = "met/triplo";
        } else {
            if (verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("triplo") || verniciaturaDAT.toLowerCase().contains("past.triplo")) {
                lacquerTypeVal = "pastel/triplo";
            } else {
                if (verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("mono") || verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("1-strato")) {
                    lacquerTypeVal = "pastel/mono";
                } else {
                    if (verniciaturaDAT.toLowerCase().contains("metallizzato") && verniciaturaDAT.toLowerCase().contains("mono")) {
                        lacquerTypeVal = "met/mono";
                    } else {
                        if (verniciaturaDAT.toLowerCase().contains("perlato") && verniciaturaDAT.toLowerCase().contains("mono")) {
                            lacquerTypeVal = "perl/mono";
                        } else {
                            if (verniciaturaDAT.toLowerCase().contains("metallizzato") && verniciaturaDAT.toLowerCase().contains("doppio") || verniciaturaDAT.toLowerCase().contains("met.doppio")) {
                                lacquerTypeVal = "met/doppio";
                            } else {
                                if (verniciaturaDAT.toLowerCase().contains("perlato") && verniciaturaDAT.toLowerCase().contains("doppio") || verniciaturaDAT.toLowerCase().contains("per.doppio")) {
                                    lacquerTypeVal = "perl/doppio";
                                } else {
                                    if (verniciaturaDAT.toLowerCase().contains("perlato") && verniciaturaDAT.toLowerCase().contains("triplo") || verniciaturaDAT.toLowerCase().contains("per.triplo")) {
                                        lacquerTypeVal = "perl/triplo";
                                    } else {
                                        if (verniciaturaDAT.toLowerCase().contains("speciale")) {
                                            lacquerTypeVal = "vern/specia";
                                        } else {
                                            if (verniciaturaDAT.toLowerCase().contains("perl") && verniciaturaDAT.toLowerCase().contains("antig")) {
                                                lacquerTypeVal = "perl/antigr";
                                            } else {
                                                if (verniciaturaDAT.toLowerCase().contains("met") && verniciaturaDAT.toLowerCase().contains("traspa")) {
                                                    lacquerTypeVal = "met/traspa";
                                                } else {
                                                    if (verniciaturaDAT.toLowerCase().contains("per") && verniciaturaDAT.toLowerCase().contains("traspa")) {
                                                        lacquerTypeVal = "perl/traspa";
                                                    } else {
                                                        if (verniciaturaDAT.toLowerCase().contains("past") && verniciaturaDAT.toLowerCase().contains("traspa")) {
                                                            lacquerTypeVal = "pastel/trasp";
                                                        } else {
                                                            lacquerTypeVal = verniciaturaDAT.toLowerCase().substring(0, 13);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    $("#customField-input-ColoreVeicolo").val(colore).change();
    $("#customField-input-paintKind").val(lacquerTypeVal).change();
    $('a[href*=tab-contractOpening]').trigger("click");
    $("#tab-contractOpening").css("position", "relative");
    $("#customField-input-paintKind").val(lacquerTypeVal).change();
    $("#customField-input-ColoreVeicolo").val(colore).change();


    var interval5 = setInterval(function() {
        if ($('#tab-contractOpening').is(":visible")) {
            setTimeout(function() {
                $("#customField-input-ColoreVeicolo").change();
                $("#customField-input-paintKind").change();
                $("#customField-input-LabourChosen").change();
                $("#customField-input-srcImage").change();
                $("#customField-button-LabourSetting").text($("#customField-input-LabourChosen").val());
                $("#customField-button-kindOfPaint").text($("#customField-input-paintKind").val());
                $("#claim-saveDraft").click();
                clearInterval(interval5);
            }, 500);
        }
    }, 600);
    /* if($("#customField-input-EnteringDate").val()=="" && !user.role.match(/EXPERT/)){
     ConfirmDialog('Il veicolo si trova in carrozzeria? Se S\u00CC inserire la Data Accettazione.');
     
     }else{*/
    $(".loadingDialog").hide();
    //  }
});
$("#customField-button-MissingConditions").on("click", function() {
    if (!$("#claim-attachments").is(":visible")) {
        $("#customField-button-attachment").click();
    }
});
$("#customField-button-MissingConditions2").on("click", function() {
    $(".loadingDialog").show();
    $("#ConfermaDetrIVA").removeClass("hiddenAbsolute");
    ConfirmDialog2('Il cliente detrae IVA? Se S\u00CC, si prega di inserire la percentuale di detraibilit\u00E0.');
    $("#ConfermaDetrIVA").parent().height("245px");
    $("#ConfermaDetrIVA").parent().css('background','#F8F8F8');
});
/*EDIT KIND OF PAINT */
$("#customField-button-kindOfPaint").on("click", function() {
    var Autored = setInterval(function() {
        if ($('#tab-activityRelatedSelection').is(':visible')) {
            $(".loadingDialog").show();
            $('#tab-contractOpening').css("display", "block");
            $('#tab-contractOpening').css("position", "absolute");
            clearInterval(Autored);
        } else {
            $('#tab-contractOpening').css("display", "block");
            $('#tab-contractOpening').css("position", "absolute");
        }
    }, 50);
    $('a[href*=tab-activityRelatedSelection]').trigger("click");
    $('#tab-activityRelatedSelection').css("visibility", "hidden");
    $('#tab-activityRelatedSelection').css({
        "visibility": "hidden",
        "left": "20000px"
    });
    $("#manodoperaPopup").removeClass("overlayVisibleDialog");
    $("#manodoperaPopup").addClass("overlayHiddenDialogVehSel");
    var autoRed2 = setInterval(function() {
        if ($("iframe").contents().find(".ManufacturerLacquerFactor_wageModePerHour").is(":visible") || $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_type-button").is(":visible")) {
            var verniciaturaDAT = $("iframe").contents().find("#ManufacturerLacquerFactor_type-button").text();
        } else {
            var verniciaturaDAT = $("iframe").contents().find("#EuroLacquerFactor_type-button").text();
        }
        var colore = $("iframe.sphinxPage").contents().find("input[id*=colorName]").val();
        var lacquerTypeVal;
        if (verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("doppio") || verniciaturaDAT.toLowerCase().contains("past.doppio")) {
            lacquerTypeVal = "pastel/doppio";
        } else {
            if (verniciaturaDAT.toLowerCase().contains("metallizato") && verniciaturaDAT.toLowerCase().contains("triplo") || verniciaturaDAT.toLowerCase().contains("met.triplo")) {
                lacquerTypeVal = "met/triplo";
            } else {
                if (verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("triplo") || verniciaturaDAT.toLowerCase().contains("past.triplo")) {
                    lacquerTypeVal = "pastel/triplo";
                } else {
                    if (verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("mono") || verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("1-strato")) {
                        lacquerTypeVal = "pastel/mono";
                    } else {
                        if (verniciaturaDAT.toLowerCase().contains("metallizzato") && verniciaturaDAT.toLowerCase().contains("mono")) {
                            lacquerTypeVal = "met/mono";
                        } else {
                            if (verniciaturaDAT.toLowerCase().contains("perlato") && verniciaturaDAT.toLowerCase().contains("mono")) {
                                lacquerTypeVal = "perl/mono";
                            } else {
                                if (verniciaturaDAT.toLowerCase().contains("metallizzato") && verniciaturaDAT.toLowerCase().contains("doppio") || verniciaturaDAT.toLowerCase().contains("met.doppio")) {
                                    lacquerTypeVal = "met/doppio";
                                } else {
                                    if (verniciaturaDAT.toLowerCase().contains("perlato") && verniciaturaDAT.toLowerCase().contains("doppio") || verniciaturaDAT.toLowerCase().contains("per.doppio")) {
                                        lacquerTypeVal = "perl/doppio";
                                    } else {
                                        if (verniciaturaDAT.toLowerCase().contains("perlato") && verniciaturaDAT.toLowerCase().contains("triplo") || verniciaturaDAT.toLowerCase().contains("per.triplo")) {
                                            lacquerTypeVal = "perl/triplo";
                                        } else {
                                            if (verniciaturaDAT.toLowerCase().contains("speciale")) {
                                                lacquerTypeVal = "vern/specia";
                                            } else {
                                                if (verniciaturaDAT.toLowerCase().contains("perl") && verniciaturaDAT.toLowerCase().contains("antig")) {
                                                    lacquerTypeVal = "perl/antigr";
                                                } else {
                                                    if (verniciaturaDAT.toLowerCase().contains("met") && verniciaturaDAT.toLowerCase().contains("traspa")) {
                                                        lacquerTypeVal = "met/traspa";
                                                    } else {
                                                        if (verniciaturaDAT.toLowerCase().contains("per") && verniciaturaDAT.toLowerCase().contains("traspa")) {
                                                            lacquerTypeVal = "perl/traspa";
                                                        } else {
                                                            if (verniciaturaDAT.toLowerCase().contains("past") && verniciaturaDAT.toLowerCase().contains("traspa")) {
                                                                lacquerTypeVal = "pastel/trasp";
                                                            } else {
                                                                lacquerTypeVal = verniciaturaDAT.toLowerCase().substring(0, 13);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $("#customField-input-ColoreVeicolo").val(colore).change();
        $("#customField-input-paintKind").val(lacquerTypeVal).change();
		
		naManodopera(autoRed2);
		
        if ($("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_type-button").is(":visible")) {
            //	var lacquerTypeVal=	$("iframe").contents().find("#EuroLacquerFactor_type-button").text();
            //	$("#customField-input-paintKind").val(lacquerTypeVal).change();
            var lacquerType = $("iframe.sphinxPage").contents().find("#EuroLacquerFactor_type").val();
            var materialFlat = $("iframe.sphinxPage").contents().find("#EuroLacquerFactor_wage[class*='mandatory']").length;
            var checker1 = 0;
            if (materialFlat == 0) {
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_repairWageLacquerMaterialFlatPercRate").hide();
                checker1 = 1;
            } else {
                if (!$("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li > span > input#EuroLacquerFactor_materialFlatRatePercent").val() == "") {
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_repairWageLacquerMaterialFlatPercRate").hide();
                    checker1 = 1;
                }
            }
            if (!user.role.match(/INSURANCE/)) {
                $("iframe.sphinxPage").contents().find("#GarageRulesFactorChapter").hide();
                $("iframe.sphinxPage").contents().find("#CalculationFactorChapter").hide();
                $("iframe.sphinxPage").contents().find("#SparePartFactorChapter").hide();
                $("iframe.sphinxPage").contents().find("#LabourCostFactorChapter").hide();
                $("iframe.sphinxPage").contents().find("#DiscountRuleFactorChapter").hide();
                $("iframe.sphinxPage").contents().find("#DomusCalculationFactorChapter").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterHeader").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("ul.ui-tabs-nav").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#euroLacquerFactor > ul > li.EuroLacquerFactor_colorName").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#euroLacquerFactor > ul > li.EuroLacquerFactor_colorCode").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find(".EuroLacquerFactor_addition").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_repairWageLacquerMaterialPerPoint").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_materialIndex").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#euroLacquerFactor > ul > li.EuroLacquerFactor_repairWageLacquerWageTitle").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > h3").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_repairWageLacquerWageTitle").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_wageModePerHour").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_wageModePerHour").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_wageModeLumpSum").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_discountWage").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_discountWageBiw").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_repairWageLacquerMaterialTitle").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_repairWageLacquerMaterialFlatAmount").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_discountMaterial").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_discountMaterialBiw").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#euroLacquerFactor > ul > li.EuroLacquerFactor_preparationTimePercent").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#euroLacquerFactor > ul > li.EuroLacquerFactor_isSurchargeForSecondColor").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#euroLacquerFactor > ul > li.EuroLacquerFactor_isSurchargeForMatt").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul").css("border", "none");
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_repairWageLacquerMaterialByManufacturer").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_materialManufSt2Price").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_materialSpecialLacquer").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_repairWageLacquerMaterialFlatRatePercent").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_repairWageLacquerMaterialFlatRateAbsolute").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_isReducedPreparationTime").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_isFrameworkUse").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_isApplyUndercoatLacquerWhenRemoved").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_isLacquerPlasticWhenFitted").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_maskingWorkGlassCount").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_maskingWorkPlasticCount").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_matBlackWindowFrameCount").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#euroLacquerFactor > ul > li.EuroLacquerFactor_isLacquerAssemblyWhenRemoved").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_preparationTimePercent").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor > ul > br").hide();
            }
            $("#activityRelatedSelection").show();
            $('#tab-activityRelatedSelection').css({
                "visibility": "visible",
                "left": "0px"
            });
            $("#manodoperaPopup").addClass("overlayVisibleDialog");
            $("#manodoperaPopup").removeClass("overlayHiddenDialogVehSel");
            $("#activityRelatedSelection").css("visibility", "visible");
            clearInterval(autoRed2);
        }
        if ($("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find(".ManufacturerLacquerFactor_wageModePerHour").is(":visible") || $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_type-button").is(":visible")) {
            var verniciaturaDAT = $("iframe").contents().find("#ManufacturerLacquerFactor_type-button").text();
            var colore = $("iframe.sphinxPage").contents().find("input[id*=colorName]").val();
            var lacquerTypeVal;
            if (verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("doppio") || verniciaturaDAT.toLowerCase().contains("past.doppio")) {
                lacquerTypeVal = "pastel/doppio";
            } else {
                if (verniciaturaDAT.toLowerCase().contains("metallizato") && verniciaturaDAT.toLowerCase().contains("triplo") || verniciaturaDAT.toLowerCase().contains("met.triplo")) {
                    lacquerTypeVal = "met/triplo";
                } else {
                    if (verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("triplo") || verniciaturaDAT.toLowerCase().contains("past.triplo")) {
                        lacquerTypeVal = "pastel/triplo";
                    } else {
                        if (verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("mono") || verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("1-strato")) {
                            lacquerTypeVal = "pastel/mono";
                        } else {
                            if (verniciaturaDAT.toLowerCase().contains("metallizzato") && verniciaturaDAT.toLowerCase().contains("mono")) {
                                lacquerTypeVal = "met/mono";
                            } else {
                                if (verniciaturaDAT.toLowerCase().contains("perlato") && verniciaturaDAT.toLowerCase().contains("mono")) {
                                    lacquerTypeVal = "perl/mono";
                                } else {
                                    if (verniciaturaDAT.toLowerCase().contains("metallizzato") && verniciaturaDAT.toLowerCase().contains("doppio") || verniciaturaDAT.toLowerCase().contains("met.doppio")) {
                                        lacquerTypeVal = "met/doppio";
                                    } else {
                                        if (verniciaturaDAT.toLowerCase().contains("perlato") && verniciaturaDAT.toLowerCase().contains("doppio") || verniciaturaDAT.toLowerCase().contains("per.doppio")) {
                                            lacquerTypeVal = "perl/doppio";
                                        } else {
                                            if (verniciaturaDAT.toLowerCase().contains("perlato") && verniciaturaDAT.toLowerCase().contains("triplo") || verniciaturaDAT.toLowerCase().contains("per.triplo")) {
                                                lacquerTypeVal = "perl/triplo";
                                            } else {
                                                if (verniciaturaDAT.toLowerCase().contains("speciale")) {
                                                    lacquerTypeVal = "vern/specia";
                                                } else {
                                                    if (verniciaturaDAT.toLowerCase().contains("perl") && verniciaturaDAT.toLowerCase().contains("antig")) {
                                                        lacquerTypeVal = "perl/antigr";
                                                    } else {
                                                        if (verniciaturaDAT.toLowerCase().contains("met") && verniciaturaDAT.toLowerCase().contains("traspa")) {
                                                            lacquerTypeVal = "met/traspa";
                                                        } else {
                                                            if (verniciaturaDAT.toLowerCase().contains("per") && verniciaturaDAT.toLowerCase().contains("traspa")) {
                                                                lacquerTypeVal = "perl/traspa";
                                                            } else {
                                                                if (verniciaturaDAT.toLowerCase().contains("past") && verniciaturaDAT.toLowerCase().contains("traspa")) {
                                                                    lacquerTypeVal = "pastel/trasp";
                                                                } else {
                                                                    lacquerTypeVal = verniciaturaDAT.toLowerCase().substring(0, 13);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $("#customField-input-ColoreVeicolo").val(colore).change();
            $("#customField-input-paintKind").val(lacquerTypeVal).change();
            var lacquerType = $("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_type").val();
            var materialFlat = $("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_materialFlatRatePercent[class*='mandatory']").length;
            var checker1 = 0;
            if (materialFlat == 0) {
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_repairWageLacquerMaterialFlatPercRate").hide();
                checker1 = 1;
            } else {
                if (!$("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li > span > input#ManufacturerLacquerFactor_materialFlatRatePercent").val() == "") {
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_repairWageLacquerMaterialFlatPercRate").hide();
                    checker1 = 1;
                }
            }
            if (!user.role.match(/INSURANCE/)) {
                $("iframe.sphinxPage").contents().find("#GarageRulesFactorChapter").hide();
                $("iframe.sphinxPage").contents().find("#CalculationFactorChapter").hide();
                $("iframe.sphinxPage").contents().find("#SparePartFactorChapter").hide();
                $("iframe.sphinxPage").contents().find("#LabourCostFactorChapter").hide();
                $("iframe.sphinxPage").contents().find("#DiscountRuleFactorChapter").hide();
                $("iframe.sphinxPage").contents().find("#DomusCalculationFactorChapter").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterHeader").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("ul.ui-tabs-nav").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#manufacturerLacquerFactor > ul > li.ManufacturerLacquerFactor_colorName").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#manufacturerLacquerFactor > ul > li.ManufacturerLacquerFactor_colorCode").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find(".ManufacturerLacquerFactor_addition").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#manufacturerLacquerFactor > ul > li.ManufacturerLacquerFactor_repairWageLacquerWageTitle").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > h3").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_repairWageLacquerWageTitle").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_wageModePerHour").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_wageModePerHour").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_wageModeLumpSum").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_discountWage").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_discountWageBiw").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_repairWageLacquerMaterialTitle").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_repairWageLacquerMaterialFlatAmount").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_discountMaterial").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_discountMaterialBiw").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor > ul > li.ManufacturerLacquerFactor_preparationTimePercent").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor > ul > li.ManufacturerLacquerFactor_isSurchargeForSecondColor").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor > ul > li.ManufacturerLacquerFactor_isSurchargeForMatt").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul").css("border", "none");
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_repairWageLacquerMaterialByManufacturer").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_materialManufSt2Price").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#manufacturerLacquerFactor > ul > li.ManufacturerLacquerFactor_isLacquerAssemblyWhenRemoved").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.ManufacturerLacquerFactor_preparationTimePercent").hide();
                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#manufacturerLacquerFactor > ul > br").hide();
            }
            $("#activityRelatedSelection").show();
            $('#tab-activityRelatedSelection').css({
                "visibility": "visible",
                "left": "0px"
            });
            $("#manodoperaPopup").addClass("overlayVisibleDialog");
            $("#manodoperaPopup").removeClass("overlayHiddenDialogVehSel");
            $("#activityRelatedSelection").css("visibility", "visible");
            clearInterval(autoRed2);
        }
    }, 600);
});
/*VEHICLE SELECTION */
$applicoRiparatore=0;
function applicaManodoperaRiparatore(){
	if(!$("#customField-input-repairer_labourRatesBody").val()=="" && !$("#customField-input-manodoperaRiparatoreApplicata").val()=="1"){
						$("#customField-input-manodoperaRiparatoreApplicata").val("1").change();
						var manodoperaCarrozzeria=$("#customField-input-repairer_labourRatesBody").val();
						$("iframe.sphinxPage").contents().find("input#LabourCostFactor_bodyWage1").val(manodoperaCarrozzeria).change();
						$("iframe.sphinxPage").contents().find("input#LabourCostFactor_bodyWage2").val(manodoperaCarrozzeria).change();
						$("iframe.sphinxPage").contents().find("input#LabourCostFactor_bodyWage3").val(manodoperaCarrozzeria).change();
					    $("iframe.sphinxPage").contents().find("input#LabourCostFactor_electricWage1").val(manodoperaCarrozzeria).change();
						$("iframe.sphinxPage").contents().find("input#LabourCostFactor_electricWage2").val(manodoperaCarrozzeria).change();
						$("iframe.sphinxPage").contents().find("input#LabourCostFactor_electricWage3").val(manodoperaCarrozzeria).change();
						$("iframe.sphinxPage").contents().find("input#EuroLacquerFactor_wage").val(manodoperaCarrozzeria).change();
						$("iframe.sphinxPage").contents().find("input#ManufacturerLacquerFactor_wage").val(manodoperaCarrozzeria).change();
						var manodoperaMeccanica=$("#customField-input-repairer_labourRatesMec").val();
					    $("iframe.sphinxPage").contents().find("input#LabourCostFactor_mechanicWage1").val(manodoperaMeccanica).change();
						$("iframe.sphinxPage").contents().find("input#LabourCostFactor_mechanicWage2").val(manodoperaMeccanica).change();
						$("iframe.sphinxPage").contents().find("input#LabourCostFactor_mechanicWage3").val(manodoperaMeccanica).change();
						/*sconto totale*/
						$("iframe.sphinxPage").contents().find("input#CalculationFactor_discount").val($("#customField-input-repairer_labourRatesTotalDiscount").val()).change();
						/*sconto ricambi*/
						$("iframe.sphinxPage").contents().find("input#SparePartFactor_discount").val($("#customField-input-repairer_labourRatesSPDiscount").val()).change();
						/*smaltimento rifiuti*/
						$("iframe.sphinxPage").contents().find("#DomusCalculationFactor_wasteDisposalCostsPercValue").val($("#customField-input-repairer_labourRatesWasteDisposal").val()).change();
						$("iframe.sphinxPage").contents().find("#DomusCalculationFactor_wasteDisposalCostsMaxValue").val($("#customField-input-repairer_labourRatesWasteDisposalMax").val()).change();
						/*doppiostrato,+15 etc.*/
						$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerMono").val($("#customField-input-repairer_labourRatesMonoCoat").val()).change();
						$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Mono").val($("#customField-input-repairer_labourRatesMonoCoat15").val()).change();
						$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerDouble").val($("#customField-input-repairer_labourRatesDoubleCoat").val()).change();
						$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Double").val($("#customField-input-repairer_labourRatesDoubleCoat15").val()).change();
						$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerTriple").val($("#customField-input-repairer_labourRatesTripleCoat").val()).change();
						$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Triple").val($("#customField-input-repairer_labourRatesTripleCoat15").val()).change();
						$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerQuad").val($("#customField-input-repairer_labourRatesQuadCoat").val()).change();
						$("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Quad").val($("#customField-input-repairer_labourRatesQuadCoat15").val()).change();

					}
}
 function manodoperaThrough(){
 $('a[href*=tab-activityRelatedSelection]').trigger("click");
        $('#tab-activityRelatedSelection').css({
            "visibility": "hidden",
            "left": "20000px"
        });
        $("#manodoperaPopup").removeClass("overlayVisibleDialog");
        $("#manodoperaPopup").addClass("overlayHiddenDialogVehSel");
        var autoRed2 = setInterval(function() {
            if ($(".qaStatusWrapper a:contains(N.A.)").length && $applicoRiparatore==0) {
                if ($("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_type-button").is(":visible") || $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find(".ManufacturerLacquerFactor_wageModePerHour").is(":visible") || $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_type-button").is(":visible")) {
					
					$("iframe.sphinxPage").contents().find("#CalculationFactorChapter > div.chapterBody > ul > li.CalculationFactor_discount_label > span:nth-child(1) > label").text("Sconto sul totale").change();
					$("iframe.sphinxPage").contents().find(".CalculationFactor_showLongWorkStrings_label").hide();
					$("iframe.sphinxPage").contents().find(".CalculationFactor_showAllIncludedWork_label").hide();
					$("iframe.sphinxPage").contents().find(".CalculationFactor_calculationWithoutWork_label").hide();
					$("iframe.sphinxPage").contents().find(".CalculationFactor_biwOptimizationMode_label").hide();
					$("iframe.sphinxPage").contents().find(".CalculationFactor_preDamageTotal_label").hide();
					$("iframe.sphinxPage").contents().find(".CalculationFactor_newForOld_label").hide();
					$("iframe.sphinxPage").contents().find(".CalculationFactor_appreciation_label").hide();
					$("iframe.sphinxPage").contents().find(".CalculationFactor_cfTimeUnit_label").hide();
					$("iframe.sphinxPage").contents().find(".CalculationFactor_vatRate_label").hide();
					$("iframe.sphinxPage").contents().find(".SparePartFactor_increaseDecrease_label").hide();
					$("iframe.sphinxPage").contents().find(".SparePartFactor_surchargeInProtocolOly_label").hide();
					$("iframe.sphinxPage").contents().find(".SparePartFactor_surchargeSeparated_label").hide();
					$("iframe.sphinxPage").contents().find(".SparePartFactor_sparePartsDisposalCosts_label").hide();
					$("iframe.sphinxPage").contents().find(".SparePartFactor_bracketSetRentCost_label").hide();
					$("iframe.sphinxPage").contents().find(".SparePartFactor_acquisitionCosts_label").hide();
					$("iframe.sphinxPage").contents().find(".SparePartFactor_bracketSetProcurementCost_label").hide();
					$("iframe.sphinxPage").contents().find(".SparePartFactor_procurementCost_label").hide();
					$("iframe.sphinxPage").contents().find(".SparePartFactor_bodyInWhiteProcurementCost_label").hide();
					$("iframe.sphinxPage").contents().find(".SparePartFactor_discountBiw_label").hide();
					$("iframe.sphinxPage").contents().find(".SparePartFactor_sparePartLumpSum_label").hide();
					$("iframe.sphinxPage").contents().find(".LabourCostFactor_title1_label").hide();
					$("iframe.sphinxPage").contents().find("#LabourCostFactor_electricWage2").hide();
					$("iframe.sphinxPage").contents().find("#LabourCostFactor_electricWage3").hide();
					$("iframe.sphinxPage").contents().find(".LabourCostFactor_dentsCountInProtocol_label").hide();
					$("iframe.sphinxPage").contents().find(".LabourCostFactor_isSuppressCavityProtection_label").hide();
					$("iframe.sphinxPage").contents().find(".LabourCostFactor_discount_label").hide();
					$("iframe.sphinxPage").contents().find(".LabourCostFactor_discountBiw_label").hide();
					$("iframe.sphinxPage").contents().find(".LabourCostFactor_labourLumpSum_label").hide();
					$("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_lacquerColorListGrp").contents().find("#ManufacturerLacquerFactor_color2ndName").parent().parent().hide();
					$("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_lacquerColorListGrp").contents().find("#ManufacturerLacquerFactor_color2ndCode").parent().parent().hide();
					$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_addition_label").hide();
					$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_preparationTimePercent_label").hide();
					$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_isSurchargeForSecondColor_label").hide();
					$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_isLacquerAssemblyWhenRemoved_label").hide();
					$("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_isSurchargeForMatt_label").hide();
					$("iframe.sphinxPage").contents().find(".LabourCostFactor_electricWage1_label").hide();
					$("iframe.sphinxPage").contents().find(".LabourCostFactor_dentWage_label").hide();
					
                    $("iframe.sphinxPage").contents().find(".SparePartFactor_increaseDecrease").hide();
                    $("iframe.sphinxPage").contents().find(".SparePartFactor_surchargeInProtocolOly").hide();
                    $("iframe.sphinxPage").contents().find(".SparePartFactor_surchargeSeparated").hide();
                    $("iframe.sphinxPage").contents().find(".SparePartFactor_sparePartsDisposalCosts").hide();
                    $("iframe.sphinxPage").contents().find(".SparePartFactor_bracketSetRentCost").hide();
                    $("iframe.sphinxPage").contents().find(".SparePartFactor_acquisitionCosts").hide();
                    $("iframe.sphinxPage").contents().find(".SparePartFactor_bracketSetProcurementCost").hide();
                    $("iframe.sphinxPage").contents().find(".SparePartFactor_procurementCost").hide();
                    $("iframe.sphinxPage").contents().find(".SparePartFactor_bodyInWhiteProcurementCost").hide();
                    $("iframe.sphinxPage").contents().find(".SparePartFactor_smallPartsChargeTitle").hide();
                    $("iframe.sphinxPage").contents().find("#SparePartFactor_smallSparePartCalculationModel").hide();
                    $("iframe.sphinxPage").contents().find("#SparePartFactor_discountMode0").hide();
                    $("iframe.sphinxPage").contents().find("#SparePartFactor_discountMode1").hide();
                    $("iframe.sphinxPage").contents().find("label[for='SparePartFactor_discountMode0']").hide();
                    $("iframe.sphinxPage").contents().find("label[for='SparePartFactor_discountMode1']").text("%");
                    $("iframe.sphinxPage").contents().find("br").hide();
                    $("iframe.sphinxPage").contents().find("#DiscountRuleFactorChapter").hide();
                    $("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_addition").hide();
                    $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_preparationTimePercent").hide();
                    $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_isLacquerAssemblyWhenRemoved").hide();
                    $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_addition").hide();
                    $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_discountWage").hide();
                    $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_discountWageBiw").hide();
                    $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_repairWageLacquerMaterialFlatAmount").hide();
                    $("iframe.sphinxPage").contents().find(".LabourCostFactor_dentsCountInProtocol").hide();
                    $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_discountMaterialBiw").hide();
                    $("iframe.sphinxPage").contents().find(".LabourCostFactor_discountBiw").hide();
                    $("iframe.sphinxPage").contents().find(".LabourCostFactor_labourLumpSum").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_addition").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_preparationTimePercent").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isReducedPreparationTime").hide();
                    $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_preparationTimePercent_preparationTimePercent").hide();
                    $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_isLacquerAssemblyWhenRemoved").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isFrameworkUse").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isApplyUndercoatLacquerWhenRemoved").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isFrameworkUse").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isSurchargeForSecondColor").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isLacquerPlasticWhenFitted").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_isLacquerAssemblyWhenRemoved").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_maskingWorkGlassCount").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_maskingWorkPlasticCount").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_matBlackWindowFrameCount").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_materialSpecialLacquer").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_repairWageLacquerMaterialFlatRateAbsolute").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_repairWageLacquerMaterialFlatRatePercent").hide();
                    $("iframe.sphinxPage").contents().find(".SparePartFactor_discountBiw").hide();
                    $("iframe.sphinxPage").contents().find(".SparePartFactor_sparePartLumpSum").hide();
                    $("iframe.sphinxPage").contents().find("#GarageRulesFactorChapter").hide();
                    $("iframe.sphinxPage").contents().find(".CalculationFactor_showLongWorkStrings").hide();
                    $("iframe.sphinxPage").contents().find(".CalculationFactor_showAllIncludedWork").hide();
                    $("iframe.sphinxPage").contents().find(".CalculationFactor_biwOptimizationMode").hide();
                    $("iframe.sphinxPage").contents().find(".CalculationFactor_preDamageTotal").hide();
                    $("iframe.sphinxPage").contents().find(".CalculationFactor_newForOld").hide();
                    $("iframe.sphinxPage").contents().find(".CalculationFactor_appreciation").hide();
                    $("iframe.sphinxPage").contents().find(".CalculationFactor_vatRate").hide();
                    $("iframe.sphinxPage").contents().find(".chapterHeader").hide();
                    $("iframe.sphinxPage").contents().find("#CalculationFactor_discountMode0").hide();
                    $("iframe.sphinxPage").contents().find("label[for='CalculationFactor_discountMode0']").hide();
                    $("iframe.sphinxPage").contents().find("label[for='CalculationFactor_discountMode1']").text("%");
                    $("iframe.sphinxPage").contents().find("#CalculationFactor_discountMode1").hide();
                    $("iframe.sphinxPage").contents().find("label[for='CalculationFactor_discount']").text("Extra Sconto %");
                    $("iframe.sphinxPage").contents().find("label[for='SparePartFactor_discount']").text("Sconto Ricambi %");
                    $("iframe.sphinxPage").contents().find("input#LabourCostFactor_mechanicWage1").on("focusout", function() {
                        var newInput = $("iframe.sphinxPage").contents().find("input#LabourCostFactor_mechanicWage1").val();
                        $("iframe.sphinxPage").contents().find("input#LabourCostFactor_mechanicWage1").val(newInput).change();
                        $("iframe.sphinxPage").contents().find("input#LabourCostFactor_electricWage1").val(newInput).change();
                        $("iframe.sphinxPage").contents().find("input#LabourCostFactor_dentWage").val(newInput).change();
                    });
                    $("iframe.sphinxPage").contents().find("input#LabourCostFactor_bodyWage1").on("focusout", function() {
                        var newInput2 = $("iframe.sphinxPage").contents().find("input#LabourCostFactor_bodyWage1").val();
                        $("iframe.sphinxPage").contents().find("input#LabourCostFactor_bodyWage1").val(newInput2).change();
                        $("iframe.sphinxPage").contents().find("input#EuroLacquerFactor_wage").val(newInput2).change();
                        $("iframe.sphinxPage").contents().find("input#ManufacturerLacquerFactor_wage").val(newInput2).change();
                    });
                    $("iframe.sphinxPage").contents().find("label[for='LabourCostFactor_mechanicWage1']").text("Manodopera Meccanica");
                    $("iframe.sphinxPage").contents().find("label[for='LabourCostFactor_bodyWage1']").text("Manodopera Carrozzeria");
                    $("iframe.sphinxPage").contents().find("#LabourCostFactor_mechanicWage2").hide();
                    $("iframe.sphinxPage").contents().find("#LabourCostFactor_mechanicWage3").hide();
                    //$("iframe.sphinxPage").contents().find(".LabourCostFactor_bodyWage1").hide();
                    $("iframe.sphinxPage").contents().find("input#LabourCostFactor_bodyWage2").hide();
                    $("iframe.sphinxPage").contents().find("input#LabourCostFactor_bodyWage3").hide();
                    $("iframe.sphinxPage").contents().find(".LabourCostFactor_electricWage1").hide();
                    $("iframe.sphinxPage").contents().find(".LabourCostFactor_dentWage").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_wage").hide();
                    $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_wage").hide();
                    $("iframe.sphinxPage").contents().find(".LabourCostFactor_discount").hide();
                    $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_calculationType").hide();
                    $("iframe.sphinxPage").contents().find(".LabourCostFactor_title1").hide();
                    $("iframe.sphinxPage").contents().find(".LabourCostFactor_title2").hide();
                    $("iframe.sphinxPage").contents().find(".LabourCostFactor_title3").hide();
                    $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerMono").on("focusout", function() {
                        var newInput = $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerMono").val();
                        $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Mono").val(newInput).change();
                        $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerDouble").val(newInput).change();
                        $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Double").val(newInput).change();
                        $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Triple").val(newInput).change();
                        $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerTriple").val(newInput).change();
                        $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerQuad").val(newInput).change();
                        $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Quad").val(newInput).change();
                    });
                    $("iframe.sphinxPage").contents().find("label[for='DomusCalculationFactor_layerAdd15Mono']").parent().hide();
                    $("iframe.sphinxPage").contents().find("label[for='DomusCalculationFactor_layerAdd15Triple']").parent().hide();
                    $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Mono").parent().hide();
                    $("iframe.sphinxPage").contents().find("input#DomusCalculationFactor_layerAdd15Triple").parent().hide();
                    $("iframe.sphinxPage").contents().find(".DomusCalculationFactor_layerDouble").hide();
                    $("iframe.sphinxPage").contents().find(".DomusCalculationFactor_layerTriple").hide();
                    $("iframe.sphinxPage").contents().find(".DomusCalculationFactor_layerQuad").hide();
                    $("iframe.sphinxPage").contents().find("#EuroLacquerFactor_calculationType").hide();
                    $("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_calculationType").hide();
                    $("iframe.sphinxPage").contents().find("li[aria-controls='euroLacquerFactor']").parent().hide();
                    $("iframe.sphinxPage").contents().find("label[for='SparePartFactor_priceDate-button']").text("Listino ricambi al");
                    $("iframe.sphinxPage").contents().find("label[for='DomusCalculationFactor_layerMono']").text("Materiale di consumo");
                    $("iframe.sphinxPage").contents().find(".ui-widget.ui-widget-content").css("border", "0px solid");
                    $("iframe.sphinxPage").contents().find("#DomusCalculationFactor_wasteDisposalCostsPercValue").val("1,50").change();
                    $("iframe.sphinxPage").contents().find("#DomusCalculationFactor_wasteDisposalCostsMaxValue").val("25,00").change();
                    $("iframe.sphinxPage").contents().find("#EuroLacquerFactor_materialFlatRatePercent").val("100").change();
                    $("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_materialFlatRatePercent").val("100").change();
					/*AGGIUNGERE CODICE QUI E VEDERE ELSE*/
					applicaManodoperaRiparatore();
                    $("iframe.sphinxPage").contents().find("#GarageRulesFactorChapter div ul li>span, #aztLacquerFactor ul li>span, #euroLacquerFactor ul li>span, #manufacturerLacquerFactor ul li>span").css("width", "267px");
                    $("iframe.sphinxPage").contents().find("label.increaseIndent").css("width", "271px");
                    $("iframe.sphinxPage").contents().find("label[for='DomusCalculationFactor_wasteDisposalCostsMaxValue']").css("width", "275px");
                    $("#manodoperaPopup > legend").text("Configurazione Manodopera");
                    $(".loadingDialog").show();
                    $('#tab-contractOpening').css("display", "block");
                    $('#tab-contractOpening').css("position", "absolute");
                    clearInterval(autoRed2);
                    $("#activityRelatedSelection").show();
                    $('#tab-activityRelatedSelection').css({
                        "visibility": "visible",
                        "left": "0px"
                    });
                    $("#manodoperaPopup").addClass("overlayVisibleDialog");
                    $("#manodoperaPopup").removeClass("overlayHiddenDialogVehSel");
                    $("#activityRelatedSelection").css("visibility", "visible");
                }
            } else {
                if ($("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_type-button").is(":visible")) {
                    if($applicoRiparatore==1){
					applicaManodoperaRiparatore();
					}
					if ($("#activityRelatedSelection > div > iframe").contents().find(".ui-dialog").contents().find(".btn_ok").is(":visible")) {
                        $("#activityRelatedSelection > div > iframe").contents().find(".ui-dialog").contents().find(".btn_ok").click();
                    }
                    var lacquerType = $("iframe.sphinxPage").contents().find("#EuroLacquerFactor_type").val();
                    var verniciaturaDAT = $("iframe").contents().find("#EuroLacquerFactor_type-button").text();
                    var colore = $("iframe.sphinxPage").contents().find("input[id*=colorName]").val();
                    var lacquerTypeVal;
                    if (verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("doppio") || verniciaturaDAT.toLowerCase().contains("past.doppio")) {
                        lacquerTypeVal = "pastel/doppio";
                    } else {
                        if (verniciaturaDAT.toLowerCase().contains("metallizato") && verniciaturaDAT.toLowerCase().contains("triplo") || verniciaturaDAT.toLowerCase().contains("met.triplo")) {
                            lacquerTypeVal = "met/triplo";
                        } else {
                            if (verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("triplo") || verniciaturaDAT.toLowerCase().contains("past.triplo")) {
                                lacquerTypeVal = "pastel/triplo";
                            } else {
                                if (verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("mono") || verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("1-strato")) {
                                    lacquerTypeVal = "pastel/mono";
                                } else {
                                    if (verniciaturaDAT.toLowerCase().contains("metallizzato") && verniciaturaDAT.toLowerCase().contains("mono")) {
                                        lacquerTypeVal = "met/mono";
                                    } else {
                                        if (verniciaturaDAT.toLowerCase().contains("perlato") && verniciaturaDAT.toLowerCase().contains("mono")) {
                                            lacquerTypeVal = "perl/mono";
                                        } else {
                                            if (verniciaturaDAT.toLowerCase().contains("metallizzato") && verniciaturaDAT.toLowerCase().contains("doppio") || verniciaturaDAT.toLowerCase().contains("met.doppio")) {
                                                lacquerTypeVal = "met/doppio";
                                            } else {
                                                if (verniciaturaDAT.toLowerCase().contains("perlato") && verniciaturaDAT.toLowerCase().contains("doppio") || verniciaturaDAT.toLowerCase().contains("per.doppio")) {
                                                    lacquerTypeVal = "perl/doppio";
                                                } else {
                                                    if (verniciaturaDAT.toLowerCase().contains("perlato") && verniciaturaDAT.toLowerCase().contains("triplo") || verniciaturaDAT.toLowerCase().contains("per.triplo")) {
                                                        lacquerTypeVal = "perl/triplo";
                                                    } else {
                                                        if (verniciaturaDAT.toLowerCase().contains("speciale")) {
                                                            lacquerTypeVal = "vern/specia";
                                                        } else {
                                                            if (verniciaturaDAT.toLowerCase().contains("perl") && verniciaturaDAT.toLowerCase().contains("antig")) {
                                                                lacquerTypeVal = "perl/antigr";
                                                            } else {
                                                                if (verniciaturaDAT.toLowerCase().contains("met") && verniciaturaDAT.toLowerCase().contains("traspa")) {
                                                                    lacquerTypeVal = "met/traspa";
                                                                } else {
                                                                    if (verniciaturaDAT.toLowerCase().contains("per") && verniciaturaDAT.toLowerCase().contains("traspa")) {
                                                                        lacquerTypeVal = "perl/traspa";
                                                                    } else {
                                                                        if (verniciaturaDAT.toLowerCase().contains("past") && verniciaturaDAT.toLowerCase().contains("traspa")) {
                                                                            lacquerTypeVal = "pastel/trasp";
                                                                        } else {
                                                                            lacquerTypeVal = verniciaturaDAT.toLowerCase().substring(0, 13);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $("#customField-input-ColoreVeicolo").val(colore).change();
                    $("#customField-input-paintKind").val(lacquerTypeVal).change();
                    var materialFlat = $("iframe.sphinxPage").contents().find("#EuroLacquerFactor_wage[class*='mandatory']").length;
                    var checker1 = 0;
                    if (lacquerType != "") {
                        $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_type").hide();
                    }
                    if (materialFlat == 0) {
                        $("iframe.sphinxPage").contents().find("#EuroLacquerFactor_wage[class*='mandatory']").hide();
                        checker1 = 1;
                    } else {
                        if (!$("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li > span > input#EuroLacquerFactor_materialIndex").val() == "" && !$("iframe.sphinxPage").contents().find("#EuroLacquerFactor_wage").val() == "") {
                            $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_materialIndex").hide();
                            $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_wageModePerHour").hide();
                            checker1 = 1;
                        } else {
                            $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_materialFlatRatePercent").val("4");
                            $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_materialIndex").hide();
                            if (!$("iframe.sphinxPage").contents().find("#EuroLacquerFactor_wage").val() == "") {
                                $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_wageModePerHour").hide();
                                checker1 = 1;
                            } else {
                                $("iframe.sphinxPage").contents().find(".EuroLacquerFactor_wageModePerHour").show();
                                checker1 = 0;
                            }
                        }
                    }
                    $("iframe.sphinxPage").contents().find("#GarageRulesFactorChapter").hide();
                    $("iframe.sphinxPage").contents().find("#CalculationFactorChapter").hide();
                    $("iframe.sphinxPage").contents().find("#SparePartFactorChapter").hide();
                    $("iframe.sphinxPage").contents().find("#LabourCostFactorChapter").hide();
                    $("iframe.sphinxPage").contents().find("#DiscountRuleFactorChapter").hide();
                    $("iframe.sphinxPage").contents().find("#DomusCalculationFactorChapter").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterHeader").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("ul.ui-tabs-nav").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#euroLacquerFactor > ul > li.EuroLacquerFactor_colorName").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#euroLacquerFactor > ul > li.EuroLacquerFactor_colorCode").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find(".EuroLacquerFactor_addition").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#euroLacquerFactor > ul > li.EuroLacquerFactor_repairWageLacquerWageTitle").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > h3").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_repairWageLacquerWageTitle").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_wageModePerHour").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_wageModePerHour").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_wageModeLumpSum").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_discountWage").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_discountWageBiw").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_repairWageLacquerMaterialTitle").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_repairWageLacquerMaterialFlatAmount").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_discountMaterial").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_discountMaterialBiw").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#euroLacquerFactor > ul > li.EuroLacquerFactor_preparationTimePercent").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#euroLacquerFactor > ul > li.EuroLacquerFactor_isSurchargeForSecondColor").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#euroLacquerFactor > ul > li.EuroLacquerFactor_isSurchargeForMatt").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul").css("border", "none");
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_repairWageLacquerMaterialByManufacturer").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_materialManufSt2Price").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_materialSpecialLacquer").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_repairWageLacquerMaterialFlatRatePercent").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor_calculationType > ul > li.EuroLacquerFactor_repairWageLacquerMaterialFlatRateAbsolute").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_isReducedPreparationTime").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_isFrameworkUse").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_isApplyUndercoatLacquerWhenRemoved").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_isLacquerPlasticWhenFitted").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_maskingWorkGlassCount").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_maskingWorkPlasticCount").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_matBlackWindowFrameCount").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#euroLacquerFactor > ul > li.EuroLacquerFactor_isLacquerAssemblyWhenRemoved").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.EuroLacquerFactor_preparationTimePercent").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#EuroLacquerFactor > ul > br").hide();
                    if (checker1 == 1 && lacquerType != "") {
                        if ($("#activityRelatedSelection > div > iframe").contents().find("#GarageRulesFactor_selectedGarageRule > option:selected").val() == "") {
                            var manodoperaSelected = $("iframe").contents().find("#GarageRulesFactor_baseGarageRule").val();
                            $("#customField-input-LabourChosen").val(manodoperaSelected).change();
                        } else {
                            var manodoperaSelected = $("iframe").contents().find("#GarageRulesFactor_selectedGarageRule-button").text();
                            $("#customField-input-LabourChosen").val(manodoperaSelected).change();
                        }
                        /*SALVO LISTINO RICAMBI AGGIORNATO AL*/
                        var listinoRicambi = $("iframe").contents().find("#SparePartFactor_priceDate-button").text().substring(3);
                        $("#customField-input-sparePartsPriceList").val(listinoRicambi).change();
                        $('a[href*=tab-contractOpening]').trigger("click");
                        $("#tab-contractOpening").css("position", "relative");

                        var interval5 = setInterval(function() {
                            if ($('#tab-contractOpening').is(":visible")) {
                                setTimeout(function() {
                                    $("#customField-input-ColoreVeicolo").change();
                                    $("#customField-input-paintKind").change();
                                    $("#customField-input-srcImage").change();
                                    clearInterval(interval5);
                                }, 500);
                            }
                        }, 600);

                        if ($("#customField-input-vtype").val() != "") {
                            $(".tools").css("visibility","visible");
                        }
                        $(".loadingDialog").hide();
                        $(".infoVehicle2").show();
                        if ($(".qaStatusWrapper a:contains(N.A.)").length) {
                            $(".infoVehicle3").show();
                        }
                        $("#activityRelatedSelection").css("visibility", "hidden");
                    } else {
                        $("#activityRelatedSelection").show();
                        $('#tab-activityRelatedSelection').css({
                            "visibility": "visible",
                            "left": "0px"
                        });
                        $("#manodoperaPopup").addClass("overlayVisibleDialog");
                        $("#manodoperaPopup").removeClass("overlayHiddenDialogVehSel");
                        $("#activityRelatedSelection").css("visibility", "visible");
                    }
                    clearInterval(autoRed2);
                }
                if ($("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find(".ManufacturerLacquerFactor_wageModePerHour").is(":visible") || $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_type-button").is(":visible")) {
                    if($applicoRiparatore==1){
					applicaManodoperaRiparatore();
					}
					if ($("#activityRelatedSelection > div > iframe").contents().find(".ui-dialog").contents().find(".btn_ok").is(":visible")) {
                        $("#activityRelatedSelection > div > iframe").contents().find(".ui-dialog").contents().find(".btn_ok").click();
                    }
                    var verniciaturaDAT = $("iframe").contents().find("#ManufacturerLacquerFactor_type-button").text();
                    var colore = $("iframe.sphinxPage").contents().find("input[id*=colorName]").val();
                    var lacquerTypeVal;
                    if (verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("doppio") || verniciaturaDAT.toLowerCase().contains("past.doppio")) {
                        lacquerTypeVal = "pastel/doppio";
                    } else {
                        if (verniciaturaDAT.toLowerCase().contains("metallizato") && verniciaturaDAT.toLowerCase().contains("triplo") || verniciaturaDAT.toLowerCase().contains("met.triplo")) {
                            lacquerTypeVal = "met/triplo";
                        } else {
                            if (verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("triplo") || verniciaturaDAT.toLowerCase().contains("past.triplo")) {
                                lacquerTypeVal = "pastel/triplo";
                            } else {
                                if (verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("mono") || verniciaturaDAT.toLowerCase().contains("pastello") && verniciaturaDAT.toLowerCase().contains("1-strato")) {
                                    lacquerTypeVal = "pastel/mono";
                                } else {
                                    if (verniciaturaDAT.toLowerCase().contains("metallizzato") && verniciaturaDAT.toLowerCase().contains("mono")) {
                                        lacquerTypeVal = "met/mono";
                                    } else {
                                        if (verniciaturaDAT.toLowerCase().contains("perlato") && verniciaturaDAT.toLowerCase().contains("mono")) {
                                            lacquerTypeVal = "perl/mono";
                                        } else {
                                            if (verniciaturaDAT.toLowerCase().contains("metallizzato") && verniciaturaDAT.toLowerCase().contains("doppio") || verniciaturaDAT.toLowerCase().contains("met.doppio")) {
                                                lacquerTypeVal = "met/doppio";
                                            } else {
                                                if (verniciaturaDAT.toLowerCase().contains("perlato") && verniciaturaDAT.toLowerCase().contains("doppio") || verniciaturaDAT.toLowerCase().contains("per.doppio")) {
                                                    lacquerTypeVal = "perl/doppio";
                                                } else {
                                                    if (verniciaturaDAT.toLowerCase().contains("perlato") && verniciaturaDAT.toLowerCase().contains("triplo") || verniciaturaDAT.toLowerCase().contains("per.triplo")) {
                                                        lacquerTypeVal = "perl/triplo";
                                                    } else {
                                                        if (verniciaturaDAT.toLowerCase().contains("speciale")) {
                                                            lacquerTypeVal = "vern/specia";
                                                        } else {
                                                            if (verniciaturaDAT.toLowerCase().contains("perl") && verniciaturaDAT.toLowerCase().contains("antig")) {
                                                                lacquerTypeVal = "perl/antigr";
                                                            } else {
                                                                if (verniciaturaDAT.toLowerCase().contains("met") && verniciaturaDAT.toLowerCase().contains("traspa")) {
                                                                    lacquerTypeVal = "met/traspa";
                                                                } else {
                                                                    if (verniciaturaDAT.toLowerCase().contains("per") && verniciaturaDAT.toLowerCase().contains("traspa")) {
                                                                        lacquerTypeVal = "perl/traspa";
                                                                    } else {
                                                                        if (verniciaturaDAT.toLowerCase().contains("past") && verniciaturaDAT.toLowerCase().contains("traspa")) {
                                                                            lacquerTypeVal = "pastel/trasp";
                                                                        } else {
                                                                            lacquerTypeVal = verniciaturaDAT.toLowerCase().substring(0, 13);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $("#customField-input-ColoreVeicolo").val(colore).change();
                    $("#customField-input-paintKind").val(lacquerTypeVal).change();
                    var lacquerType = $("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_type").val();
                    var materialFlat = $("iframe.sphinxPage").contents().find("#ManufacturerLacquerFactor_materialFlatRatePercent[class*='mandatory']").length;
                    var checker1 = 0;
                    if (lacquerType != "") {
                        $("iframe.sphinxPage").contents().find(".ManufacturerLacquerFactor_type").hide();
                    }
                    if (materialFlat == 0) {
                        $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_repairWageLacquerMaterialFlatPercRate").hide();
                        checker1 = 1;
                    } else {
                        if (!$("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li > span > input#ManufacturerLacquerFactor_materialFlatRatePercent").val() == "") {
                            $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_repairWageLacquerMaterialFlatPercRate").hide();
                            checker1 = 1;
                        } else {
                            $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_repairWageLacquerMaterialFlatPercRate").hide();
                            $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_materialFlatRatePercent").val("4");
                            checker1 = 1;
                        }
                    }
                    $("iframe.sphinxPage").contents().find("#GarageRulesFactorChapter").hide();
                    $("iframe.sphinxPage").contents().find("#CalculationFactorChapter").hide();
                    $("iframe.sphinxPage").contents().find("#SparePartFactorChapter").hide();
                    $("iframe.sphinxPage").contents().find("#LabourCostFactorChapter").hide();
                    $("iframe.sphinxPage").contents().find("#DiscountRuleFactorChapter").hide();
                    $("iframe.sphinxPage").contents().find("#DomusCalculationFactorChapter").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterHeader").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("ul.ui-tabs-nav").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#manufacturerLacquerFactor > ul > li.ManufacturerLacquerFactor_colorName").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#manufacturerLacquerFactor > ul > li.ManufacturerLacquerFactor_colorCode").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find(".ManufacturerLacquerFactor_addition").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#manufacturerLacquerFactor > ul > li.ManufacturerLacquerFactor_repairWageLacquerWageTitle").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > h3").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_repairWageLacquerWageTitle").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_wageModePerHour").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_wageModePerHour").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_wageModeLumpSum").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_discountWage").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_discountWageBiw").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_repairWageLacquerMaterialTitle").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_repairWageLacquerMaterialFlatAmount").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_discountMaterial").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_discountMaterialBiw").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor > ul > li.ManufacturerLacquerFactor_preparationTimePercent").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor > ul > li.ManufacturerLacquerFactor_isSurchargeForSecondColor").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor > ul > li.ManufacturerLacquerFactor_isSurchargeForMatt").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul").css("border", "none");
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_repairWageLacquerMaterialByManufacturer").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#ManufacturerLacquerFactor_calculationType > ul > li.ManufacturerLacquerFactor_materialManufSt2Price").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#manufacturerLacquerFactor > ul > li.ManufacturerLacquerFactor_isLacquerAssemblyWhenRemoved").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("li.ManufacturerLacquerFactor_preparationTimePercent").hide();
                    $("iframe.sphinxPage").contents().find("#LacquerCostFactorChapter > div.chapterBody").contents().find("#manufacturerLacquerFactor > ul > br").hide();
                    if (checker1 == 1 && lacquerType != "") {
                        if ($("#activityRelatedSelection > div > iframe").contents().find("#GarageRulesFactor_selectedGarageRule > option:selected").val() == "") {
                            var manodoperaSelected = $("iframe").contents().find("#GarageRulesFactor_baseGarageRule").val();
                            $("#customField-input-LabourChosen").val(manodoperaSelected).change();
                        } else {
                            var manodoperaSelected = $("iframe").contents().find("#GarageRulesFactor_selectedGarageRule-button").text();
                            $("#customField-input-LabourChosen").val(manodoperaSelected).change();
                        }
                        $('a[href*=tab-contractOpening]').trigger("click");
                        $("#tab-contractOpening").css("position", "relative");
                        if ($("#customField-input-vtype").val() != "") {
                            $(".tools").css("visibility","visible");
                        }

                        var interval5 = setInterval(function() {
                            if ($('#tab-contractOpening').is(":visible")) {
                                setTimeout(function() {
                                    $("#customField-input-ColoreVeicolo").change();
                                    $("#customField-input-LabourChosen").change();
                                    $("#customField-input-paintKind").change();
                                    $("#customField-input-srcImage").change();
                                    $("#customField-button-LabourSetting").text($("#customField-input-LabourChosen").val());
                                    $("#customField-button-kindOfPaint").text($("#customField-input-paintKind").val());
                                    $("#claim-saveDraft").click();
                                    clearInterval(interval5);
                                }, 500);
                            }
                        }, 600);

                        $(".loadingDialog").hide();
                        $(".infoVehicle2").show();
                        if ($(".qaStatusWrapper a:contains(N.A.)").length) {
                            $(".infoVehicle3").show();
                        }
                        $("#activityRelatedSelection").css("visibility", "hidden");
                    } else {
                        $("#activityRelatedSelection").show();
                        $('#tab-activityRelatedSelection').css({
                            "visibility": "visible",
                            "left": "0px"
                        });
                        $("#manodoperaPopup").addClass("overlayVisibleDialog");
                        $("#manodoperaPopup").removeClass("overlayHiddenDialogVehSel");
                        $("#activityRelatedSelection").css("visibility", "visible");
                    }
                    clearInterval(autoRed2);
                }
            }
        }, 500);
}
$("#customField-button-closeVehicleSel").addClass("closeButton");
$("#customField-button-closeVehicleSel").on("click", function() {
    var vinCheck = $("#txtVin").val().length;
    var targaCheck = $("#txtLicenceNumberIt").val().length;
    if (vinCheck == "17" && targaCheck == "7") {
        $(".loadingDialog").show();
        var marca = $("#cboManufacturer-button").text().substring(4);
        var modello = $("#cboMaintype-button").text().substring(4);
        var versione = $("#cboSubtype-button").text().substring(4);
        var veicolo = marca + " " + modello + " " + versione;
        $("#customField-input-vehicleDescription").val(veicolo).change();
        if ($("#btnLicenceNumberItalyRequestResult").is(":visible")) {
            if ($('tr[data-key="dataUltimaRevisione"] > td').text() != "") {
                var omologazione = $('tr[data-key="codiceOmologazione"] > td').text();
                $("#customField-input-approvalCode").val(omologazione).change();
                var revisione = $('tr[data-key="dataUltimaRevisione"] > td').text();
                $("#customField-input-revision4").val(revisione).change();
                var motore = $('tr[data-key="codiceMotore"] > td').text();
                $("#customField-input-motorCode").val(motore).change();
            }
        }
        /*     $('#tab-activityRelatedSelection').css("visibility","hidden"); */
       manodoperaThrough();
        var immagine2Check=$('#divPictures1 > a > img').eq(1).length;
		if(immagine2Check==1){
        var immagine = $('#divPictures1 > a > img').eq(1).attr("src");
		}else{
			var immagine = $('#divPictures1 > a > img').attr("src");
		}
        if (immagine != null) {
            convertImgToBase64URL(immagine, function(base64Img) {
                document.getElementById('customField-input-VehicleImage').style.background = 'url(' + base64Img + ')';
                $("#customField-input-srcImage").val(base64Img).change();
            });
            document.getElementById('customField-input-VehicleImage').style.background = 'url(' + immagine + ')';
            var imageSrc = $("#customField-input-srcImage").val();
            var image = new Image();
            image.src = imageSrc;
            var maxWidth = 230;
            var width = image.width;
            var height = image.height;
            var ratio = width / maxWidth;
            // get ratio for scaling image
            var finalHeight = height / ratio;
            $("#customField-input-VehicleImage").css("height", finalHeight, "important");
        }
    } else {
        $("<div>Il telaio o la targa sono mancanti o non sono stati inseriti correttamente. Si prega di verificare</div>").dialog({
            title: "Campi Mancanti da Inserire",
            resizable: false,
            modal: true,
            buttons: {
                'Ok': function() {
                    $(this).dialog('close');
                }
            }
        });
    }
});
$("#customField-input-appointmentDate").on("change", function() {
    $("#customField-input-appointmentDate2").val($("#customField-input-appointmentDate").val()).change();
});
$("#customField-input-vehicleSelData").on("click", function() {
    $("#vehicleSelPopup").addClass("overlayVisibleDialog");
    $("#vehicleSelPopup").removeClass("overlayHiddenDialogVehSel");
});
$("#customField-button-Equipments").on("click", function() {
    $('a[href*=tab-allestimenti]').trigger("click");
});

$("#customField-button-VehicleSelSearch").on("click", function() {
    var veicolo2 = $("#customField-input-vehicleDescription").val();
    $("#customField-input-vehicleDescription2").val(veicolo2);
    $('a[href*=tab-vehicleSelection]').trigger("click");
    var Autored = setInterval(function() {
        if ($('#tab-vehicleSelection').is(':visible')) {
            $(".loadingDialog").show();
			
			var intervalVehicleSelection = setInterval(function(){
				if ($("#txtLicenceNumberIt").is(":visible"))
				{
					clearInterval(intervalVehicleSelection);

                    if ($("#btnVINRequestResult").is(":visible"))
                    {
                        $('#modelSelection input, #modelSelection select, #modelSelection span, #modelSelection input[type="checkbox"], #modelSelection input[type="radio"], #modelSelection button').css({"pointer-events": "none","opacity": "0.7"});
                        $('#dpRegistrationDate, #colRegistrationDate .btnCalendar').css({"pointer-events": "unset","opacity": "1"});
                    }
				}
			}, 500);
			
            $('#tab-contractOpening').css("display", "block");
            $('#tab-contractOpening').css("position", "absolute");
			clearInterval(Autored);
        } else {
            $('#tab-contractOpening').css("display", "block");
            $('#tab-contractOpening').css("position", "absolute");
        }

    }, 50);
});
/*DRIVER*/
$("#customField-button-closeDriver").addClass("closeButton");
$("#customField-button-closeDriver").on("click", function() {
    $("#driverPopup").addClass("overlayHiddenDialog");
    $("#driverPopup").removeClass("overlayVisibleDialog");
    $(".loadingDialog").hide();
});
$("#customField-control-driverData").on("click", function() {
    $("#driverPopup").addClass("overlayVisibleDialog");
    $("#driverPopup").removeClass("overlayHiddenDialog");
    $(".loadingDialog").show();
});
$("#driverPopup").contents().find("#addressBook-clear").on("click", function() {
    $("#customField-input-driverData").val("");
});
/*OPPONENT*/
$("#customField-button-closeOpponent").addClass("closeButton");
$("#customField-button-closeOpponent").on("click", function() {
    $("#contropartePopup").addClass("overlayHiddenDialog");
    $("#contropartePopup").removeClass("overlayVisibleDialog");
    $(".loadingDialog").hide();
});
$("#customField-button-OpponentButton").on("click", function() {
    $("#contropartePopup").addClass("overlayVisibleDialog");
    $("#contropartePopup").removeClass("overlayHiddenDialog");
    $(".loadingDialog").show();
});
$("#contropartePopup").contents().find("#addressBook-clear").on("click", function() {
    $("#customField-input-opponentData").val("");
});
/*INSURANCE COMPANY*/
$("#customField-button-closeInsurer").addClass("closeButton");
$("#customField-button-closeInsurer").on("click", function() {
    $("#insurerPopup").addClass("overlayHiddenDialog");
    $("#insurerPopup").removeClass("overlayVisibleDialog");
    $(".loadingDialog").hide();
});
$("#customField-control-insurerData").on("click", function() {
    $("#insurerPopup").addClass("overlayVisibleDialog");
    $("#insurerPopup").removeClass("overlayHiddenDialog");
    $(".loadingDialog").show();
});
$("#insurerPopup").contents().find("#addressBook-clear").on("click", function() {
    $("#customField-input-insurerData").val("");
});
/*OPPONENT INSURANCE COMPANY*/
$("#customField-button-closeOpponentCompany").addClass("closeButton");
$("#customField-button-closeOpponentCompany").on("click", function() {
    $("#opponentCompanyPopup").addClass("overlayHiddenDialog");
    $("#opponentCompanyPopup").removeClass("overlayVisibleDialog");
    $(".loadingDialog").hide();
});
$("#customField-control-opponentCompanyData").on("click", function() {
    $("#opponentCompanyPopup").addClass("overlayVisibleDialog");
    $("#opponentCompanyPopup").removeClass("overlayHiddenDialog");
    $(".loadingDialog").show();
});
$("#opponentCompanyPopup").contents().find("#addressBook-clear").on("click", function() {
    $("#customField-input-opponentCompanyData").val("");
});
/*PLACE OF INCIDENT */
$("#customField-button-closeIncident").addClass("closeButton");
$("#customField-button-closeIncident").on("click", function() {
    $("#incidentPopup").addClass("overlayHiddenDialog");
    $("#incidentPopup").removeClass("overlayVisibleDialog");
    $(".loadingDialog").hide();
});
$("#customField-button-incidentButton").on("click", function() {
    $("#incidentPopup").addClass("overlayVisibleDialog");
    $("#incidentPopup").removeClass("overlayHiddenDialog");
    $(".loadingDialog").show();
});
$("#incidentPopup").contents().find("#addressBook-clear").on("click", function() {
    $("#customField-input-incidentData").val("");
});
$("#customField-input-direction_front_right").on("click", function() {
    if ($("#customField-input-direction_front_right").is(":checked")) {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation + "A3";
        $("#customField-input-damageLocation").val(damageLocation);
    } else {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation.replace('A3', '');
        $("#customField-input-damageLocation").val(damageLocation);
    }
});
$("#customField-input-direction_front_middle").on("click", function() {
    if ($("#customField-input-direction_front_middle").is(":checked")) {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation + "A1";
        $("#customField-input-damageLocation").val(damageLocation);
    } else {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation.replace('A1', '');
        $("#customField-input-damageLocation").val(damageLocation);
    }
});
$("#customField-input-direction_front_left").on("click", function() {
    if ($("#customField-input-direction_front_left").is(":checked")) {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation + "A2";
        $("#customField-input-damageLocation").val(damageLocation);
    } else {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation.replace('A2', '');
        $("#customField-input-damageLocation").val(damageLocation);
    }
});
$("#customField-input-direction_roof_cofanoant").on("click", function() {
    if ($("#customField-input-direction_roof_cofanoant").is(":checked")) {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation + "D";
        $("#customField-input-damageLocation").val(damageLocation);
    } else {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation.replace('D', '');
        $("#customField-input-damageLocation").val(damageLocation);
    }
});
$("#customField-input-direction_sideant_left").on("click", function() {
    if ($("#customField-input-direction_sideant_left").is(":checked")) {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation + "B2";
        $("#customField-input-damageLocation").val(damageLocation);
    } else {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation.replace('B2', '');
        $("#customField-input-damageLocation").val(damageLocation);
    }
});
$("#customField-input-direction_sideant_right").on("click", function() {
    if ($("#customField-input-direction_sideant_right").is(":checked")) {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation + "B1";
        $("#customField-input-damageLocation").val(damageLocation);
    } else {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation.replace('B1', '');
        $("#customField-input-damageLocation").val(damageLocation);
    }
});
$("#customField-input-direction_roof_floor").on("click", function() {
    if ($("#customField-input-direction_roof_floor").is(":checked")) {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation + "F";
        $("#customField-input-damageLocation").val(damageLocation);
    } else {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation.replace('F', '');
        $("#customField-input-damageLocation").val(damageLocation);
    }
});
$("#customField-input-direction_sidepost_right").on("click", function() {
    if ($("#customField-input-direction_sidepost_right").is(":checked")) {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation + "B3";
        $("#customField-input-damageLocation").val(damageLocation);
    } else {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation.replace('B3', '');
        $("#customField-input-damageLocation").val(damageLocation);
    }
});
$("#customField-input-direction_sidepost_left").on("click", function() {
    if ($("#customField-input-direction_sidepost_left").is(":checked")) {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation + "B4";
        $("#customField-input-damageLocation").val(damageLocation);
    } else {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation.replace('B4', '');
        $("#customField-input-damageLocation").val(damageLocation);
    }
});
$("#customField-input-direction_rear_cofanopost").on("click", function() {
    if ($("#customField-input-direction_rear_cofanopost").is(":checked")) {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation + "E";
        $("#customField-input-damageLocation").val(damageLocation);
    } else {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation.replace('E', '');
        $("#customField-input-damageLocation").val(damageLocation);
    }
});
$("#customField-input-direction_rear_left").on("click", function() {
    if ($("#customField-input-direction_rear_left").is(":checked")) {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation + "C2";
        $("#customField-input-damageLocation").val(damageLocation);
    } else {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation.replace('C2', '');
        $("#customField-input-damageLocation").val(damageLocation);
    }
});
$("#customField-input-direction_rear_right").on("click", function() {
    if ($("#customField-input-direction_rear_right").is(":checked")) {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation + "C3";
        $("#customField-input-damageLocation").val(damageLocation);
    } else {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation.replace('C3', '');
        $("#customField-input-damageLocation").val(damageLocation);
    }
});
$("#customField-input-direction_rear_middle").on("click", function() {
    if ($("#customField-input-direction_rear_middle").is(":checked")) {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation + "C1";
        $("#customField-input-damageLocation").val(damageLocation);
    } else {
        var damageLocation = $("#customField-input-damageLocation").val();
        damageLocation = damageLocation.replace('C1', '');
        $("#customField-input-damageLocation").val(damageLocation);
    }
});
$("#customField-input-GTA").on("change", function() {
    var id = $("#customField-input-GTA").val();
    var lengthCompanyID = $("#customField-input-GTA").val().length;
    if (lengthCompanyID == 3) {
        id = "0" + id;
    }
    if (lengthCompanyID == 2) {
        id = "00" + id;
    }
    $("#customField-input-GTA").val(id);
});
$("#customField-control-addressTaxCodeInsurant").addClass("hiddenAbsolute");

function floatToString(input)
{
	return input.replace(".", "").replace(",", ".").toString();
}

function copyVal(from, to)
{
    var valore = floatToString($(from).val());
    $(to).val(valore).change();
}

function copyVals()
{
    copyVal("#customField-input-float_replacementValue", "#customField-input-replacementValue");
    copyVal("#customField-input-float_insuredValue", "#customField-input-InsuredValue");
    copyVal("#customField-input-float_deductionParts", "customField-input-deductionParts");
    copyVal("#customField-input-float_insuranceFailure", "#customField-input-insuranceFailure");
}

$("#customField-input-float_replacementValue, #customField-input-float_insuredValue, " +
    "#customField-input-float_deductionParts, #customField-input-float_insuranceFailure").on("change", function() {
    copyVals();
});

/*
$("#customField-input-float_replacementValue").on("change", function() {
	var valore = floatToString($(this).val());
	$("#customField-input-replacementValue").val(valore).change();
});

$("#customField-input-float_insuredValue").on("change", function() {
	var valore = floatToString($(this).val());
	$("#customField-input-InsuredValue").val(valore).change();
});

$("#customField-input-float_deductionParts").on("change", function() {
	var valore = floatToString($(this).val());
	$("#customField-input-deductionParts").val(valore).change();
});

$("#customField-input-float_insuranceFailure").on("change", function() {
	var valore = floatToString($(this).val());
	$("#customField-input-insuranceFailure").val(valore).change();
});
*/

/*
$("#customField-input-float_FinalTotal").on("change", function() {
	//var valore = floatToString($(this).val());
	$("#customField-input-FinalTotal3").val($(this).val()).change();
});*/

function hideManufacturerCalculation()
{
    setTimeout(function(){
        var Calculationchecker1 = $("#claim-calculationHistory-tableLister-table").contents().find("tr[class*='active']").children().find(".icon-calculationType-VRO_DOMUS").length;
        var Calculationchecker2 = $("#claim-calculationHistory-tableLister-table").contents().find("tr[class*='active']").children().find(".icon-calculationType-VRO_API").length;
        if (!user.role.match(/REPAIRER/) && !user.role.match(/EXPERT/)) {
            if (Calculationchecker1 == "1") {
                $("#claim-calculationHistory-tableLister-table").contents().find(".icon-calculationType-VRO_API").parent().parent().hide();
                $("#claim-calculationHistory-tableLister-table").contents().find(".icon-calculationType-VRO_DOMUS").parent().parent().show();
            }
            if (Calculationchecker2 == "1") {
                $("#claim-calculationHistory-tableLister-table").contents().find(".icon-calculationType-VRO_API").parent().parent().show();
                $("#claim-calculationHistory-tableLister-table").contents().find(".icon-calculationType-VRO_DOMUS").parent().parent().hide();
            }
        } else {
            $("#claim-calculationHistory-tableLister-table").contents().find(".icon-calculationType-VRO_API").parent().parent().hide();
            $("#claim-calculationHistory-tableLister-table").contents().find(".icon-calculationType-VRO_DOMUS").parent().parent().show();
        }
    },1500);
}

function contaFoto()
{
    var contaJPG2 = $(".filename:contains(.JPG)").length;
    var contaJPG = $(".filename:contains(.jpg)").length;
    var contaPNG2 = $(".filename:contains(.PNG)").length;
    var contaPNG = $(".filename:contains(.png)").length;
    var contaJPEG2 = $(".filename:contains(.JPEG)").length;
    var contaJPEG = $(".filename:contains(.jpeg)").length;
    var contaBMP = $(".filename:contains(.bmp)").length;
    var contaBMP2 = $(".filename:contains(.BMP)").length;
    var sommaFoto = parseInt(contaJPG) + parseInt(contaPNG) + parseInt(contaJPEG) + parseInt(contaBMP) + parseInt(contaJPG2) + parseInt(contaPNG2) + parseInt(contaJPEG2) + parseInt(contaBMP2);
    $("#customField-input-attachmentNumber").val(sommaFoto).change();
}

function checkMissingConditions()
{
    if ($("#customField-input-VATDeductionDialog").val() == ""){
        $("#customField-control-MissingConditions2").css("display", "inline-block");
    }else{
        $("#customField-control-MissingConditions").css("display", "none");
    }
    if ($("#customField-input-attachmentNumber").val() < 3) {
        $("#customField-control-MissingConditions").css("display", "inline-block");
        $("#customField-button-MissingConditions").text("ALLEGARE ALMENO 3 FOTO");
    } else {
        $("#customField-control-MissingConditions").css("display", "none");
    }
}

hideManufacturerCalculation();

$('body').on('inbox_historyChanged', function(e){
    setTimeout(function(){
        hideManufacturerCalculation();
        contaFoto();
        checkMissingConditions();
    }, 1000);
});

function generateJS(fieldName, rows, rowNr, fieldNr, fullRow=false)
{
    var fields = rows[rowNr].split("=");

    if (fullRow == false) {
        fields = fields[1].split("~");
        var value = fields[fieldNr];
    } else
    {
        var value = fields[1];
    }

    $("#customField-input-"+fieldName).val(value).change();

    return "$('#customField-input-"+fieldName+"').val('"+value+"').change();\n";
}

setTimeout(function(){
    if ($("#customField-input-insuranceType").val()!=null) return;

    var output = "";
    var rows = $("#customField-input-importAXA2").val().split("\n")

    output += generateJS("CompanyID", rows, 2, 0);
    output += generateJS("CompanyName", rows, 2, 1);
    output += generateJS("idMessage", rows, 4, 0);
    output += generateJS("MailConfirm", rows, 5, 0);
    output += generateJS("assignmentKind", rows, 8, 0, true);
    output += generateJS("createOrderDate", rows, 9, 0);
    output += generateJS("DamageNumber", rows, 10, 0);
    output += generateJS("assignmentID", rows, 10, 0);
    output += generateJS("insuranceType", rows, 11, 0, true);
    output += generateJS("exercise", rows, 12, 0);
    output += generateJS("CityIncident", rows, 14, 0);
    output += generateJS("zipCodeIncident", rows, 14, 1);
    output += generateJS("provinceIncident", rows, 14, 2);
    output += generateJS("DamageDate", rows, 15, 0);
    output += generateJS("PolicyNumber", rows, 17, 0);
    output += generateJS("idAgencia", rows, 18, 0);
    output += generateJS("AgenziaName", rows, 18, 1);
    output += generateJS("PolicyBranch2", rows, 19, 0);
    output += generateJS("RegularityContractual", rows, 20, 0);
    output += generateJS("InsuredValue", rows, 21, 0);
    output += generateJS("PolicyExcess", rows, 22, 0);
    output += generateJS("PercentageUncovered", rows, 23, 0);
    output += generateJS("addressFirstNameInsurant", rows, 24, 0);
    output += generateJS("addressSurnameInsurant", rows, 24, 1);
    output += generateJS("addressTaxCodeInsurant", rows, 25, 0);
    output += generateJS("address_streetInsurant", rows, 26, 0);
    output += generateJS("address_cityInsurant", rows, 26, 1);
    output += generateJS("address_zipInsurant", rows, 26, 2);
    output += generateJS("addressInsurantPV", rows, 26, 3);
    output += generateJS("addressPrivatePhoneInsurant", rows, 27, 0);
    output += generateJS("vehicleDescription", rows, 28, 0);
    output += generateJS("LicenseNumber", rows, 29, 0);
    output += generateJS("addressSurnameHolder", rows, 37, 0);
    output += generateJS("addressFirstNameHolder", rows, 37, 1);
    output += generateJS("address_streetHolder", rows, 38, 0);
    output += generateJS("address_cityHolder", rows, 38, 1);
    output += generateJS("address_zipHolder", rows, 38, 2);
    output += generateJS("Opponent_Pr", rows, 38, 3);
    output += generateJS("addressPrivatePhoneHolder", rows, 39, 0);
    output += generateJS("Opponent_vehicleDescription", rows, 40, 0);
    output += generateJS("Opponent_LicenseNumber", rows, 41, 0);
    output += generateJS("ExpCompanyID", rows, 52, 0);
    output += generateJS("ExpCompanyName", rows, 52, 1);
    output += generateJS("LicenseNumber", rows, 53, 0);
    output += generateJS("remarks", rows, 54, 0);
    output += generateJS("AspisBodyshopId", rows, 56, 0);
    output += generateJS("expertName", rows, 56, 1);

    $("#claim-saveDraft").click();

    console.log(output);
},1000);

$(document).ready(function(){
    var template = $( ".template" );
    var form = template.template("form");
});