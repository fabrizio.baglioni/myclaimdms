<?php
require_once '../engine/SubjectCollection/SubjectCollection.php';
require_once '../engine/Subject/Subject.php';

use Subject\Insurance;
use Subject\Owner;
use SubjectCollection\SubjectCollection;

$obj = new stdClass();

/*
$def1 = new stdClass();
$def1->name = 'firstName';
$def1->type = 'string';
$def1->description = 'First Name';
$def1->tooltip = 'Enter first name here';

$def2 = new stdClass();
$def2->fieldName = 'lastName';
$def2->type = 'string';

$def3 = new stdClass();
$def3->fieldName = 'code';
$def3->type = 'string';

$def4 = new stdClass();
$def4->fieldName = 'description';
$def4->type = 'string';

$obj = [
    'owners' => [$def1, $def2, $def3],
    'insurances' => [$def3, $def4]
];

echo '<pre>';
echo json_encode($obj, JSON_PRETTY_PRINT);
echo '</pre>';
die;
*/

$owner = new Owner(12);
$owner->firstName = 'Fabrizio';
$owner->lastName  = 'Baglioni';

$owner2 = new Owner(13);
$owner2->firstName = 'Mario';
$owner2->lastName  = 'Rossi';
$owner2->setType('pippo');

$insurance = new Insurance(15);
$insurance->code = '001';
$insurance->name = 'Generali';

$insurance2 = new Insurance(16);
$insurance2->code = '002';
$insurance2->name = 'AXA';

$insurance3 = new Insurance(17);
$insurance3->code = '003';
$insurance3->name = 'CATTOLICA';

$obj->subjects = [
    'insurances' => [$insurance, $insurance2, $insurance3],
    'owners' => [$owner, $owner2]
];

echo '<pre>';
echo json_encode($obj, JSON_PRETTY_PRINT);

echo '</pre>';
die;

SubjectCollection::joinModeLoose();
$sc = new SubjectCollection('owner');
$sc2 = new SubjectCollection('insurance');


$owner2 = new Owner(13);
$owner2->firstName = 'Mario';
$owner2->lastName  = 'Rossi';
$owner2->setType('pippo');

$insurance = new Insurance(15);
$insurance->code = '001';
$insurance->name = 'Generali';

$insurance2 = new Insurance(16);
$insurance2->code = '002';
$insurance2->name = 'AXA';

$insurance3 = new Insurance(17);
$insurance3->code = '003';
$insurance3->name = 'CATTOLICA';

$sc->addSubject($owner);
$sc->addSubject($owner2);

$sc2->addSubject($insurance);
$sc2->addSubject($insurance2);
$sc2->addSubject($insurance3);

echo json_encode($sc);
echo '<br>';
echo json_encode($sc2);
echo '<br>';
echo json_encode($owner);

echo '<br>';
echo '<br>';
$s = $sc2->getSubjectById(17);

//$s->decodeObject()


