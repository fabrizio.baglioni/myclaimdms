<?php

require_once '../engine/SubjectCollection/SubjectCollection.php';
require_once '../engine/Subject/Subject.php';

use Subject\Insurance;
use Subject\Owner;
use SubjectCollection\SubjectCollection;

$j = '{"type":"owner","subjects":{"2":{"firstName":"Maremmo","lastName":"Infame"},"3":{"firstName":"Ciro","lastName":"Ignobile"}}}';
$j = '[
    {"type":"owner","subjects":{"2":{"firstName":"Maremmo","lastName":"Infame"},"3":{"firstName":"Ciro","lastName":"Ignobile"}}},
    {"type":"insurance","subjects":{"2":{"firstName":"Maremmo","lastName":"Infame"},"3":{"skskfirstName":"Ciro","lastName":"Ignobile"}}}
    ]';

echo '<pre>';
var_dump(json_decode($j));
echo '</pre>';
die;

$j = file_get_contents('owners.json');
$sc = \SubjectCollection\SubjectCollectionFactory::buildFromJson($j);

$sub = $sc->getSubjectById(13);
echo '<pre>';
echo 'prima<br>';
var_dump($sc,$sub);
echo '</pre>';

$jsonModified = "{\"id\":\"12\",\"firstName\":\"Paolo\",\"lastName\":\"Bianchi\"}";

//$newSubj = \Subject\SubjectFactory::buildFromJsonString()

$jsonObj = json_decode($jsonModified);

echo '<pre>';
var_dump($jsonObj);
echo '</pre>';

$sub->decodeObject($jsonObj);

echo '<pre>';
var_dump($sc, $sub);
echo '</pre>';
/*
foreach ($sc as $id=>$sub)
    var_dump($id, $sub);

*/
echo '<hr>';

echo '<pre>';
var_dump($j);
echo '<hr>';
var_dump(json_encode($sc->jsonSerialize()));
echo '</pre>';

die;