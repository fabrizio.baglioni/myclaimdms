var bound = false;

$(document).ready(function() {
	$(".anagraficaGroup .core-row").on("click", function() {
		var kindAnagraphic = $(this).attr("class").split(' ')[1];
		var idRow = "";
		if (!$("#customField-input-" + kindAnagraphic + "_id").val() == "") {
			idRow = $("#customField-input-" + kindAnagraphic + "_id").val();
		}
		$(".anagrafica .foreignIFrame >iframe").contents().find("#variableSubject").text($(this).find("div[id*='anaText']").text());
		if ($(this).hasClass("noAddressBook")) {
			$(".anagrafica .foreignIFrame >iframe").contents().find("#variableTitle").text("Dati ");
			withoutAddressBook();
		} else {
			withAddressBook();
			if (kindAnagraphic.contains("Note")) {
				$(".anagrafica .foreignIFrame >iframe").contents().find("#searchInput").attr("placeholder", "Cerca");
				$(".anagrafica .foreignIFrame >iframe").contents().find("a[href='#saveSubject'] span").text("Aggiungi a Testi predefiniti")
				$(".anagrafica .foreignIFrame >iframe").contents().find("#variableTitle").text("Blocco ");
				$(".anagrafica .foreignIFrame >iframe").contents().find("#variableSubject").text($("#customField-control-notesBlock_block label").text());
			} else {
				$(".anagrafica .foreignIFrame >iframe").contents().find("#variableTitle").text("Anagrafica ");
				$(".anagrafica .foreignIFrame >iframe").contents().find("#searchInput").attr("placeholder", "Cerca per nome, città, pv, cap");
				$(".anagrafica .foreignIFrame >iframe").contents().find("a[href='#saveSubject'] span").text("Aggiungi ad Anagrafica");
			}
		}
		if(kindAnagraphic=="opponent_insurance"){
			$(".anagrafica .foreignIFrame >iframe").contents().find("#REQ_CALLERPREFIX").val("opponent_");
			kindAnagraphic="insurance";
		}else{
			$(".anagrafica .foreignIFrame >iframe").contents().find("#REQ_CALLERPREFIX").val("");
		}
		dialogAnagrafica(kindAnagraphic, idRow);
	});

	$("#customField-input-repairer_labourRatesBody").on("change", function() {
		$applicoRiparatore = 1;
		$("#customField-input-manodoperaRiparatoreApplicata").val("0").change();
		if (!$("#customField-input-vehicle_datEuropaCode").val() == "") {
			manodoperaThrough();
		}
	});

	var $temporaryInfo = 0;

	$("#customField-button-notesBlock_open").on("click", function() {
		var notesBlockId = "";
		if (!$("#customField-input-notesBlock_id").val() == "") {
			notesBlockId = $("#customField-input-notesBlock_id").val();
		}
		withAddressBook();
		dialogAnagrafica("notesBlock", notesBlockId);
		$(".anagrafica .foreignIFrame >iframe").contents().find("#searchInput").attr("placeholder", "Cerca");
		$(".anagrafica .foreignIFrame >iframe").contents().find("a[href='#saveSubject'] span").text("Aggiungi a Testi predefiniti")
		$(".anagrafica .foreignIFrame >iframe").contents().find("#variableTitle").text("Blocco ");
		$(".anagrafica .foreignIFrame >iframe").contents().find("#variableSubject").text($("#customField-control-notesBlock_block label").text());
	});

	function updateReverse() {
		var prefix=$(".anagrafica .foreignIFrame >iframe").contents().find("#REQ_CALLERPREFIX").val();
		var AnagraficaInputLength = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm input").length;
		for (i = 0; i < AnagraficaInputLength; i++) {
			var anagraficaAttr = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm input").eq(i).attr("id");
			$(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm input").eq(i).val($("#customField-input-" +prefix+ anagraficaAttr).val());
		}
		var AnagraficaTextAreaLength = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm textarea").length;
		for (t = 0; t < AnagraficaTextAreaLength; t++) {
			var anagraficaAttr2 = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm textarea").eq(t).attr("id");
			$(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm textarea").eq(t).val($("#customField-input-" +prefix+ anagraficaAttr2).val());
			$("#claim-saveDraft").click();
		}
		var AnagraficaSelectLength = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm select").length;
		for (z = 0; z < AnagraficaSelectLength; z++) {
			var anagraficaAttr3 = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm select").eq(z).attr("id");
			$(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm select").eq(z).val($("#customField-input-" +prefix+ anagraficaAttr3).val());
			$("#claim-saveDraft").click();
		}
		$(".anagrafica .foreignIFrame >iframe").contents().find("#messageAnagrafica").show();
	}

	function updateInput() {
	    var prefix=$(".anagrafica .foreignIFrame >iframe").contents().find("#REQ_CALLERPREFIX").val();
		var AnagraficaInputLength = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm input").length;
		for (i = 0; i < AnagraficaInputLength; i++) {
			var anagraficaAttr = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm input").eq(i).attr("id");
			var anagraficaInput = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm input").eq(i).val();
			$("#customField-input-" +prefix+ anagraficaAttr).val(anagraficaInput).change();
		}
		var id = $(".anagrafica .foreignIFrame >iframe").contents().find("form").eq(2).attr("id");
		if ($("#customField-input-" +prefix+ id + "_name").val() != "") {
			$("#customField-input-" +prefix+ id + "_name").parent().parent().parent().removeClass("aGEmpty");
		}else{
			$("#customField-input-" +prefix+ id + "_name").parent().parent().parent().addClass("aGEmpty");
		}
		var AnagraficaTextAreaLength = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm textarea").length;
		for (t = 0; t < AnagraficaTextAreaLength; t++) {
			var anagraficaAttr2 = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm textarea").eq(t).attr("id");
			var anagraficaInput2 = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm textarea").eq(t).val();
			$("#customField-input-"+ prefix + anagraficaAttr2).val(anagraficaInput2).change();
		}
		var AnagraficaSelectLength = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm select").length;
		for (z = 0; z < AnagraficaSelectLength; z++) {
			var anagraficaAttr3 = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm select").eq(z).attr("id");
			var anagraficaInput3 = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm select").eq(z).val();
			$("#customField-input-"+ prefix + anagraficaAttr3).val(anagraficaInput3).change();
		}
		$("#claim-saveDraft").click();
	}

	function itemsOnClick() {
		$(".anagrafica .foreignIFrame >iframe").contents().find("#readResultList a").on("click", function() {
			$(".anagrafica .foreignIFrame >iframe").contents().find("#readResultList a").removeClass("selectedA");
			$(this).addClass("selectedA");
			$(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm input").eq(0).val(this.id);
		});
		$(".anagrafica .foreignIFrame >iframe").contents().find("#readResultList a").on("dblclick", function() {
			updateInput();
			$(".anagrafica").addClass("hiddenAbsolute");
			$(".loadingDialog").hide();
		});
	}

	function iframeFunction() {
		$(".loadingDialog").show("slow");
		if (!bound) {
			bound = true;
			$(".anagrafica .foreignIFrame >iframe").contents().find("#readResultList").bind("DOMSubtreeModified", function() {
				$(".anagrafica").removeClass("hiddenAbsolute");
				var prefix=$(".anagrafica .foreignIFrame >iframe").contents().find("#REQ_CALLERPREFIX").val();
				var idKind = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm input").eq(0).attr("id");
				var idSelected = $("#customField-input-"+prefix+idKind).val();
				console.log(idKind);
				if (!idSelected == ""){
					$(".anagrafica .foreignIFrame >iframe").contents().find("#readResultList #" + idSelected).click();
				}
				var nameKind = $(".anagrafica .foreignIFrame >iframe").contents().find("#insertForm input").eq(1).attr("id");
				var nameSelected = $("#customField-input-" + nameKind).val();
				if ($temporaryInfo == 1 ) {
					$temporaryInfo = 0;
					if (!nameSelected == ""){
						updateReverse();
					}
				}
				itemsOnClick();
			});
			$(".anagrafica .foreignIFrame >iframe").contents().find(".btn-success").on("click", function() {
				$(".anagrafica .foreignIFrame >iframe").contents().find("#messageAnagrafica").hide();
			});
			$(".anagrafica .foreignIFrame >iframe").contents().find("a[href='#exitSubject']").on("click", function() {
				updateInput();
				$(".anagrafica").addClass("hiddenAbsolute");
				$(".loadingDialog").hide();
			});
		}
	}

	function dialogAnagrafica(p1, p2) {
		$(".anagrafica .foreignIFrame >iframe").contents().find("#REQ_COLLECTIONNAME_READ").val(p1).change();
		$(".anagrafica .foreignIFrame >iframe").contents().find(".container-fluid").eq(0).hide();
		$(".anagrafica .foreignIFrame >iframe").contents().find("hr").hide();
		$(".anagrafica .foreignIFrame >iframe").contents().find("#readBtn").click();
		if (p2 == "") {
			$temporaryInfo = 1;
		}
		iframeFunction();
	}

	$(".loadingDialog").on("click", function() {
		if (!$(".anagrafica").hasClass("hiddenAbsolute")) {
			$(".anagrafica").addClass("hiddenAbsolute");
			$(".loadingDialog").hide();
		}
	});
	var emptyAnagrafica = $('.anagraficaGroup input[id*="name"]').length;
	for (i = 0; i < emptyAnagrafica; i++) {
		if ($('.anagraficaGroup input[id*="name"]').eq(i).val() == "") {
			$('.anagraficaGroup input[id*="name"]').eq(i).parent().parent().parent().addClass("aGEmpty");
		}
	}
	function withAddressBook() {
		if ($(".anagrafica .foreignIFrame >iframe").contents().find(".order-md-1").children().eq(1).hasClass("col-sm-12")) {
			$(".anagrafica .foreignIFrame >iframe").contents().find(".order-md-1").children().eq(0).show();
			$(".anagrafica .foreignIFrame >iframe").contents().find(".order-md-1").children().eq(1).addClass("col-sm-8").removeClass("col-sm-12");
		}
		$(".anagrafica .foreignIFrame >iframe").contents().find("#messageAnagrafica").parent().show();
	}
	function withoutAddressBook() {
		if ($(".anagrafica .foreignIFrame >iframe").contents().find(".order-md-1").children().eq(1).hasClass("col-sm-8")) {
			$(".anagrafica .foreignIFrame >iframe").contents().find(".order-md-1").children().eq(0).hide();
			$(".anagrafica .foreignIFrame >iframe").contents().find(".order-md-1").children().eq(1).removeClass("col-sm-8").addClass("col-sm-12");
		}
		$(".anagrafica .foreignIFrame >iframe").contents().find("#messageAnagrafica").parent().hide();
	}
});