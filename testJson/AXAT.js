var bound = false;
//$(document).ready(function() {
//$('body').on('template_loaded',function() {
	$("#layout-center").addClass("hiddenAbsolute");
setTimeout(function(){	
     $("#customField-control-bigTitlePolizza").on("click",function(){
		$("#datiAssicurativi").addClass("hiddenAbsolute");
		$("#DatiPolizza").removeClass("hiddenAbsolute");
		$("#customField-control-bigTitlePolizza").css("color","black");
		$("#customField-control-bigTitleAssicurativi").css("color","grey");
	 });
	 $("#customField-control-bigTitleAssicurativi").on("click",function(){
		$("#DatiPolizza").addClass("hiddenAbsolute");
		$("#datiAssicurativi").removeClass("hiddenAbsolute");
		$("#customField-control-bigTitlePolizza").css("color","grey");
		$("#customField-control-bigTitleAssicurativi").css("color","black");
	 });
	$(".anagraficaGroup .core-row").on("click", function() {
		var iFrameContent = $(".anagrafica .foreignIFrame >iframe").contents();
		var kindAnagraphic = $(this).attr("class").split(' ')[1];
		console.log(kindAnagraphic);
		var idRow = "";
		if (!$("#customField-input-" + kindAnagraphic + "_id").val() == "") {
			idRow = $("#customField-input-" + kindAnagraphic + "_id").val();
		}
		iFrameContent.find("#variableSubject").text($(this).find("div[id*='anaText']").text());
		if ($(this).hasClass("noAddressBook")) {
			iFrameContent.find("#variableTitle").text("Dati ");
			withoutAddressBook();
		} else {
			withAddressBook();
			if (kindAnagraphic.contains("Note")) {
				iFrameContent.find("#searchInput").attr("placeholder", "Cerca");
				iFrameContent.find("a[href='#saveSubject'] span").text("Aggiungi a Testi predefiniti")
				iFrameContent.find("#variableTitle").text("Blocco ");
				iFrameContent.find("#variableSubject").text($("#customField-control-notesBlock_block label").text());
			} else {
				iFrameContent.find("#variableTitle").text("Anagrafica ");
				iFrameContent.find("#searchInput").attr("placeholder", "Cerca per nome, città, pv, cap");
				iFrameContent.find("a[href='#saveSubject'] span").text("Aggiungi ad Anagrafica");
			}
		}
		if(kindAnagraphic=="opponent_insurance" ){
			iFrameContent.find("#REQ_CALLERPREFIX").val("opponent_");
			kindAnagraphic="insurance";
		}else{
			if(kindAnagraphic=="opponent_inspectorate" ){
				iFrameContent.find("#REQ_CALLERPREFIX").val("opponent_");
				kindAnagraphic="inspectorate";
				console.log("opp inspectorate");
			}else{
			iFrameContent.find("#REQ_CALLERPREFIX").val("");
			}
		}
		console.log("a: "+kindAnagraphic);
		openDialogAnagrafica(kindAnagraphic, idRow);
	});

	$("#customField-input-repairer_labourRatesBody").on("change", function() {
		$applicoRiparatore = 1;
		$("#customField-input-manodoperaRiparatoreApplicata").val("0").change();
		if (!$("#customField-input-vehicle_datEuropaCode").val() == "") {
			manodoperaThrough();
		}
	});

	var $temporaryInfo = 0;

	$("#customField-button-notesBlock_open").on("click", function() {
		var iFrameContent = $(".anagrafica .foreignIFrame >iframe").contents();
		var notesBlockId = "";
		if (!$("#customField-input-notesBlock_id").val() == "") {
			notesBlockId = $("#customField-input-notesBlock_id").val();
		}
		withAddressBook();
		openDialogAnagrafica("notesBlock", notesBlockId);
		iFrameContent.find("#searchInput").attr("placeholder", "Cerca");
		iFrameContent.find("a[href='#saveSubject'] span").text("Aggiungi a Testi predefiniti")
		iFrameContent.find("#variableTitle").text("Blocco ");
		iFrameContent.find("#variableSubject").text($("#customField-control-notesBlock_block label").text());
	});

	function updateReverse() {
		var iFrameContent = $(".anagrafica .foreignIFrame >iframe").contents();
		var prefix=iFrameContent.find("#REQ_CALLERPREFIX").val();
		var AnagraficaInputLength = iFrameContent.find("#insertForm input").length;
		for (i = 0; i < AnagraficaInputLength; i++) {
			var anagraficaAttr = iFrameContent.find("#insertForm input").eq(i).attr("id");
			iFrameContent.find("#insertForm input").eq(i).val($("#customField-input-" +prefix+ anagraficaAttr).val());
		}
		var AnagraficaTextAreaLength = iFrameContent.find("#insertForm textarea").length;
		for (t = 0; t < AnagraficaTextAreaLength; t++) {
			var anagraficaAttr2 = iFrameContent.find("#insertForm textarea").eq(t).attr("id");
			iFrameContent.find("#insertForm textarea").eq(t).val($("#customField-input-" +prefix+ anagraficaAttr2).val());
			$("#claim-saveDraft").click();
		}
		var AnagraficaSelectLength = iFrameContent.find("#insertForm select").length;
		for (z = 0; z < AnagraficaSelectLength; z++) {
			var anagraficaAttr3 = iFrameContent.find("#insertForm select").eq(z).attr("id");
			iFrameContent.find("#insertForm select").eq(z).val($("#customField-input-" +prefix+ anagraficaAttr3).val());
			$("#claim-saveDraft").click();
		}
		iFrameContent.find("#messageAnagrafica").show();
	}

	function updateInput() {
		var iFrameContent = $(".anagrafica .foreignIFrame >iframe").contents();

	    var prefix=iFrameContent.find("#REQ_CALLERPREFIX").val();
		var AnagraficaInputLength = iFrameContent.find("#insertForm input").length;
		for (i = 0; i < AnagraficaInputLength; i++) {
			var anagraficaAttr = iFrameContent.find("#insertForm input").eq(i).attr("id");
			var anagraficaInput = iFrameContent.find("#insertForm input").eq(i).val();
			$("#customField-input-" +prefix+ anagraficaAttr).val(anagraficaInput).change();
		}
		var id = iFrameContent.find("form").eq(2).attr("id");
		if ($("#customField-input-" +prefix+ id + "_name").val() != "") {
			$("#customField-input-" +prefix+ id + "_name").parent().parent().parent().removeClass("aGEmpty");
		}else{
			$("#customField-input-" +prefix+ id + "_name").parent().parent().parent().addClass("aGEmpty");
		}
		var AnagraficaTextAreaLength = iFrameContent.find("#insertForm textarea").length;
		for (t = 0; t < AnagraficaTextAreaLength; t++) {
			var anagraficaAttr2 = iFrameContent.find("#insertForm textarea").eq(t).attr("id");
			var anagraficaInput2 = iFrameContent.find("#insertForm textarea").eq(t).val();
			$("#customField-input-"+ prefix + anagraficaAttr2).val(anagraficaInput2).change();
		}
		var AnagraficaSelectLength = iFrameContent.find("#insertForm select").length;
		for (z = 0; z < AnagraficaSelectLength; z++) {
			var anagraficaAttr3 = iFrameContent.find("#insertForm select").eq(z).attr("id");
			var anagraficaInput3 = iFrameContent.find("#insertForm select").eq(z).val();
			$("#customField-input-"+ prefix + anagraficaAttr3).val(anagraficaInput3).change();
		}
		$("#claim-saveDraft").click();
	}

	function itemsOnClick() {
		var iFrameContent = $(".anagrafica .foreignIFrame >iframe").contents();
		iFrameContent.find("#readResultList a").on("click", function() {
			iFrameContent.find("#readResultList a").removeClass("selectedA");
			$(this).addClass("selectedA");
			iFrameContent.find("#insertForm input").eq(0).val(this.id);
		});
		iFrameContent.find("#readResultList a").on("dblclick", function() {
			updateInput();
			$(".anagrafica").addClass("hiddenAbsolute");
			$(".loadingDialog").hide();
		});
	}

	function iframeFunction() {
		var iFrameContent = $(".anagrafica .foreignIFrame >iframe").contents();
		$(".loadingDialog").show("slow");

		iFrameContent.find("#changingOnResponse").on('click', function() {
			var prefix=iFrameContent.find("#REQ_CALLERPREFIX").val();
			var idKind = iFrameContent.find("#insertForm input").eq(0).attr("id");
			var idSelected = $("#customField-input-"+prefix+idKind).val();
			console.log(idKind);
			if (!idSelected == ""){
				iFrameContent.find("#readResultList #" + idSelected).click();
			}
			var nameKind = iFrameContent.find("#insertForm input").eq(1).attr("id");
			var nameSelected = $("#customField-input-" + nameKind).val();
			if ($temporaryInfo == 1 ) {
				$temporaryInfo = 0;
				if (!nameSelected == ""){
					updateReverse();
				}
			}
			itemsOnClick();
			$(".anagrafica").removeClass("hiddenAbsolute");
		});

		iFrameContent.find(".btn-success").on("click", function() {
			iFrameContent.find("#messageAnagrafica").hide();
		});
		//iFrameContent.find("a[href='#exitSubject']").on("click", function() {
		iFrameContent.find("#exitSubject").on("click", function() {
			updateInput();
			$(".anagrafica").addClass("hiddenAbsolute");
			$(".loadingDialog").hide();

		});

	}

	function openDialogAnagrafica(kindAnagraphic, idRow) {
		var iFrameContent = $(".anagrafica .foreignIFrame >iframe").contents();
		iFrameContent.find(".container-fluid").eq(0).hide();
		iFrameContent.find("hr").hide();

		iFrameContent.find("#REQ_COLLECTIONNAME_READ").val(kindAnagraphic).change();
		iFrameContent.find("#readBtn").click();
		if (idRow == "") {
			$temporaryInfo = 1;
		}
		iframeFunction();
	}


	$(".loadingDialog").on("click", function() {
		if (!$(".anagrafica").hasClass("hiddenAbsolute")) {
			$(".anagrafica").addClass("hiddenAbsolute");
			$(".loadingDialog").hide();
		}
	});

	var emptyAnagrafica = $('.anagraficaGroup input[id*="name"]').length;

	for (i = 0; i < emptyAnagrafica; i++) {
		if ($('.anagraficaGroup input[id*="name"]').eq(i).val() == "") {
			$('.anagraficaGroup input[id*="name"]').eq(i).parent().parent().parent().addClass("aGEmpty");
		}
	}

	function withAddressBook() {
		var iFrameContent = $(".anagrafica .foreignIFrame >iframe").contents();
		if (iFrameContent.find(".order-md-1").children().eq(1).hasClass("col-sm-12")) {
			iFrameContent.find(".order-md-1").children().eq(0).show();
			iFrameContent.find(".order-md-1").children().eq(1).addClass("col-sm-8").removeClass("col-sm-12");
		}
		iFrameContent.find("#messageAnagrafica").parent().show();
	}
	function withoutAddressBook() {
		var iFrameContent = $(".anagrafica .foreignIFrame >iframe").contents();
		if (iFrameContent.find(".order-md-1").children().eq(1).hasClass("col-sm-8")) {
			iFrameContent.find(".order-md-1").children().eq(0).hide();
			iFrameContent.find(".order-md-1").children().eq(1).removeClass("col-sm-8").addClass("col-sm-12");
		}
		iFrameContent.find("#messageAnagrafica").parent().hide();
	}


	$('body').on("iFrameListLoaded",function(){
		//console.log('lol');
	});
	setTimeout(function(){
	$("#layout-center").removeClass("hiddenAbsolute");
	},200);
},600);
