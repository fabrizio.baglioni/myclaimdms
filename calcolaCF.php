<?php

define('__ROOT__', dirname(__FILE__));
header('Access-Control-Allow-Origin: *');
require_once __ROOT__.'/engine/Utils.php';

$url = 'http://webservices.dotnethell.it/codicefiscale.asmx/CalcolaCodiceFiscale';

$data = array(
    'Nome'          => \Request\Request::get('Nome'),
    'Cognome'       => \Request\Request::get('Cognome'),
    'ComuneNascita' => \Request\Request::get('ComuneNascita'),
    'DataNascita'   => \Request\Request::get('DataNascita'),
    'Sesso'         => \Request\Request::get('Sesso')
);
/*
$data = array(
    'Nome'          => 'Fabrizio',
    'Cognome'       => 'Baglioni',
    'ComuneNascita' => 'Marino',
    'DataNascita'   => '23/09/1981',
    'Sesso'         => 'M'
);
*/

// use key 'http' even if you send the request to https://...
$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
    )
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
if ($result === FALSE) { }

$dom = new DOMDocument;
$dom->loadXML($result);
$books = $dom->getElementsByTagName('string');

echo $books[0]->nodeValue;