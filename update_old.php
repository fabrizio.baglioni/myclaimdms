<?php
header('Access-Control-Allow-Origin: *');

require_once 'engine\DATConnector\DATConnector.php';
require_once 'engine\configurator.php';
require_once 'engine\Utils.php';
require_once 'engine\SubjectCollection\SubjectCollection.php';
require_once 'engine\Subject\Subject.php';
require_once 'engine\Request\Request.php';

//generates a request from GET and POST arrays
use DATConnector\DATConnector;
use Request\Request;
use Request\RequestNotValidException;
use Response\FailureResponse;
use Subject\SubjectNotValidException;
use DAT\PostmanDAT\PostmanDAT;
use SubjectCollection\SubjectCollection;


try {
    Request::validate();   //throws an exception and exit if validation fails
    $kn                 = Request::getCustomerNumber();
    $collName           = Request::getCollectionName();
    $subjectId          = Request::getSubjectId();
    $updatedSubjectJson = Request::getSubject();
    $fileName   = $collName.'.'.getenv('jsonExt');

    $dossierId = DATConnector::findContractByRef($kn);

    $attachment = DATConnector::getSingleAttachment($dossierId, getenv('mcFldId'), $fileName);

    $sc = \SubjectCollection\SubjectCollectionFactory::buildFromJson($attachment);

    $subjectToUpdate = $sc->getSubjectById($subjectId);
    $subjectToUpdate->decodeObject(json_decode($updatedSubjectJson));

    DATConnector::changeStatus($dossierId, getenv('statusDocumentUpload'));

    DATConnector::uploadAttachment($dossierId, $fileName, getenv('mimeType'), getenv('mcFldId'), json_encode($sc));

    var_dump($updatedSubjectJson);
}
catch (RequestNotValidException $e)
{
    return new FailureResponse($e);
}
catch (SubjectNotValidException $e)
{
    return new FailureResponse($e);
}
catch (\SubjectCollection\SubjectNotFoundException $e) {
    return new FailureResponse($e);
}



