<?php
header('Access-Control-Allow-Origin: *');

require_once 'engine\DATConnector\DATConnector.php';
require_once 'engine\configurator.php';
require_once 'engine\Utils.php';
require_once 'engine\SubjectCollection\SubjectCollection.php';
require_once 'engine\Subject\Subject.php';
require_once 'engine\Request\Request.php';

//generates a request from GET and POST arrays
use DATConnector\DATConnector;
use Request\Request;
use Request\RequestNotValidException;
use Response\FailureResponse;
use Subject\ModularSubjectFactory;
use Subject\SubjectNotValidException;
use Subject\Subject;
use DAT\PostmanDAT\PostmanDAT;
use SubjectCollection\SubjectCollection;


try {
    //Request::validate();   //throws an exception and exit if validation fails
    //$kn                 = Request::getCustomerNumber();
    $kn = '3132952';
    //$collName           = Request::getCollectionName();
    $collName = 'owner';
    //$updatedSubjectJson = Request::getSubject();
    $updatedSubjectJson = "{
                \"firstName\": \"Ciro\",
                \"lastName\": \"Baglioni\"
            }";

    /*
    $lc = DATConnector::listContract($kn);
    $dossierId = $lc->return[0]->contractId;
    $definitions = DATConnector::getSingleAttachment($dossierId, getenv('mcFldDef'), getenv('mcFileDef'));
    */
    $definitions = file_get_contents('testJson/definitions.hjson');

    $defObjs = json_decode($definitions);

    $modularSubject = ModularSubjectFactory::buildFromJsonObj($collName, $defObjs->$collName);
    $subject = \Subject\SubjectFactory::buildFromModularDefinition($modularSubject);

    //$attachment = DATConnector::getSingleAttachment($dossierId, getenv('mcSbjFld'), getenv('mcSbjFile'));
    $attachment = "{
   \"type\":\"owner\",
   \"subjects\":{
      \"12\":{
         \"firstName\":\"Fabrizio\",
         \"lastName\":\"Baglioni\"
      },
      \"13\":{
         \"firstName\":\"Mario\",
         \"lastName\":\"Rossi\"
      }
   }
}";
    $subjects = json_decode($attachment);

    $sc = \SubjectCollection\SubjectCollectionFactory::buildFromJson($attachment);
    //$subject = \Subject\SubjectFactory::build($collName);
    $subject = \Subject\SubjectFactory::buildFromModularDefinition($modularSubject);
/*

    echo '<pre>';
    var_dump($sc);
    echo '</pre>';
    die;

    foreach ($subjects->subjects->$collName as $id=>$subject) {
        echo '<pre>';
        var_dump($id, $subject);
        echo '</pre>';
    }

    die;*/

    $subject->decodeObject(json_decode($updatedSubjectJson));

    $sc->addSubject($subject);

    echo '<pre>';
    var_dump($sc);
    echo '</pre>';
    die;

    DATConnector::changeStatus($dossierId, getenv('statusDocumentUpload'));

    DATConnector::uploadAttachment($dossierId, $fileName, getenv('mimeType'), getenv('mcFldId'), json_encode($sc));

    var_dump(json_encode($subject));
}
catch (RequestNotValidException $e)
{
    return new FailureResponse($e);
}
catch (SubjectNotValidException $e)
{
    return new FailureResponse($e);
}
catch (\SubjectCollection\SubjectNotFoundException $e) {
    return new FailureResponse($e);
}

