<?php

define('__ROOT__', dirname(__FILE__));
header('Access-Control-Allow-Origin: *');
require_once __ROOT__.'/engine/Utils.php';

//generates a request from GET and POST arrays
use DATConnector\DATConnector;
use Request\Request;
use Request\RequestNotValidException;
use Response\FailureResponse;
use Subject\SubjectNotValidException;


try {
    Request::validate();   //throws an exception and exit if validation fails
    $masterAcc  = Request::getMasterAccount();
    $kn         = Request::getCustomerNumber();
    $kn        .= getenv('env_suffix');
    $collName   = Request::getCollectionName();
    $fileName   = getenv('mcSbjFile');      //should depend on strategy

    $subjectId          = Request::getSubjectId();
    $updatedSubjectJson = Request::getSubject();

    $dossierId = DATConnector::findContractByRef($kn);
    //if ($dossierId == -1) $dossierId = DATConnector::createDossier($kn); -> ERROR CONDITION
    $attachment = DATConnector::getSingleAttachment($dossierId, getenv('mcFldId'), $fileName);

    $lc = \SubjectCollection\CollectionArrayFactory::buildFromJson($attachment);
    $sc = $lc[$collName];
    $subjectToUpdate = $sc->getSubjectById($subjectId);
    $subjectToUpdate->decodeObject(json_decode($updatedSubjectJson));

    DATConnector::changeStatus($dossierId, getenv('statusDocumentUpload'));
    DATConnector::uploadAttachment($dossierId, $fileName, getenv('mimeType'), getenv('mcFldId'), json_encode($lc));

    echo json_encode([]);
}
catch (RequestNotValidException $e)
{
    return new FailureResponse($e);
}
catch (SubjectNotValidException $e)
{
    return new FailureResponse($e);
}
catch (\SubjectCollection\SubjectNotFoundException $e) {
    echo $e->getMessage();
}
catch (Exception $e)
{
    echo $e->getMessage();
}



