<?php

if (isset($_GET['debug']))
{
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    //var_dump(json_decode($_GET['subj']));die;
}

define('__ROOT__', dirname(__FILE__));
header('Access-Control-Allow-Origin: *');
require_once __ROOT__.'/engine/Utils.php';

use DATConnector\DATConnector;
use Request\Request;
use Request\RequestNotValidException;
use Response\FailureResponse;
use Subject\ModularSubjectFactory;
use Subject\SubjectNotValidException;


try {
    Request::validate();   //throws an exception and exit if validation fails
    $masterAcc  = Request::getMasterAccount();
    $kn         = Request::getCustomerNumber();
    $kn        .= getenv('env_suffix');
    $collName   = Request::getCollectionName();
    $fileName   = getenv('mcSbjFile');      //should depend on strategy

    $subjectId          = Request::getSubjectId();
    $updatedSubjectJson = Request::getSubject();

    $dossierId = DATConnector::findContractByRef($kn);
    if ($dossierId == -1) $dossierId = DATConnector::createDossier($kn);
    $attachment = DATConnector::getSingleAttachment($dossierId, getenv('mcFldId'), $fileName);

    $dossierIdFM  = DATConnector::findContractByRef($masterAcc);
    $definitions = DATConnector::getSingleAttachment($dossierIdFM, getenv('mcFldDef'), getenv('mcFileDef'));
    ModularSubjectFactory::init(json_decode($definitions));
    $lc = \SubjectCollection\CollectionArrayFactory::buildFromJson($attachment);
    $sc = (array_key_exists($collName, $lc))?$lc[$collName]:$lc[$collName] = new \SubjectCollection\SubjectCollection($collName);

    $subject = New \Subject\ModularSubject($collName);
    $subject->decodeObject(json_decode($updatedSubjectJson));
    $sc->addSubject($subject);
    DATConnector::changeStatus($dossierId, getenv('statusDocumentUpload'));
    DATConnector::uploadAttachment($dossierId, $fileName, getenv('mimeType'), getenv('mcFldId'), json_encode($lc));

    echo json_encode($subject->getId());
}
catch (RequestNotValidException $e)
{
    return new FailureResponse($e);
}
catch (SubjectNotValidException $e)
{
    return new FailureResponse($e);
}
catch (\SubjectCollection\SubjectNotFoundException $e) {
    return new FailureResponse($e);
}



