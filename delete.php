<?php

define('__ROOT__', dirname(__FILE__));
header('Access-Control-Allow-Origin: *');
require_once __ROOT__.'/engine/Utils.php';

//generates a request from GET and POST arrays
use DATConnector\DATConnector;
use Request\Request;
use Request\RequestNotValidException;
use Response\FailureResponse;
use Subject\SubjectNotValidException;


try {
    Request::validate();   //throws an exception and exit if validation fails
    $kn                 = Request::getCustomerNumber();
    $kn        .= getenv('env_suffix');
    $collName           = Request::getCollectionName();
    $subjectId          = Request::getSubjectId();

    $fileName   = getenv('mcSbjFile');      //should depend on strategy

    $dossierId = DATConnector::findContractByRef($kn);
    $attachment = DATConnector::getSingleAttachment($dossierId, getenv('mcFldId'), $fileName);

    $lc = \SubjectCollection\CollectionArrayFactory::buildFromJson($attachment);
    $sc = $lc[$collName];
    $subjectToUpdate = $sc->getSubjectById($subjectId);

    $sc->removeSubject($subjectId);

    DATConnector::changeStatus($dossierId, getenv('statusDocumentUpload'));
    DATConnector::uploadAttachment($dossierId, $fileName, getenv('mimeType'), getenv('mcFldId'), json_encode($lc));

    echo json_encode([]);
}
catch (RequestNotValidException $e)
{
    return new FailureResponse($e);
}
catch (SubjectNotValidException $e)
{
    return new FailureResponse($e);
}
catch (\SubjectCollection\SubjectNotFoundException $e) {
    return new FailureResponse($e);
}



